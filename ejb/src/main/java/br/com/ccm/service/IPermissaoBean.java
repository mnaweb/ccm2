package br.com.ccm.service;

import java.util.List;

import javax.ejb.Remote;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.Usuario;

@Remote
public interface IPermissaoBean{
	
	public List<Modulo> listPermissao(Usuario usuario, int qtdereg);
	
	public List<Permissao> listPermissaoUsuario(Usuario usuario, String tarefa, int qtdereg);
	
	public void deletar(Permissao permissao);

	public boolean verifAcesso(Usuario usuario, String tarefa, int acao, int qtdereg);
	
}
