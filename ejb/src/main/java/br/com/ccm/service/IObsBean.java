package br.com.ccm.service;

import java.util.List;

import javax.ejb.Remote;

import br.com.ccm.model.Empresa;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Remote
public interface IObsBean{

	public List<Obs> listObsVendedor(Vendedor vendedor, int lido, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	public long getQtdeObsParaVendedor(Vendedor vendedor, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	public List<Obs> listObsFichaVendedor(Vendedor vendedor, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdereg);
	
	public Obs getById(Long id);

	public long getQtdeObsParaCcm(Usuario usuarioLogado, Empresa empresa, int qtdereg);
	
	public List<Obs> listObsCcm( int lido, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	public void salvarObs(Obs obs, Usuario usuarioLogado);

	List<Obs> listObsFichaLocatario(Empresa empresaPortal, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	List<Obs> listObsLocatario(int lido, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	long getQtdeObsParaLocatario(Usuario usuarioLogado, Empresa empresa, int qtdereg);
}
