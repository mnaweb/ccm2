package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IGrupoDao;
import br.com.ccm.model.Grupo;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Usuario;

@Stateless
public class GrupoDao extends GenericDAO<Grupo> implements IGrupoDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6168939769568108724L;

	public GrupoDao() {
		super(Grupo.class);
	}

	@SuppressWarnings("unchecked")
	public List<Modulo> listPermissao(Usuario usuario, int qtdeReg){
		HashMap<String, Long> params = new HashMap<String, Long>();
		params.put("grupo",usuario.getGrupo().getId());
		List<Modulo> listaMod = execQuery(" select p.modulo from Permissao p"+
												" where p.grupo.id = :grupo"+
												" order by p.modulo.guia, p.modulo.tipo,"+
												" p.modulo.posicao", params, qtdeReg);

		return listaMod;
	}

}
