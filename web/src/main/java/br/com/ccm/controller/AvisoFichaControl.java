package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IClienteBean;
import br.com.ccm.service.IObsBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;

@Named(value="mbAvisoFicha")
@RequestScoped
public class AvisoFichaControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4330071676487001110L;

	public AvisoFichaControl(){
	}

	@EJB
	private IClienteBean clienteBean;
	
	@EJB
	private IObsBean obsBean;

	@EJB
	private IPrincipalBean principalBean;

	private long qtdeFichasPendente;
	private long qtdeFichasAnalise;
	private long qtdeFichasAprovada;
	private long qtdeFichasChecagem;
	private long qtdeObsPendenteParaVendedor;
	private long qtdeObsPendenteParaCcm;
	private long qtdeObsPendenteParaLocatario;
	private List<Obs> listObs;
	private int lido;
	
	public IClienteBean getClienteBean() {
		return clienteBean;
	}

	public void setClienteBean(IClienteBean clienteBean) {
		this.clienteBean = clienteBean;
	}

	public IObsBean getObsBean() {
		return obsBean;
	}

	public void setObsBean(IObsBean obsBean) {
		this.obsBean = obsBean;
	}

	public long getQtdeFichasPendente() {
		qtdeFichasPendente = clienteBean.getQtdeFichasStatus(Cliente.STATUS_PENDENTE, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		return qtdeFichasPendente;
	}

	public void setQtdeFichasPendente(long qtdeFichasPendente) {
		this.qtdeFichasPendente = qtdeFichasPendente;
	}

	public long getQtdeObsPendenteParaVendedor() {
		PrincipalControl princ = (PrincipalControl) Funcoes.getManagerBean("mbPrincipal");
		qtdeObsPendenteParaVendedor = obsBean.getQtdeObsParaVendedor(princ.getUsuario().getVendedor(), Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		return qtdeObsPendenteParaVendedor;
	}

	public void setQtdeObsPendenteParaVendedor(long qtdeObsPendenteParaVendedor) {
		this.qtdeObsPendenteParaVendedor = qtdeObsPendenteParaVendedor;
	}

	public int getLido() {
		return lido;
	}

	public void setLido(int lido) {
		this.lido = lido;
	}

	public List<Obs> getListObs() {
		Usuario usu = Funcoes.getUsuarioLogado();
		
		listObs = obsBean.listObsVendedor(usu.getVendedor(), lido, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		
		return listObs;
	}

	public void setListObs(List<Obs> listObs) {
		this.listObs = listObs;
	}

	public long getQtdeObsPendenteParaCcm() {
		qtdeObsPendenteParaCcm = obsBean.getQtdeObsParaCcm(Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		return qtdeObsPendenteParaCcm;
	}

	public void setQtdeObsPendenteParaCcm(long qtdeObsPendenteParaCcm) {
		this.qtdeObsPendenteParaCcm = qtdeObsPendenteParaCcm;
	}

	public long getQtdeFichasAnalise() {
		qtdeFichasAnalise = clienteBean.getQtdeFichasStatus(Cliente.STATUS_ANALISE, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		return qtdeFichasAnalise;
	}

	public void setQtdeFichasAnalise(long qtdeFichasAnalise) {
		this.qtdeFichasAnalise = qtdeFichasAnalise;
	}

	public long getQtdeFichasAprovada() {
		qtdeFichasAprovada = clienteBean.getQtdeFichasStatus(Cliente.STATUS_APROVADA, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		return qtdeFichasAprovada;
	}

	public void setQtdeFichasAprovada(long qtdeFichasAprovada) {
		this.qtdeFichasAprovada = qtdeFichasAprovada;
	}

	public long getQtdeFichasChecagem() {
		qtdeFichasChecagem = clienteBean.getQtdeFichasStatus(Cliente.STATUS_CHECAGEM, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		return qtdeFichasChecagem;
	}

	public void setQtdeFichasChecagem(long qtdeFichasChecagem) {
		this.qtdeFichasChecagem = qtdeFichasChecagem;
	}

	public long getQtdeObsPendenteParaLocatario() {
		qtdeObsPendenteParaLocatario = obsBean.getQtdeObsParaLocatario(Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		return qtdeObsPendenteParaLocatario;
	}

	public void setQtdeObsPendenteParaLocatario(long qtdeObsPendenteParaLocatario) {
		this.qtdeObsPendenteParaLocatario = qtdeObsPendenteParaLocatario;
	}

}
