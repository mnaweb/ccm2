package br.com.ccm.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.ccm.service.IClienteBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.service.IUsuarioBean;
import br.com.ccm.service.IVendedorBean;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Named(value="mbRelatorioFicha")
@ConversationScoped
public class RelatorioFichaControl implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8843902163170153645L;
	
	@EJB
	private IClienteBean clienteBean;
	
	@EJB
	private IVendedorBean vendedorBean;

	@EJB
	private IUsuarioBean usuarioBean;

	@EJB
	private IPrincipalBean principalBean;

	private StreamedContent reportComissao;
	
	private Vendedor vendedorProc;
	private Usuario usuarioProc;
	private Date dtIni;
	private Date dtFin;
	private int formato = 1;

	private List<SelectItem> vendedorLista = new ArrayList<SelectItem>();
	private List<SelectItem> usuVendedorList = new ArrayList<SelectItem>();

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public StreamedContent getReportComissao() {
		return reportComissao;
	}

	public void setReportComissao(StreamedContent reportComissao) {
		this.reportComissao = reportComissao;
	}

	public List<SelectItem> getVendedorLista() {
		Usuario usuAux = Funcoes.getUsuarioLogado();
		
		vendedorLista = new ArrayList<SelectItem>();

		List<Vendedor> listAux = vendedorBean.listaAll();
		if(usuAux.getVendedor().isCcm())
			vendedorLista.add(new SelectItem(new Vendedor(),"Todas"));
		
		for(Vendedor obj:listAux){
			if(usuAux.getVendedor().isCcm()){
				vendedorLista.add(new SelectItem(obj,obj.getNome()));
			}else{
				if(usuAux.getVendedor().getId().equals(obj.getId()))
					vendedorLista.add(new SelectItem(obj,obj.getNome()));
			}
		}
		
		if(vendedorProc==null && !usuAux.getVendedor().isCcm()){
			vendedorProc = usuAux.getVendedor();
		}
		
		return vendedorLista;
	}

	public void setVendedorLista(List<SelectItem> vendedorLista) {
		this.vendedorLista = vendedorLista;
	}

	public List<SelectItem> getUsuVendedorList() {
		return usuVendedorList;
	}

	public void setUsuVendedorList(List<SelectItem> usuVendedorList) {
		this.usuVendedorList = usuVendedorList;
	}
	
	public Vendedor getVendedorProc() {
		return vendedorProc;
	}

	public void setVendedorProc(Vendedor vendedorProc) {
		this.vendedorProc = vendedorProc;
	}

	public Usuario getUsuarioProc() {
		return usuarioProc;
	}

	public void setUsuarioProc(Usuario usuarioProc) {
		this.usuarioProc = usuarioProc;
	}

	public void preencherListaUsuario() {
		List<Usuario> listaUsu = usuarioBean.listUsuarioPorVendedor(vendedorProc, Funcoes.getQtdeRegMax());
		
		usuVendedorList = new ArrayList<SelectItem>();
		usuVendedorList.add(new SelectItem(new Usuario(),"Selecione..."));
		
		Usuario usuLogado = Funcoes.getUsuarioLogado();
		if(vendedorProc.getId()==usuLogado.getVendedor().getId() && vendedorProc.isApresentarFichasUsuario()){
			usuVendedorList.add(new SelectItem(usuLogado,usuLogado.getNome()));
		}else{
			for(Usuario usu:listaUsu){
				usuVendedorList.add(new SelectItem(usu,usu.getNome()));
			}
		}
	}

	public int getFormato() {
		return formato;
	}

	public void setFormato(int formato) {
		this.formato = formato;
	}

	public Date getDtIni() {
		return dtIni;
	}

	public void setDtIni(Date dtIni) {
		this.dtIni = dtIni;
	}

	public Date getDtFin() {
		return dtFin;
	}

	public void setDtFin(Date dtFin) {
		this.dtFin = dtFin;
	}

	public void impressaoRetorno(ActionEvent actionEvent) {
		
		JasperPrint impressao = clienteBean.getReportComissao(Funcoes.getEmpresa(),vendedorProc, usuarioProc, dtIni, dtFin);

		try {
			ByteArrayOutputStream out1 = new ByteArrayOutputStream();

			byte[] buf = null;
			ByteArrayInputStream inStream = null;
			
			if(this.formato==1){
				JasperExportManager.exportReportToPdfStream(impressao, out1);
				
				buf = out1.toByteArray();
				inStream = new ByteArrayInputStream(buf);
				  
				reportComissao = new DefaultStreamedContent(inStream, "pdf", "Relatório de Comissionamento");  
			}else{
				// coding For Excel:
				JRXlsExporter exporterXLS = new JRXlsExporter();
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, impressao);
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, out1);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				exporterXLS.exportReport();
		         
				buf = out1.toByteArray();
				inStream = new ByteArrayInputStream(buf);
				  
				reportComissao = new DefaultStreamedContent(inStream, "application/xls", "Relatório de Comissionamento.xls");  
			}
			
		} catch (JRException e) {
			e.printStackTrace();
		}
		
		conversation.end();
    }

}
