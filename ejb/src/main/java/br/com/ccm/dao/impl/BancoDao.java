package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IBancoDao;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

@Stateless
public class BancoDao extends GenericDAO<Banco> implements IBancoDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6303444605954631812L;

	public BancoDao() {
		super(Banco.class);
	}

	public List<Banco> listAll(Usuario usuarioLogado, Empresa empresa, int qtdeReg){

		HashMap<String, Long> params = new HashMap<String, Long>();

		String where = "";
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		@SuppressWarnings("unchecked")
		List<Banco> listaUsu = execQuery("from Banco "+where+" order by nome", params,qtdeReg);
				
		return listaUsu;
	}
	
	@SuppressWarnings("unchecked")
	public List<Banco> listAllByEmpresa(Empresa empresa, int qtdeReg){

		HashMap<String, Long> params = new HashMap<String, Long>();

		String where = "";
		where += " where empresaPortal.id = :idEmp";
		params.put("idEmp",empresa.getId());
		
		List<Banco> listaUsu = execQuery("from Banco "+where+" order by nome", params,qtdeReg);
				
		return listaUsu;
	}
	
	@SuppressWarnings("unchecked")
	public List<Banco> listBancosByUsuario(Usuario usuario, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where (c.criador.id = :usuId)";
		params.put("usuId",new Long(usuario.getId()));
		
		List<Banco> lista = execQuery("Select c from Banco c "+where, params, qtdeReg);

		return lista;
	}

}
