package br.com.ccm.dao;

import javax.ejb.Local;

import br.com.ccm.model.ConfiguracaoSimulador;

@Local
public interface IConfiguracaoSimuladorDao extends IGenericDAO<ConfiguracaoSimulador>{	

}
