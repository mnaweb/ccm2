package br.com.ccm.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import net.sf.jasperreports.engine.JasperPrint;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Remote
public interface IClienteBean{

	Cliente getClienteById(Long id);

	Cliente salvar(Cliente cliente, Obs obs, Usuario usuarioLogado, Empresa empresa);

	void deletar(Cliente Cliente);

	List<Cliente> listFichasVerificarHist(Vendedor vendedor, String cpfProc, Date dtFin, Date dtIni, String statusProc, boolean cancelado, String cnpjProc, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	List<Cliente> listFichas(Vendedor vendedor, String cpfProc, Date dtFin, Date dtIni, String statusProc, boolean cancelado, String cnpjProc, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	long getQtdeFichasStatus(String status, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	boolean verificarDuplicidadeFicha(Vendedor vendedor, String cpf, Date data, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	JasperPrint getReportFicha(Cliente cliente, String logoEmp, Empresa empresa);

	JasperPrint getReportComissao(Empresa empresa, Vendedor vendedor, Usuario usuario, Date dtIni, Date dtFin);

	List<Cliente> listFichasByVendedor(Vendedor vendedor, int qtdereg);

	List<Cliente> listFichasByUsuario(Usuario usuario, int qtdereg);

	List<Cliente> listFichasByBanco(Banco banco, int qtdereg);

}