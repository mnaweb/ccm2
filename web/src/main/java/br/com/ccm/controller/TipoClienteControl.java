package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.ITipoClienteBean;
import br.com.ccm.model.TipoCliente;

@Named(value="mbTipoCliente")
@ConversationScoped
public class TipoClienteControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5690636436369060328L;

	public TipoClienteControl(){
	}

	@EJB
	private ITipoClienteBean tipoClienteBean;

    private List<TipoCliente> dataModel;
    private TipoCliente selection = new TipoCliente();
    private TipoCliente tipoCliente = new TipoCliente();

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public ITipoClienteBean getTipoClienteBean() {
		return tipoClienteBean;
	}

	public void setTipoClienteBean(ITipoClienteBean tipoClienteBean) {
		this.tipoClienteBean = tipoClienteBean;
	}

	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoCliente TipoCliente) {
		this.tipoCliente = TipoCliente;
	}

	public List<TipoCliente> getDataModel(){
    	
		List<TipoCliente> lista = tipoClienteBean.listaAll();
    	
		dataModel = lista;
        return dataModel;
    }

    public TipoCliente getSelection() {
        return selection;
    }

    public void setSelection(TipoCliente selection) {
        this.selection = selection;
    }
    
	public String salvar(){
		try {
			tipoClienteBean.salvar(tipoCliente, Funcoes.getUsuarioLogado());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}

		Funcoes.setarAcesso("tipoCliente");	
    	Funcoes.alteraPag1("simulador/tipoClienteConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoClienteDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.setarAcesso("tipoCliente");	
    	Funcoes.alteraPag1("simulador/tipoClienteConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoClienteDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String deletar(ActionEvent actionEvent){
		tipoCliente = getSelection();
		tipoClienteBean.deletar(tipoCliente);
		
		conversation.end();
		
		return "tipoClienteConsultar";
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.tipoCliente = new TipoCliente();
		
    	Funcoes.setarAcesso("tipoCliente");	
    	Funcoes.alteraPag1("simulador/tipoClienteConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoClienteDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar() {
		this.tipoCliente = getSelection();
		
		Funcoes.setarAcesso("tipoCliente");	
    	Funcoes.alteraPag1("simulador/tipoClienteConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoClienteDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

}
