package br.com.ccm.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.query.JRJpaQueryExecuterFactory;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import br.com.ccm.dao.IClienteDao;
import br.com.ccm.service.IAvalistaBean;
import br.com.ccm.service.IClienteBean;
import br.com.ccm.service.IObsBean;
import br.com.ccm.model.Avalista;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class ClienteBean implements IClienteBean,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5954038135635498229L;
		
	@EJB
	private IClienteDao dao;
		
	@EJB
	private IAvalistaBean avalistaBean;

	@EJB
	private IObsBean obsBean;
		
    private final static String UNIT_NAME = "ccm_ejb";
    
    @PersistenceContext(unitName = UNIT_NAME)
    private EntityManager em;
	
    @Override
	public Cliente getClienteById(Long id){
		Cliente retorno = dao.find(id);
		retorno.getAvalistas();
		
		return retorno;
	}

	@Override
	public Cliente salvar(Cliente cliente, Obs obs, Usuario usuarioLogado, Empresa empresa){
		if(cliente.getDtEntrada()==null){
			cliente.setDtEntrada(new Date());
		}
		
		if(cliente.getStatus()==null || cliente.getStatus().equals(""))
			cliente.setStatus("Checagem");

		if(obs!=null && !obs.getObservacao().equals("") && cliente.getId()==0){
			obs.setCriador(usuarioLogado);
			obs.setInclusao(new Date());
			obs.setAvisarCcm(true);
			
			cliente.addObs(obs);
		}
		
		try{
			Cliente clienteOld = dao.find(cliente.getId());
			
			for(Avalista obj : clienteOld.getAvalistas()){
				if(!cliente.getAvalistas().contains(obj)){
					avalistaBean.deletar(obj);
				}
			}
		}catch (Exception e) {}

		if(cliente.getId()==0){
			if(cliente.getEmpresaPortal()==null){
				cliente.setEmpresaPortal(empresa);
			} 
			cliente.setInclusao(new Date());
			if(cliente.getCriador()==null || cliente.getCriador().getId()==0)
				cliente.setCriador(usuarioLogado);
			
			cliente.setUsuinc(cliente.getCriador().getNome());
		}else{
			cliente.setAlteracao(new Date());
			cliente.setAlterado(usuarioLogado);
			cliente.setUsualt(cliente.getAlterado().getNome());
		}
		
		if(cliente.getStatus().equalsIgnoreCase("Efetivada")){
			cliente.setCustoFicha(cliente.getEmpresaPortal().getCustoFicha());
			cliente.setCustoFichaPerc(cliente.getEmpresaPortal().getCustoFichaPerc());
			cliente.setImpostos(cliente.getEmpresaPortal().getImpostos());
		}
		
		dao.save(cliente);
		
		if(obs!=null && !obs.getObservacao().equals("") && cliente.getId()!=0){
			obs.setCriador(usuarioLogado);
			obs.setInclusao(new Date());
			obs.setCliente(cliente);
			obsBean.salvarObs(obs, usuarioLogado);
		}
				
		return cliente;
	}
	
	@Override
	public void deletar(Cliente Cliente){
		dao.delete(Cliente);
	}
	
	@Override
	public List<Cliente> listFichasVerificarHist(Vendedor vendedor, String cpfProc, Date dtFin, Date dtIni, String statusProc, boolean cancelado, String cnpjProc, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		List<Cliente> lista = listFichas(vendedor, cpfProc, dtFin, dtIni, statusProc, cancelado, cnpjProc, usuarioLogado, empresa, qtdereg);
		for(Cliente obj : lista){
			obj.setPossuiHistorico(dao.verificarDuplicidadeFicha(null, obj.getCpf(), null, true, usuarioLogado, empresa, qtdereg)>1);
		}
		return lista;
	}
	
	@Override
	public List<Cliente> listFichas(Vendedor vendedor, String cpfProc, Date dtFin, Date dtIni, String statusProc, boolean cancelado, String cnpjProc, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.listFichas(vendedor, cpfProc, dtFin, dtIni, statusProc, cancelado, cnpjProc, usuarioLogado, empresa, qtdereg);
	}

	@Override
	public long getQtdeFichasStatus(String status, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.getQtdeFichasStatus(status, usuarioLogado, empresa, qtdereg);
	}

	@Override
	public boolean verificarDuplicidadeFicha(Vendedor vendedor, String cpf, Date data, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.verificarDuplicidadeFicha(vendedor, cpf, data, false, usuarioLogado, empresa, qtdereg)>0;
	}
	
	@Override
	public JasperPrint getReportFicha(Cliente cliente, String logoEmp, Empresa empresa){
		
		JasperPrint impressao = null;

		HttpServletRequest request = (HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String realPath = request.getSession().getServletContext().getRealPath("");
		
        try {
    		HashMap<String, Object> parameters = new HashMap<String, Object>();
    		parameters.put("idFicha", cliente.getId());		
    		parameters.put("logo", realPath+logoEmp);
    		parameters.put("url", empresa.getRelatorioUrl());
    		parameters.put("email", empresa.getRelatorioEmail());
    		parameters.put("fone1", empresa.getRelatorioFone1());
    		parameters.put("fone2", empresa.getRelatorioFone2());
    		parameters.put(JRJpaQueryExecuterFactory.PARAMETER_JPA_ENTITY_MANAGER, em); 

    		if(cliente.getTipo().equals("PJ"))
    			impressao = JasperFillManager.fillReport(realPath + "/report/ficha_pj.jasper", parameters);
    		else
    			impressao = JasperFillManager.fillReport(realPath + "/report/ficha_pf.jasper", parameters);
    		
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        return impressao;
	}

	@Override
	public JasperPrint getReportComissao(Empresa empresa, Vendedor vendedor, Usuario usuario, Date dtIni, Date dtFin){
		
		JasperPrint impressao = null;

		HttpServletRequest request = (HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String realPath = request.getSession().getServletContext().getRealPath("");
		
		SimpleDateFormat formatDt = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatDt2 = new SimpleDateFormat("dd/MM/yyyy");
		
		StringBuilder where = new StringBuilder("");
		StringBuilder filtros = new StringBuilder("");
		if(empresa!=null && empresa.getId()!=0){
			where.append(" and emp.pk_id="+empresa.getId());
			filtros.append(" - Empresa : "+empresa.getNome());
		}
		if(vendedor!=null && vendedor.getId()!=0){
			where.append(" and loj.pk_id="+vendedor.getId());
			filtros.append(" - Loja : "+vendedor.getNome());
		}
		if(usuario!=null && usuario.getId()!=0){
			where.append(" and usu.pk_id="+usuario.getId());
			filtros.append(" - Usu??rio : "+usuario.getNome());
		}
		if(dtIni!=null){
			where.append(" and cli.dt_efetivada>='"+formatDt.format(dtIni)+"'");
			filtros.append(" - De : "+formatDt2.format(dtIni));
		}
		if(dtFin!=null){
			where.append(" and cli.dt_efetivada<='"+formatDt.format(dtFin)+"'");
			filtros.append(" - At?? : "+formatDt2.format(dtFin));
		}
		where.append(" and ds_status='Efetivada'");
		
		where = new StringBuilder(" where "+where.toString().substring(5));
		
		if(filtros.toString().length()>0)
			filtros = new StringBuilder(filtros.toString().substring(3));
		
        try {
    		HashMap<String, Object> parameters = new HashMap<String, Object>();
    				
    		parameters.put("logo", realPath+"/images/ccm.png"); //realPath+Funcoes.getMbPrincipal().getLogoEmp()
    		parameters.put("url",	"http://www.consultoriaccm.com.br/");//empresa.getRelatorioUrl()
    		parameters.put("email", "comercial@consultoriaccm.com.br");//empresa.getRelatorioEmail()
    		parameters.put("fone1", "(11)2765-5547");//empresa.getRelatorioFone1()
    		parameters.put("fone2", "(11)2063-5099");//empresa.getRelatorioFone2()
    		parameters.put("where", where.toString());
    		parameters.put("filtros", filtros.toString());
    		parameters.put(JRJpaQueryExecuterFactory.PARAMETER_JPA_ENTITY_MANAGER, em); 
	    	
            impressao = JasperFillManager.fillReport(realPath + "/report/relatorio_comissao.jasper", parameters);
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        return impressao;
	}

	@Override
	public List<Cliente> listFichasByVendedor(Vendedor vendedor, int qtdereg){
		return dao.listFichasByVendedor(vendedor, qtdereg);
	}
	
	@Override
	public List<Cliente> listFichasByUsuario(Usuario usuario, int qtdereg){
		return dao.listFichasByUsuario(usuario, qtdereg);
	}

	@Override	
	public List<Cliente> listFichasByBanco(Banco banco, int qtdereg){
		return dao.listFichasByBanco(banco, qtdereg);
	}

}