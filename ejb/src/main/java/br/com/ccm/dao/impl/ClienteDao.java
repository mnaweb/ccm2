package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IClienteDao;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class ClienteDao extends GenericDAO<Cliente> implements IClienteDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9096499069391346051L;

	public ClienteDao() {
		super(Cliente.class);
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> listFichas(Vendedor vendedor, String cpfProc, Date dtFin, Date dtIni, String statusProc, boolean cancelado, String cnpjProc, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";
		if(vendedor!=null){
			where += " and c.vendedor.id = :venId";
			params.put("venId",new Long(vendedor.getId()));
			
			if(vendedor.isApresentarFichasUsuario() && vendedor.getId()==usuarioLogado.getVendedor().getId()){
				where += " and c.criador.id = :usuId";
				params.put("usuId",new Long(usuarioLogado.getId()));
			}
		}
		
		if(cpfProc!=null && !cpfProc.equals("")){
			where += " and c.cpf = :cpfProc";
			params.put("cpfProc",cpfProc);
		}

		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and c.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		if(dtIni!=null){
			where += " and c.dtEntrada >= :dtIni";
			params.put("dtIni",dtIni);
		}

		if(dtFin!=null){
			Calendar cal = Calendar.getInstance();
			cal.setTime(dtFin);
			cal.set(Calendar.AM_PM, 0);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.AM_PM, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.add(Calendar.DAY_OF_MONTH, 1);

			where += " and c.dtEntrada < :dtFin";
			params.put("dtFin",cal.getTime());
		}

		if(statusProc!=null && !statusProc.equals("")){
			where += " and c.status = :statusProc";
			params.put("statusProc",statusProc);
		}
		
		if(!cancelado){
			where += " and c.cancelar = :cancelar";
			params.put("cancelar",new Boolean(false));
		}

		if(cnpjProc!=null && !cnpjProc.equals("")){
			where += " and c.cnpjPJ = :cnpjProc";
			params.put("cnpjProc",cnpjProc);
		}

		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Cliente> listaCli = execQuery("Select c from Cliente c "+where+" order by c.dtEntrada desc", params, qtdeReg);

		return listaCli;
	}

	public long getQtdeFichasStatus(String status, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where status = :statusProc";
		params.put("statusProc",status);
		
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		if(!usuarioLogado.getVendedor().isCcm()){
			where += " and vendedor.id = :venId";
			params.put("venId",new Long(usuarioLogado.getVendedor().getId()));
			
			if(usuarioLogado.getVendedor().isApresentarFichasUsuario()){
				where += " and criador.id = :usuId";
				params.put("usuId",new Long(usuarioLogado.getId()));
			}
		}

		where += " and cancelar = :cancelar";
		params.put("cancelar",new Boolean(false));

		Long qtdeFichas = (Long) execQueryObj("Select count(*) from Cliente "+where, params, qtdeReg);

		return qtdeFichas;
	}

	public Long verificarDuplicidadeFicha(Vendedor vendedor, String cpf, Date data, boolean considerarExcluidas, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";
		if(vendedor!=null){
			where += " and c.vendedor.id = :venId";
			params.put("venId",new Long(vendedor.getId()));
			
			if(vendedor.isApresentarFichasUsuario() && vendedor.getId()==usuarioLogado.getVendedor().getId()){
				where += " and c.criador.id = :usuId";
				params.put("usuId",new Long(usuarioLogado.getId()));
			}
		}
		
		if(cpf!=null && !cpf.equals("")){
			where += " and c.cpf = :cpfProc";
			params.put("cpfProc",cpf);
		}
		
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and c.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		if(data!=null){
			Calendar cal = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal.setTime(data);
			cal.set(Calendar.AM_PM, 0);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.AM_PM, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal2.setTime(cal.getTime());
			cal2.add(Calendar.DAY_OF_MONTH, 1);
			
			where += " and c.dtEntrada >= :data and c.dtEntrada < :datF";
			params.put("data",cal.getTime());
			params.put("datF",cal2.getTime());
		}
		
		if(!considerarExcluidas){
			where += " and c.cancelar = :cancelar";
			params.put("cancelar",new Boolean(false));
		}
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		Long qtdeFichas = (Long) execQueryObj("Select count(*) from Cliente c "+where, params, qtdeReg);

		return qtdeFichas;
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> listFichasByVendedor(Vendedor vendedor, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where c.vendedor.id = :venId";
		params.put("venId",new Long(vendedor.getId()));
		
		List<Cliente> listaCli = execQuery("Select c from Cliente c "+where+" order by c.dtEntrada desc", params, qtdeReg);

		return listaCli;
	}
	
	@SuppressWarnings("unchecked")
	public List<Cliente> listFichasByUsuario(Usuario usuario, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where (c.criador.id = :usuId OR c.alterado.id = :usuId)";
		params.put("usuId",new Long(usuario.getId()));
		
		List<Cliente> listaCli = execQuery("Select c from Cliente c "+where+" order by c.dtEntrada desc", params, qtdeReg);

		return listaCli;
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> listFichasByBanco(Banco banco, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where c.bancoFin.id = :banId";
		params.put("banId",new Long(banco.getId()));
		
		List<Cliente> listaCli = execQuery("Select c from Cliente c "+where+" order by c.dtEntrada desc", params, qtdeReg);

		return listaCli;
	}

}