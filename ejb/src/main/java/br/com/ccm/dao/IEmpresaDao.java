package br.com.ccm.dao;

import java.util.List;
import javax.ejb.Local;
import br.com.ccm.model.Empresa;

@Local
public interface IEmpresaDao extends IGenericDAO<Empresa>{

	List<Empresa> listAllByAtivo(int qtdeReg);	

}
