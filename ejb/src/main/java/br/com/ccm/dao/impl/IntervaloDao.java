package br.com.ccm.dao.impl;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.ccm.dao.IIntervaloDao;
import br.com.ccm.model.Intervalo;

@Stateless
public class IntervaloDao extends GenericDAO<Intervalo> implements IIntervaloDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460340813643414204L;

	public IntervaloDao() {
		super(Intervalo.class);
	}

}
