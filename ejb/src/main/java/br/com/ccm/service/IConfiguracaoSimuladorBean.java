package br.com.ccm.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.ConfiguracaoSimulador;
import br.com.ccm.model.Usuario;

@Remote
public interface IConfiguracaoSimuladorBean{

	public ArrayList<ConfiguracaoSimulador> listaAll();
	
	public ConfiguracaoSimulador salvar(ConfiguracaoSimulador configuracaoSimulador, List<Coeficiente> coeficientes, Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(ConfiguracaoSimulador configuracaoSimulador);
	
	public ConfiguracaoSimulador getById(Long id);

}
