package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.IPermissaoDao;
import br.com.ccm.service.IPermissaoBean;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.Usuario;

@Stateless
public class PermissaoBean implements IPermissaoBean,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3079656157098142669L;

	@EJB
	private IPermissaoDao dao;
			
	@Override
	public List<Modulo> listPermissao(Usuario usuario, int qtdereg){
		return dao.listPermissao(usuario,qtdereg);
	}
	
	@Override
	public List<Permissao> listPermissaoUsuario(Usuario usuario, String tarefa, int qtdereg) {
		return dao.listPermissaoUsuario(usuario, tarefa,qtdereg);
	}

	@Override
	public boolean verifAcesso(Usuario usuario, String tarefa, int acao, int qtdereg){
		return dao.verifAcesso(usuario, tarefa, acao,qtdereg);
	}

	@Override
	public void deletar(Permissao permissao){
		dao.delete(permissao);
	}
}
