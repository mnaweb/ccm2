package br.com.ccm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "sm_coeficiente")
public class Coeficiente implements Serializable, Cloneable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1719149170084228719L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_id")
	private Long id;
	@ManyToOne
	@JoinColumn(name="fk_configuracao")
	private ConfiguracaoSimulador configuracaoSimulador;
	@ManyToOne
	@JoinColumn(name="fk_intervalo")
	private Intervalo intervalo;
	@ManyToOne
	@JoinColumn(name="fk_tipoCliente")
	private TipoCliente tipoCliente;
	@ManyToOne
	@JoinColumn(name="fk_tipoVeiculo")
	private TipoVeiculo tipoVeiculo;
	@Column(name = "ds_prazo")
	private String prazo;
	@Column(name = "fl_pontoDeEquilibrio")
	private boolean pontoDeEquilibrio;
	@Column(name = "vl_coeficiente")
	private BigDecimal vlCoeficiente;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	
	@Transient
	private int compositeKey;
	
	public Coeficiente() {
		id = new Long(0);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public ConfiguracaoSimulador getConfiguracaoSimulador() {
		return configuracaoSimulador;
	}
	public void setConfiguracaoSimulador(ConfiguracaoSimulador configuracaoSimulador) {
		this.configuracaoSimulador = configuracaoSimulador;
	}
	public Intervalo getIntervalo() {
		return intervalo;
	}
	public void setIntervalo(Intervalo intervalo) {
		this.intervalo = intervalo;
	}
	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(TipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}
	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}
	public String getPrazo() {
		return prazo;
	}
	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}
	public boolean isPontoDeEquilibrio() {
		return pontoDeEquilibrio;
	}
	public void setPontoDeEquilibrio(boolean pontoDeEquilibrio) {
		this.pontoDeEquilibrio = pontoDeEquilibrio;
	}
	public BigDecimal getVlCoeficiente() {
		return vlCoeficiente;
	}
	public void setVlCoeficiente(BigDecimal vlCoeficiente) {
		this.vlCoeficiente = vlCoeficiente;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}
	
	
	public int getCompositeKey() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((alteracao == null) ? 0 : alteracao.hashCode());
		result = prime
				* result
				+ ((configuracaoSimulador == null) ? 0 : configuracaoSimulador
						.hashCode());
		result = prime * result + ((criador == null) ? 0 : criador.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((inclusao == null) ? 0 : inclusao.hashCode());
		result = prime * result
				+ ((intervalo == null) ? 0 : intervalo.hashCode());
		result = prime * result + (pontoDeEquilibrio ? 1231 : 1237);
		result = prime * result + ((prazo == null) ? 0 : prazo.hashCode());
		result = prime * result
				+ ((tipoCliente == null) ? 0 : tipoCliente.hashCode());
		result = prime * result
				+ ((tipoVeiculo == null) ? 0 : tipoVeiculo.hashCode());
		result = prime * result
				+ ((vlCoeficiente == null) ? 0 : vlCoeficiente.hashCode());
		
		compositeKey = result;
		
		return compositeKey;
	}

	public void setCompositeKey(int compositeKey) {
		this.compositeKey = compositeKey;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((alteracao == null) ? 0 : alteracao.hashCode());
		result = prime * result + compositeKey;
		result = prime
				* result
				+ ((configuracaoSimulador == null) ? 0 : configuracaoSimulador
						.hashCode());
		result = prime * result + ((criador == null) ? 0 : criador.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((inclusao == null) ? 0 : inclusao.hashCode());
		result = prime * result
				+ ((intervalo == null) ? 0 : intervalo.hashCode());
		result = prime * result + (pontoDeEquilibrio ? 1231 : 1237);
		result = prime * result + ((prazo == null) ? 0 : prazo.hashCode());
		result = prime * result
				+ ((tipoCliente == null) ? 0 : tipoCliente.hashCode());
		result = prime * result
				+ ((tipoVeiculo == null) ? 0 : tipoVeiculo.hashCode());
		result = prime * result
				+ ((vlCoeficiente == null) ? 0 : vlCoeficiente.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coeficiente other = (Coeficiente) obj;
		if (alteracao == null) {
			if (other.alteracao != null)
				return false;
		} else if (!alteracao.equals(other.alteracao))
			return false;
		if (compositeKey != other.compositeKey)
			return false;
		if (configuracaoSimulador == null) {
			if (other.configuracaoSimulador != null)
				return false;
		} else if (!configuracaoSimulador.equals(other.configuracaoSimulador))
			return false;
		if (criador == null) {
			if (other.criador != null)
				return false;
		} else if (!criador.equals(other.criador))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inclusao == null) {
			if (other.inclusao != null)
				return false;
		} else if (!inclusao.equals(other.inclusao))
			return false;
		if (intervalo == null) {
			if (other.intervalo != null)
				return false;
		} else if (!intervalo.equals(other.intervalo))
			return false;
		if (pontoDeEquilibrio != other.pontoDeEquilibrio)
			return false;
		if (prazo == null) {
			if (other.prazo != null)
				return false;
		} else if (!prazo.equals(other.prazo))
			return false;
		if (tipoCliente == null) {
			if (other.tipoCliente != null)
				return false;
		} else if (!tipoCliente.equals(other.tipoCliente))
			return false;
		if (tipoVeiculo == null) {
			if (other.tipoVeiculo != null)
				return false;
		} else if (!tipoVeiculo.equals(other.tipoVeiculo))
			return false;
		if (vlCoeficiente == null) {
			if (other.vlCoeficiente != null)
				return false;
		} else if (!vlCoeficiente.equals(other.vlCoeficiente))
			return false;
		return true;
	}
	
    public Object clone(){
        try{
            return super.clone();
        }catch( CloneNotSupportedException e){
            return null;
        }
    }
}
