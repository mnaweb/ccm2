package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IIntervaloBean;
import br.com.ccm.model.Intervalo;

@Named(value="mbIntervalo")
@ConversationScoped
public class IntervaloControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5690636436369060328L;

	public IntervaloControl(){
	}

	@EJB
	private IIntervaloBean intervaloBean;

    private List<Intervalo> dataModel;
    private Intervalo selection = new Intervalo();
    private Intervalo intervalo = new Intervalo();

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public IIntervaloBean getIntervaloBean() {
		return intervaloBean;
	}

	public void setIntervaloBean(IIntervaloBean intervaloBean) {
		this.intervaloBean = intervaloBean;
	}

	public Intervalo getIntervalo() {
		return intervalo;
	}

	public void setIntervalo(Intervalo Intervalo) {
		this.intervalo = Intervalo;
	}

	public List<Intervalo> getDataModel(){
    	
		List<Intervalo> lista = intervaloBean.listaAll();
    	
		dataModel = lista;
        return dataModel;
    }

    public Intervalo getSelection() {
        return selection;
    }

    public void setSelection(Intervalo selection) {
        this.selection = selection;
    }
    
	public String salvar(){
		try {
			intervaloBean.salvar(intervalo, Funcoes.getUsuarioLogado());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}

		Funcoes.setarAcesso("intervalo");	
    	Funcoes.alteraPag1("simulador/intervaloConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/intervaloDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.setarAcesso("intervalo");	
    	Funcoes.alteraPag1("simulador/intervaloConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/intervaloDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String deletar(ActionEvent actionEvent){
		intervalo = getSelection();
		intervaloBean.deletar(intervalo);
		
		conversation.end();
		
		return "intervaloConsultar";
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.intervalo = new Intervalo();
		
    	Funcoes.setarAcesso("intervalo");	
    	Funcoes.alteraPag1("simulador/intervaloConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/intervaloDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar() {
		this.intervalo = getSelection();
		
		Funcoes.setarAcesso("intervalo");	
    	Funcoes.alteraPag1("simulador/intervaloConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/intervaloDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

}
