package br.com.ccm.service.impl;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.IModuloBean;
import br.com.ccm.service.IPermissaoBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.service.IUsuarioBean;
import br.com.ccm.service.IVendedorBean;
import br.com.ccm.util.Funcoes;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class PrincipalBean implements IPrincipalBean,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4642411120210660118L;

	@EJB
	private IUsuarioBean usuarioBean;
	
	@EJB
	private IPermissaoBean permissaoBean;
	
	@EJB
	private IModuloBean moduloBean;
	
	@EJB
	private IVendedorBean vendedorBean;
		
    @PostConstruct  
    public void onLoad() {  
        System.out.println("Iniciei. -> Principal");  
    }  
      
    @PreDestroy  
    public void onDestroy() {  
        System.out.println("Fui destruido. -> Principal");  
    } 

	public Usuario logar(String nome, String senha, Empresa empresa, int qtdereg) throws NoSuchAlgorithmException{
		List<Modulo> listMod = moduloBean.listModulo(qtdereg);
		
		if(listMod.size()<=0){
			Vendedor vendedor = new Vendedor();
			vendedor.setNome("CCM");
			vendedor.setCcm(true);
			vendedorBean.salvar(vendedor, null, null);
			
			Usuario usuNew = new Usuario();
			usuNew.setLogin("admin");
			usuNew.setSenha("admin");
			usuNew.setAdmin(true);
			try {
				usuarioBean.salvar(usuNew, null, null, qtdereg);
			} catch (AplicacaoException e){}
			
			Modulo moduloFerramenta = new Modulo();
			moduloFerramenta.setGuia(new Long(9));
			moduloFerramenta.setTipo(new Long(0));
			moduloFerramenta.setPosicao(new Long(0));
			moduloFerramenta.setTitulo("Ferramenta");
			moduloBean.salvar(moduloFerramenta, null);
			
			Modulo moduloFerramenta1 = new Modulo();
			moduloFerramenta1.setGuia(new Long(9));
			moduloFerramenta1.setTipo(new Long(1));
			moduloFerramenta1.setPosicao(new Long(1));
			moduloFerramenta1.setTitulo("Cadastro de modulos");
			moduloFerramenta1.setPagina1("ferramenta/moduloConsultar.xhtml");
			moduloFerramenta1.setPagina2("ferramenta/moduloDigitar.xhtml");
			moduloBean.salvar(moduloFerramenta1, null);			

			Modulo moduloFerramenta2 = new Modulo();
			moduloFerramenta2.setGuia(new Long(9));
			moduloFerramenta2.setTipo(new Long(1));
			moduloFerramenta2.setPosicao(new Long(2));
			moduloFerramenta2.setTarefa("grupo");
			moduloFerramenta2.setTitulo("Cadastro de grupo");
			moduloFerramenta2.setPagina1("ferramenta/grupoConsultar.xhtml");
			moduloFerramenta2.setPagina2("ferramenta/grupoDigitar.xhtml");
			moduloBean.salvar(moduloFerramenta2, null);			

			Modulo moduloFerramenta3 = new Modulo();
			moduloFerramenta3.setGuia(new Long(9));
			moduloFerramenta3.setTipo(new Long(1));
			moduloFerramenta3.setPosicao(new Long(3));
			moduloFerramenta3.setTarefa("usuario");
			moduloFerramenta3.setTitulo("Cadastro de usuarios");
			moduloFerramenta3.setPagina1("ferramenta/usuarioConsultar.xhtml");
			moduloFerramenta3.setPagina2("ferramenta/usuarioDigitar.xhtml");
			moduloBean.salvar(moduloFerramenta3, null);

			Modulo moduloFerramenta4 = new Modulo();
			moduloFerramenta4.setGuia(new Long(9));
			moduloFerramenta4.setTipo(new Long(1));
			moduloFerramenta4.setPosicao(new Long(3));
			moduloFerramenta4.setTarefa("empresa");
			moduloFerramenta4.setTitulo("Cadastro de empresas");
			moduloFerramenta4.setPagina1("ferramenta/empresaConsultar.xhtml");
			moduloFerramenta4.setPagina2("ferramenta/empresaDigitar.xhtml");
			moduloBean.salvar(moduloFerramenta4, null);

			Modulo cadastro = new Modulo();
			cadastro.setGuia(new Long(1));
			cadastro.setTipo(new Long(0));
			cadastro.setPosicao(new Long(0));
			cadastro.setTitulo("Cadastro");
			moduloBean.salvar(cadastro, null);

			Modulo cadastro1 = new Modulo();
			cadastro1.setGuia(new Long(1));
			cadastro1.setTipo(new Long(1));
			cadastro1.setPosicao(new Long(1));
			cadastro1.setTarefa("vendedor");
			cadastro1.setTitulo("Lojas");
			cadastro1.setPagina1("Cadastro/vendedorConsultar.xhtml");
			cadastro1.setPagina2("Cadastro/vendedorDigitar.xhtml");
			moduloBean.salvar(cadastro1, null);
			
			Modulo cadastro2 = new Modulo();
			cadastro2.setGuia(new Long(1));
			cadastro2.setTipo(new Long(1));
			cadastro2.setPosicao(new Long(2));
			cadastro2.setTarefa("banco");
			cadastro2.setTitulo("Bancos");
			cadastro2.setPagina1("Cadastro/bancoConsultar.xhtml");
			cadastro2.setPagina2("Cadastro/bancoDigitar.xhtml");
			moduloBean.salvar(cadastro2, null);

			Modulo ficha = new Modulo();
			ficha.setGuia(new Long(2));
			ficha.setTipo(new Long(0));
			ficha.setPosicao(new Long(0));
			ficha.setTitulo("ficha");
			moduloBean.salvar(ficha, null);
			
			Modulo ficha2 = new Modulo();
			ficha2.setGuia(new Long(2));
			ficha2.setTipo(new Long(1));
			ficha2.setPosicao(new Long(2));
			ficha2.setTarefa("ficha");
			ficha2.setTitulo("Criar ficha");
			ficha2.setPagina1("ficha/fichaDigitar.xhtml");
			ficha2.setPagina2("");
			moduloBean.salvar(ficha2, null);
			
			Modulo ficha3 = new Modulo();
			ficha3.setGuia(new Long(2));
			ficha3.setTipo(new Long(1));
			ficha3.setPosicao(new Long(2));
			ficha3.setTarefa("ficha");
			ficha3.setTitulo("Fichas");
			ficha3.setPagina1("ficha/fichaConsultar.xhtml");
			ficha3.setPagina2("ficha/fichaDigitar.xhtml");
			moduloBean.salvar(ficha3, null);

		}
		
		senha = Funcoes.md5(senha);
		
		Usuario usuario = usuarioBean.getUsuarioNomeESenha(nome, senha, null, null, qtdereg);
		
		if(usuario==null){
			return null;
		}
		
		if(!usuario.getAdmin() && (usuario.getEmpresaPortal()==null || !usuario.getEmpresaPortal().equals(empresa))){
			return null;
		}
				
		return usuario;
	}

	public List<Modulo> getListModulo(Usuario usuario, int qtdereg) {
		List<Modulo> listaMod = new ArrayList<Modulo>(); 
		if(!usuario.getAdmin()){
			listaMod = permissaoBean.listPermissao(usuario, qtdereg);
		}else{
			listaMod = moduloBean.listModulo(qtdereg);
		}
		return listaMod;
	}

	public List<Permissao> listPermissaoUsuario(Usuario usuario, String tarefa, int qtdereg) {
		List<Permissao> listaMod = new ArrayList<Permissao>(); 

		listaMod = permissaoBean.listPermissaoUsuario(usuario, tarefa, qtdereg);

		return listaMod;
	}

}