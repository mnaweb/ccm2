package br.com.ccm.service.impl;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.ccm.service.IBeanDeLista;

@Stateless
public class BeanDeLista implements IBeanDeLista,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4339902332656558875L;
	
    private final static String UNIT_NAME = "ccm_ejb";
    
    @PersistenceContext(unitName = UNIT_NAME)
    private EntityManager em;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object getObjetoLista(Class clazz, long id) {
		return em.find(clazz, id); 
	}
	
}
