package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IVendedorDao;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class VendedorDao extends GenericDAO<Vendedor> implements IVendedorDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1227116135053819154L;

	public VendedorDao() {
		super(Vendedor.class);
	}

	@SuppressWarnings("unchecked")
	public List<Vendedor> listAll(Usuario usuarioLogado, Empresa empresa, int qtdeReg){

		HashMap<String, Long> params = new HashMap<String, Long>();

		String where = "";
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Vendedor> listaVen = execQuery("from Vendedor "+where+" order by nome", params, qtdeReg);
				
		return listaVen;
	}
	
	@SuppressWarnings("unchecked")
	public List<Vendedor> listVendedorByUsuario(Usuario usuario, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where (c.criador.id = :usuId)";
		params.put("usuId",new Long(usuario.getId()));
		
		List<Vendedor> lista = execQuery("Select c from Vendedor c "+where, params, qtdeReg);

		return lista;
	}
}
