package br.com.ccm.dao;

import javax.ejb.Local;
import br.com.ccm.model.Intervalo;

@Local
public interface IIntervaloDao extends IGenericDAO<Intervalo>{	

}
