package br.com.ccm.dao;

import java.util.List;

import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

public interface IVendedorDao extends IGenericDAO<Vendedor>{

	public List<Vendedor> listAll(Usuario usuarioLogado, Empresa empresa, int qtdeReg);
	
	public List<Vendedor> listVendedorByUsuario(Usuario usuario, int qtdeReg);
	
}
