package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IPermissaoDao;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.PermissaoEnum;
import br.com.ccm.model.Usuario;

@Stateless
public class PermissaoDao extends GenericDAO<Permissao> implements IPermissaoDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2888101660072896747L;

	public PermissaoDao() {
		super(Permissao.class);
	}

	@SuppressWarnings("unchecked")
	public List<Modulo> listPermissao(Usuario usuario, int qtdeReg){
		HashMap<String, Long> params = new HashMap<String, Long>();
		params.put("grupo",usuario.getGrupo().getId());
		List<Modulo> listaMod = execQuery(" select p.modulo from Permissao p"+
												" where p.grupo.id = :grupo"+
												" order by p.modulo.guia, p.modulo.tipo,"+
												" p.modulo.posicao", params, qtdeReg);

		return listaMod;
	}

	@SuppressWarnings("unchecked")
	public List<Permissao> listPermissaoUsuario(Usuario usuario, String tarefa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();
		
		params.put("grupo",usuario.getGrupo().getId());
		params.put("tarefa",tarefa);
		
		List<Permissao> listaMod = execQuery(" select p from Permissao p"+
															" where p.grupo.id = :grupo and p.modulo.tarefa = :tarefa "+
															" order by p.modulo.guia, p.modulo.tipo,"+
															" p.modulo.posicao", params, qtdeReg);

		return listaMod;
	}
	
	@Override
	public boolean verifAcesso(Usuario usuario, String tarefa, int acao, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();
		
		String and = "";
		
		if(acao==PermissaoEnum.ACAO_CRIAR.ordinal()){
			and = " and p.incluir = :acao";
		}else if(acao==PermissaoEnum.ACAO_ALTERAR.ordinal()){
			and = " and p.alterar = :acao";
		}else if(acao==PermissaoEnum.ACAO_REMOVER.ordinal()){
			and = " and p.remover = :acao";
		}
		
		params.put("grupo",usuario.getGrupo().getId());
		params.put("tarefa",tarefa);
		params.put("acao",true);
		
		Long qtde = (Long) execQueryObj(" select count(p.id) from Permissao p"+
										" where p.grupo.id = :grupo and p.modulo.tarefa = :tarefa "+and
										, params, qtdeReg);

		return qtde>0;
	}
}
