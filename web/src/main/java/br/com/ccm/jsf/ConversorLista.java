package br.com.ccm.jsf;

import java.io.Serializable;
import java.lang.reflect.Method;

import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

import br.com.ccm.service.IBeanDeLista;

@FacesConverter(value="convListagem")
public class ConversorLista implements Converter, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2059142829083502656L;
	
	@EJB
	private IBeanDeLista beanDeLista;
	
	public IBeanDeLista getBeanDeLista() {
		return beanDeLista;
	}

	public void setBeanDeLista(IBeanDeLista beanDeLista) {
		this.beanDeLista = beanDeLista;
	}

	public ConversorLista(){}

	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String novoValor) {

		Object obj=null;
		StringBuffer textoFornecedor = new StringBuffer(novoValor);
		
		int idPos = textoFornecedor.indexOf("id=");
		int classePos = textoFornecedor.indexOf("classe=");

		String classe = null;
		Long id = null;
		try{
			classe = textoFornecedor.substring(classePos+7,idPos-1);
			id = Long.parseLong(textoFornecedor.substring(idPos+3,textoFornecedor.length()));
		}catch(StringIndexOutOfBoundsException ex){
			return null;
		}
		
		if(classe.equalsIgnoreCase("java.lang.String")){
			return null;
		}
		
		try{
			Class clazz = Class.forName(classe);
			obj = beanDeLista.getObjetoLista(clazz, id);
		}catch(Exception e){
			e.printStackTrace();
		}

		return obj;
	}

	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object obj) {
		
		String classe = obj.getClass().getName();
				
		Method[] m = obj.getClass().getMethods();
		
		Long id=0L;
		
		for(Method mExe:m){
			if(mExe.getName().toUpperCase().equals("GETID")){
				try {
					id = (Long) mExe.invoke(obj,null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		String retorno=new String();
		try{
			retorno="classe="+classe+"&id="+id;
		}catch (Exception e) {
			retorno="";
		}

		return retorno;
	}	
}