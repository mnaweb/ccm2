package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IAvisoBean;
import br.com.ccm.model.Aviso;

@Named(value="mbAviso")
@ConversationScoped
public class AvisoControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5690636436369060328L;

	public AvisoControl(){
	}

	@EJB
	private IAvisoBean avisoBean;

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

    private List<Aviso> dataModel;
    private Aviso selection = new Aviso();
    private Aviso aviso = new Aviso();

	public IAvisoBean getAvisoBean() {
		return avisoBean;
	}

	public void setAvisoBean(IAvisoBean avisoBean) {
		this.avisoBean = avisoBean;
	}

	public Aviso getAviso() {
		return aviso;
	}

	public void setAviso(Aviso Aviso) {
		this.aviso = Aviso;
	}

	public List<Aviso> getDataModel(){
    	
		List<Aviso> lista = avisoBean.listaAll();
    	
		dataModel = lista;
        return dataModel;
    }

    public Aviso getSelection() {
        return selection;
    }

    public void setSelection(Aviso selection) {
        this.selection = selection;
    }
    
	public String salvar(){
		try {
			avisoBean.salvar(aviso, Funcoes.getUsuarioLogado());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}

		Funcoes.setarAcesso("aviso");	
    	Funcoes.alteraPag1("ferramenta/avisoConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/avisoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.setarAcesso("aviso");	
    	Funcoes.alteraPag1("ferramenta/avisoConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/avisoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String deletar(ActionEvent actionEvent){
		aviso = getSelection();
		avisoBean.deletar(aviso);
		
		conversation.end();
		
		return "avisoConsultar";
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.aviso = new Aviso();
		
    	Funcoes.setarAcesso("aviso");	
    	Funcoes.alteraPag1("ferramenta/avisoConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/avisoDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar() {
		this.aviso = getSelection();
		
		Funcoes.setarAcesso("aviso");	
    	Funcoes.alteraPag1("ferramenta/avisoConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/avisoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

}
