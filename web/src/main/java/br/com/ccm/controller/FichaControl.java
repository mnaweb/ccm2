package br.com.ccm.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IAvalistaBean;
import br.com.ccm.service.IBancoBean;
import br.com.ccm.service.IClienteBean;
import br.com.ccm.service.IObsBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.service.IUsuarioBean;
import br.com.ccm.service.IVendedorBean;
import br.com.ccm.model.Avalista;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;
import byjg.webservices.cep.CEPService;

@Named(value="mbFicha")
@ConversationScoped
public class FichaControl implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8843902163170153645L;
	
	@EJB
	private IClienteBean clienteBean;
	
    private Comparator<Cliente> dateInclusao;
    private Comparator<Cliente> dateAlteracao;
    private List<Cliente> dataModel;
    private Cliente selection;
	private List<SelectItem> estadoCivilLista; 
	private List<SelectItem> estadosLista;
	private List<SelectItem> tpResidenciaLista;
	private List<SelectItem> cargoLista;
	private List<SelectItem> comprovadaPorLista;
	private Cliente cliente;
	private List<SelectItem> statusLista;
	private Obs obs;
	private Vendedor vendedorProc;
	private String cpfProc;
	private String cnpjProc;
	private Date dtFin;
	private Date dtIni;
	private String statusProc;
	private boolean cancelado;
	private List<SelectItem> vendedorLista = new ArrayList<SelectItem>();
	private List<SelectItem> bancoLista = new ArrayList<SelectItem>();
	private List<SelectItem> usuVendedorList = new ArrayList<SelectItem>();
	private Avalista avalista;
	private Avalista avalistaOld;
	private boolean existeRegistroAvalista;
	private boolean carroAbaixo2002;
	private List<String> listCargo = new ArrayList<String>();
	private List<Obs> listaHistorico;
	
	private StreamedContent reportFicha;
	
	@EJB
	private IVendedorBean vendedorBean;
	
	@EJB
	private IBancoBean bancoBean;
	
	@EJB
	private IObsBean obsBean;

	@EJB
	private IUsuarioBean usuarioBean;

	@EJB
	private IAvalistaBean avalistaBean;
	
	@EJB
	private IPrincipalBean principalBean;
	
	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public FichaControl(){
		selection = new Cliente();
		estadoCivilLista = new ArrayList<SelectItem>(); 
		estadosLista = new ArrayList<SelectItem>();
		tpResidenciaLista = new ArrayList<SelectItem>();
		cargoLista = new ArrayList<SelectItem>();
		comprovadaPorLista = new ArrayList<SelectItem>();
		cliente = new Cliente();
		statusLista = new ArrayList<SelectItem>();
		obs = new Obs();
		listaHistorico = new ArrayList<Obs>();
	}

	public void setSelection(Cliente selection) {
		this.selection = selection;
	}

	public List<Cliente> getDataModel(){
		Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		if(dataModel==null){
			montarDados();
		}
		
        return dataModel;
    }
    
    public Comparator<Cliente> getDateInclusao(){
        if (dateInclusao == null){
            dateInclusao = new Comparator<Cliente>(){
                public int compare(Cliente o1, Cliente o2) {
                    return o1.getInclusao().compareTo(o2.getInclusao());
                }
            };
        }
        return dateInclusao;
    }

    public Comparator<Cliente> getDateAlteracao(){
        if (dateAlteracao == null){
            dateAlteracao = new Comparator<Cliente>(){
                public int compare(Cliente o1, Cliente o2) {
                    return o1.getAlteracao().compareTo(o2.getAlteracao());
                }
            };
        }
        return dateAlteracao;
    }
    
	public String salvar(){
		
		if((Funcoes.getEmpresa()==null || Funcoes.getEmpresa().getId()==0) && cliente.getId()==0){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Selecione uma empresa.", "Selecione uma empresa.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		if(cliente.getId()==0){
			if(clienteBean.verificarDuplicidadeFicha(cliente.getVendedor(), cliente.getCpf(), new Date(), Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax())){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Já existe uma ficha criada para este CPF.", "Já existe uma ficha criada para este CPF.");
				FacesContext.getCurrentInstance().addMessage("cpf", message);
				return null;
			}
		}
		
		if(cliente.getEstadoCivil().equalsIgnoreCase("Casado")){
			if(cliente.getConjuge()==null || cliente.getConjuge().equals("")){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cônjuge: Erro de validação: o valor é necessário.", "Cônjuge: Erro de validação: o valor é necessário.");
				FacesContext.getCurrentInstance().addMessage("conjuge", message);
				return null;
			}
			if(cliente.getCpfConjuge()==null || cliente.getCpfConjuge().equals("")){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF Cônjuge: Erro de validação: o valor é necessário.", "CPF Cônjuge: Erro de validação: o valor é necessário.");
				FacesContext.getCurrentInstance().addMessage("cpfConjuge", message);
				return null;
			}
			if(cliente.getIdentidadeConjuge()==null || cliente.getIdentidadeConjuge().equals("")){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Identidade: Erro de validação: o valor é necessário.", "Identidade: Erro de validação: o valor é necessário.");
				FacesContext.getCurrentInstance().addMessage("identidadeConjuge", message);
				return null;
			}
			if(cliente.getDtNascConjuge()==null){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data Nascimento Cônjuge: Erro de validação: o valor é necessário.", "Data Nascimento Cônjuge: Erro de validação: o valor é necessário.");
				FacesContext.getCurrentInstance().addMessage("dtNascConjuge", message);
				return null;
			}
		}
		
		if(cliente.getCargo().equalsIgnoreCase("Empresário")){
			if(cliente.getCnpj()==null || cliente.getCnpj().equals("")){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CNPJ: Erro de validação: o valor é necessário.", "CNPJ: Erro de validação: o valor é necessário.");
				FacesContext.getCurrentInstance().addMessage("cnpj", message);
				return null;
			}
		}
		
		Pattern p = Pattern.compile("\\d{3,12}-\\d{1,1}");
		Matcher m = p.matcher(this.cliente.getContaCorrente());
		if(!m.matches()){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conta corrente inválida", "Conta corrente inválida");
			FacesContext.getCurrentInstance().addMessage("contaCorrente", message);
			return null;
		}

		clienteBean.salvar(this.cliente, this.obs, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa());
		
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
		
	}

	public String cancelar(){
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public void deletar(ActionEvent actionEvent){
		cliente = getSelection();
		cliente.setCancelar(true);
		clienteBean.salvar(cliente, null, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa());
		
		conversation.end();
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.cliente = new Cliente();
    	
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar(ActionEvent actionEvent) {
    	try{
			this.cliente = clienteBean.getClienteById(getSelection().getId());
			
			this.validarAnoFabMod();
			
			this.preencherListaUsuario();
			
			usuVendedorList.add(new SelectItem(this.cliente.getCriador(),this.cliente.getCriador().getNome()));
			
			Funcoes.setarAcesso("ficha");
	    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
	    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
			Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
		}catch (NullPointerException e) {
			Funcoes.createMessageJSFErro("Selecione uma ficha", null);
		}
    }

	public List<SelectItem> getEstadoCivilLista() {
		estadoCivilLista = new ArrayList<SelectItem>();
		
		estadoCivilLista.add(new SelectItem("Casado","Casado"));
		estadoCivilLista.add(new SelectItem("Solteiro","Solteiro"));
		estadoCivilLista.add(new SelectItem("Separado","Separado"));
		estadoCivilLista.add(new SelectItem("Viúvo","Viúvo"));

		return estadoCivilLista;
	}

	public void setEstadoCivilLista(List<SelectItem> estadoCivilLista) {
		this.estadoCivilLista = estadoCivilLista;
	}

	public List<SelectItem> getEstadosLista() {
		estadosLista = new ArrayList<SelectItem>();
		estadosLista.add(new SelectItem("AC","AC"));
		estadosLista.add(new SelectItem("AL","AL"));
		estadosLista.add(new SelectItem("AP","AP"));
		estadosLista.add(new SelectItem("AM","AM"));
		estadosLista.add(new SelectItem("BA","BA"));
		estadosLista.add(new SelectItem("CE","CE"));
		estadosLista.add(new SelectItem("DF","DF"));
		estadosLista.add(new SelectItem("ES","ES"));
		estadosLista.add(new SelectItem("GO","GO"));
		estadosLista.add(new SelectItem("MA","MA"));
		estadosLista.add(new SelectItem("MT","MT"));
		estadosLista.add(new SelectItem("MS","MS"));
		estadosLista.add(new SelectItem("MG","MG"));
		estadosLista.add(new SelectItem("PA","PA"));
		estadosLista.add(new SelectItem("PB","PB"));
		estadosLista.add(new SelectItem("PR","PR"));
		estadosLista.add(new SelectItem("PE","PE"));
		estadosLista.add(new SelectItem("PI","PI"));
		estadosLista.add(new SelectItem("RJ","RJ"));
		estadosLista.add(new SelectItem("RN","RN"));
		estadosLista.add(new SelectItem("RS","RS"));
		estadosLista.add(new SelectItem("RO","RO"));
		estadosLista.add(new SelectItem("RR","RR"));
		estadosLista.add(new SelectItem("SC","SC"));
		estadosLista.add(new SelectItem("SP","SP"));
		estadosLista.add(new SelectItem("SE","SE"));
		estadosLista.add(new SelectItem("TO","TO"));
		return estadosLista;
	}

	public void setEstadosLista(List<SelectItem> estadosLista) {
		this.estadosLista = estadosLista;
	}

	public List<SelectItem> getTpResidenciaLista() {
		tpResidenciaLista = new ArrayList<SelectItem>();
		
		tpResidenciaLista.add(new SelectItem("Própria","Própria"));
		tpResidenciaLista.add(new SelectItem("Alugada","Alugada"));
		tpResidenciaLista.add(new SelectItem("Familiar","Familiar"));
		
		return tpResidenciaLista;
	}

	public void setTpResidenciaLista(List<SelectItem> tpResidenciaLista) {
		this.tpResidenciaLista = tpResidenciaLista;
	}

	public List<SelectItem> getCargoLista() {
		cargoLista = new ArrayList<SelectItem>();
		
		cargoLista.add(new SelectItem("Empresário","Empresário"));
		cargoLista.add(new SelectItem("Autônomo","Autônomo"));
		cargoLista.add(new SelectItem("Profissional Liberal","Profissional Liberal"));
		cargoLista.add(new SelectItem("Assalariado","Assalariado"));
		cargoLista.add(new SelectItem("Aposentado","Aposentado"));
		cargoLista.add(new SelectItem("Pensionista","Pensionista"));
		
		return cargoLista;
	}

	public void setCargoLista(List<SelectItem> cargoLista) {
		this.cargoLista = cargoLista;
	}

	public List<SelectItem> getComprovadaPorLista() {
		comprovadaPorLista = new ArrayList<SelectItem>();
		comprovadaPorLista.add(new SelectItem("Extrato","Extrato"));
		comprovadaPorLista.add(new SelectItem("Holerite","Holerite"));
		comprovadaPorLista.add(new SelectItem("Holerite + Extrato","Holerite + Extrato"));

		return comprovadaPorLista;
	}

	public void setComprovadaPorLista(List<SelectItem> comprovadaPorLista) {
		this.comprovadaPorLista = comprovadaPorLista;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void pesquisarCEPRes() {
		CEPService service = new CEPService();
		
		String endereco = "";
		try {
			endereco = service.obterLogradouro(this.cliente.getCepres(), "ccmsistema", "123456");
			String[] result = endereco.split(",");
			
			this.cliente.setLogradourores(result[0].trim());
			this.cliente.setBairrores(result[1].trim());
			this.cliente.setCidaderes(result[2].trim());
			this.cliente.setUfres(result[3].trim());
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CEP não cadastrado", "CEP não cadastrado");
			FacesContext.getCurrentInstance().addMessage("cepRes", message);	
		}
	}
	
	public void pesquisarCEPCom() {
		CEPService service = new CEPService();
		
		String endereco = "";
		try {
			endereco = service.obterLogradouro(this.cliente.getCepcom(), "ccmsistema", "123456");
			String[] result = endereco.split(",");
			
			this.cliente.setLogradourocom(result[0].trim());
			this.cliente.setBairrocom(result[1].trim());
			this.cliente.setCidadecom(result[2].trim());
			this.cliente.setUfcom(result[3].trim());
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CEP não cadastrado", "CEP não cadastrado");
			FacesContext.getCurrentInstance().addMessage("cepCom", message);	
		}
	}

	public Cliente getSelection() {
		return selection;
	}

	public void setDateInclusao(Comparator<Cliente> dateInclusao) {
		this.dateInclusao = dateInclusao;
	}

	public void setDateAlteracao(Comparator<Cliente> dateAlteracao) {
		this.dateAlteracao = dateAlteracao;
	}

	public void setDataModel(List<Cliente> dataModel) {
		this.dataModel = dataModel;
	}

    public void mudarStatus(ActionEvent actionEvent) {
    	StatusControl mb = (StatusControl) Funcoes.getManagerBean("mbStatusFicha");
    	
    	this.cliente = getSelection();
    	mb.setCliente(cliente);
    	mb.setObs(new Obs());
    	
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
		Funcoes.alteraPag2("ficha/fichaMudarStatus.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public Obs getObs() {
		return obs;
	}

	public void setObs(Obs obs) {
		this.obs = obs;
	}

	public List<SelectItem> getStatusLista() {
		statusLista = new ArrayList<SelectItem>();
		
		statusLista.add(new SelectItem(Cliente.STATUS_CHECAGEM,"Checagem"));
		statusLista.add(new SelectItem(Cliente.STATUS_PENDENTE,"Pendente"));
		statusLista.add(new SelectItem(Cliente.STATUS_NEGOCIACAO,"Negociação"));
		statusLista.add(new SelectItem(Cliente.STATUS_ANALISE,"Análise"));
		statusLista.add(new SelectItem(Cliente.STATUS_APROVADA,"Aprovada"));
		statusLista.add(new SelectItem(Cliente.STATUS_RECUSADA,"Recusada"));
		statusLista.add(new SelectItem(Cliente.STATUS_CANCELADA,"Cancelada"));
		statusLista.add(new SelectItem(Cliente.STATUS_EFETIVADA,"Efetivada"));
		statusLista.add(new SelectItem(Cliente.STATUS_APROVADO_CANC,"Aprovada-Canc."));

		return statusLista;
	}

	public void setStatusLista(List<SelectItem> statusLista) {
		this.statusLista = statusLista;
	}

	public Vendedor getVendedorProc() {
		return vendedorProc;
	}

	public void setVendedorProc(Vendedor vendedorProc) {
		this.vendedorProc = vendedorProc;
	}

	public String getCpfProc() {
		return cpfProc;
	}

	public void setCpfProc(String cpfProc) {
		this.cpfProc = cpfProc;
	}

	public Date getDtFin() {
		return dtFin;
	}

	public void setDtFin(Date dtFin) {
		this.dtFin = dtFin;
	}

	public Date getDtIni() {
		return dtIni;
	}

	public void setDtIni(Date dtIni) {
		this.dtIni = dtIni;
	}

	public String getStatusProc() {
		if(this.statusProc==null && Funcoes.getValorDoParamAdd("status")!=null)
			statusProc = Funcoes.getValorDoParamAdd("status");

		return statusProc;
	}

	public void setStatusProc(String statusProc) {
		this.statusProc = statusProc;
	}
	
	public List<SelectItem> getVendedorLista() {
		Usuario usuAux = Funcoes.getUsuarioLogado();
		
		vendedorLista = new ArrayList<SelectItem>();

		List<Vendedor> listAux = vendedorBean.listaAll();
		if(usuAux.getVendedor().isCcm())
			vendedorLista.add(new SelectItem(new Vendedor(),"Todas"));
		
		for(Vendedor obj:listAux){
			if(usuAux.getVendedor().isCcm()){
				vendedorLista.add(new SelectItem(obj,obj.getNome()));
			}else{
				if(usuAux.getVendedor().getId().equals(obj.getId()))
					vendedorLista.add(new SelectItem(obj,obj.getNome()));
			}
		}
		
		if(vendedorProc==null && !usuAux.getVendedor().isCcm()){
			vendedorProc = usuAux.getVendedor();
		}
		
		return vendedorLista;
	}

	public void setVendedorLista(List<SelectItem> vendedorLista) {
		this.vendedorLista = vendedorLista;
	}
	
	public void procurar(ActionEvent actionEvent){
		montarDados();
	}

	public void montarDados(){
		dataModel = clienteBean.listFichasVerificarHist(vendedorProc, cpfProc, dtFin, dtIni, statusProc, cancelado, cnpjProc, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
	}
	
    public void observacao(ActionEvent actionEvent) {
		try{
	    	this.cliente = getSelection();
			
			Usuario usu = Funcoes.getUsuarioLogado();
			if(usu.getAdmin()){
				this.cliente = clienteBean.getClienteById(this.cliente.getId());
				listaHistorico = new ArrayList<Obs>(this.cliente.getObs());
			}else if(usu.getVendedor().isCcm()){
				listaHistorico = new ArrayList<Obs>(obsBean.listObsFichaLocatario(usu.getEmpresaPortal(), cliente.getId(), Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax()));
			}else{
				listaHistorico = new ArrayList<Obs>(obsBean.listObsFichaVendedor(usu.getVendedor(), cliente.getId(), Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax()));
			}
			
			Funcoes.setarAcesso("ficha");
	    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
			Funcoes.alteraPag2("ficha/fichaHistorico.xhtml");
	    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    	}catch (NullPointerException e) {
    		Funcoes.createMessageJSFErro("Selecione uma ficha", null);
		}
    }

	public String salvarHistorico(){
		Cliente clienteAux = clienteBean.getClienteById(cliente.getId());
		
		obs.setCriador(Funcoes.getUsuarioLogado());
		obs.setInclusao(new Date());
		obs.setCliente(clienteAux);
				
		obsBean.salvarObs(obs, Funcoes.getUsuarioLogado());
		
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}
	
	public String cancelarHistorico(){
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}

	public List<Obs> getListaHistorico() {
		return listaHistorico;
	}

	public void setListaHistorico(List<Obs> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}

	private StreamedContent file;
	
	public void downloadFile() {
		long idCli = 0;
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getParameter("idCli")!=null){
			idCli = Long.parseLong(req.getParameter("idCli"));
		}
		Cliente clienteAux = clienteBean.getClienteById(idCli);
		
		if(!new File(clienteAux.getArquivo()).exists()){
			try {
				HttpServletRequest request = (HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
				String realPath = request.getSession().getServletContext().getRealPath("");
				File fileBack = new File(Funcoes.getMessageFromResourceBundle("caminho.logo")+"/sistema/pdf/"+idCli+"/"+clienteAux.getArquivo().replaceAll("/sistema/pdf/"+idCli+"/", ""));
	            FileInputStream fin;
				fin = new FileInputStream(fileBack);
				File arquivoDest = new File(realPath+clienteAux.getArquivo());
	            FileOutputStream fos = new FileOutputStream(arquivoDest);
	            final byte[] bytes = new byte[1000000];  
	            for (int lidos = -1; (lidos = fin.read(bytes, 0, 1000000)) != -1; fos.write(bytes, 0, lidos)) {}  
	            fos.flush();
	            fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		InputStream stream = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream(clienteAux.getArquivo());  
        file = new DefaultStreamedContent(stream, "pdf", "seguro");  
    }
	
    public void setFile(StreamedContent file) {
		this.file = file;
	}

	public StreamedContent getFile() {  
        return file;  
    }

	public List<SelectItem> getBancoLista() {

		List<Banco> listAux = bancoBean.listAllByEmpresa(this.cliente.getEmpresaPortal(), Funcoes.getQtdeRegMax());
		
		bancoLista = new ArrayList<SelectItem>();
		bancoLista.add(new SelectItem(new Vendedor(),"Selecione..."));
		
		for(Banco obj:listAux){
			bancoLista.add(new SelectItem(obj,obj.getNome()));
		}

		return bancoLista;
	}

	public void setBancoLista(List<SelectItem> bancoLista) {
		this.bancoLista = bancoLista;
	}

    public void editarFin(ActionEvent actionEvent) {
		this.cliente = getSelection();

		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaEditarFin.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }
	
	public void salvarFin(ActionEvent actionEvent){
		clienteBean.salvar(cliente, null, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa());

		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
	}

	public boolean isCancelado() {
		return cancelado;
	}

	public void setCancelado(boolean cancelado) {
		this.cancelado = cancelado;
	}

	public void auditoria(ActionEvent actionEvent){
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getParameter("id")!=null){
			long id = Long.parseLong(req.getParameter("id"));
			cliente = clienteBean.getClienteById(id);
		}
		
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaAuditoria.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    	
	}

    public void visualizar(ActionEvent actionEvent) {
    	try{
    		this.cliente = clienteBean.getClienteById(getSelection().getId());
		
	    	this.validarAnoFabMod();
	    	
			Funcoes.setarAcesso("ficha");
	    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
	    	Funcoes.alteraPag2("ficha/fichaVisualizar.xhtml");
			Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    	}catch (NullPointerException e) {
    		Funcoes.createMessageJSFErro("Selecione uma ficha", null);
		}
    }

    public void visualizarAuditoria(ActionEvent actionEvent) {
		this.cliente = getSelection();
		
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaAuditoria.xhtml");
    	Funcoes.alteraPag2("ficha/fichaAuditoriaVisualizar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public void preencherConta() {
		if(this.cliente.getContaCorrente()!=null && !this.cliente.getContaCorrente().equals("") && this.cliente.getContaCorrente().indexOf("-")<0){
			this.cliente.setContaCorrente(this.cliente.getContaCorrente().substring(0,
											this.cliente.getContaCorrente().length()-1)+"-"+
											this.cliente.getContaCorrente().substring(
											this.cliente.getContaCorrente().length()-1));
		}
		
		Pattern p = Pattern.compile("\\d{3,12}-\\d{1,1}");
		Matcher m = p.matcher(this.cliente.getContaCorrente());
		if(!m.matches()){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conta corrente inválida", "Conta corrente inválida");
			FacesContext.getCurrentInstance().addMessage("contaCorrente", message);	
		}
	}

	public List<SelectItem> getUsuVendedorList() {
		if(usuVendedorList.size()==0)
			usuVendedorList.add(new SelectItem(new Usuario(),"Selecione..."));
		
		return usuVendedorList;
	}

	public void setUsuVendedorList(List<SelectItem> usuVendedorList) {
		this.usuVendedorList = usuVendedorList;
	}

	public void preencherListaUsuario() {
		List<Usuario> listaUsu = usuarioBean.listUsuarioPorVendedor(this.cliente.getVendedor(), Funcoes.getQtdeRegMax());
		
		usuVendedorList = new ArrayList<SelectItem>();
		usuVendedorList.add(new SelectItem(new Usuario(),"Selecione..."));
		for(Usuario usu:listaUsu){
			usuVendedorList.add(new SelectItem(usu,usu.getNome()));
		}
	}

	public StreamedContent getReportFicha() {
		return reportFicha;
	}

	public void setReportFicha(StreamedContent reportFicha) {
		this.reportFicha = reportFicha;
	}

    public void impressaoFicha(ActionEvent actionEvent) {
		this.cliente = getSelection();
		
		JasperPrint impressao = clienteBean.getReportFicha(cliente, Funcoes.getLogoEmp(), Funcoes.getEmpresa());

		try {
			ByteArrayOutputStream out1 = new ByteArrayOutputStream();

			JasperExportManager.exportReportToPdfStream(impressao, out1);

			byte[] buf = out1.toByteArray();
			
			ByteArrayInputStream inStream = new ByteArrayInputStream(buf);
			  
			reportFicha = new DefaultStreamedContent(inStream, "pdf", "ficha");  

		} catch (JRException e) {
			e.printStackTrace();
		}
    }

	public void responder(ActionEvent actionEvent){
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getAttribute("his")!=null){
			Long id = ((Obs)req.getAttribute("his")).getCliente().getId();
			this.cliente = clienteBean.getClienteById(id);
			
			Usuario usu = Funcoes.getUsuarioLogado();
			if(usu.getVendedor().isCcm()){
				this.cliente = clienteBean.getClienteById(this.cliente.getId());
				
				listaHistorico = new ArrayList<Obs>(this.cliente.getObs());
			}else{
				listaHistorico = new ArrayList<Obs>(obsBean.listObsFichaVendedor(usu.getVendedor(), cliente.getId(), Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax()));
			}
			
			Funcoes.setarAcesso("ficha");
	    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
			Funcoes.alteraPag2("ficha/fichaHistorico.xhtml");
	    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
	    	
		}
	}

	public void irParaFicha(ActionEvent actionEvent){
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getAttribute("his")!=null){
			Long id = ((Obs)req.getAttribute("his")).getCliente().getId();
			
			this.cliente = clienteBean.getClienteById(id);
			req.setAttribute("objCli", this.cliente);
			
			this.validarAnoFabMod();
			
			this.preencherListaUsuario();
			
			usuVendedorList.add(new SelectItem(this.cliente.getCriador(),this.cliente.getCriador().getNome()));
			
			Funcoes.setarAcesso("ficha");
	    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
	    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
			Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
			
		}

	}
	
    public void addAvalista(ActionEvent actionEvent) {
    	this.avalista = new Avalista();
    	
    	Funcoes.alteraTitulo("Ficha - Avalista");
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaAddAvalista.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }
    
    
    public void salvarAvalista() {
    	if((this.avalista.getId()==null || this.avalista.getId()==0) && (this.cliente.getAvalistas()==null || this.cliente.getAvalistas().size()<=0))
    		this.cliente.addAvalista(this.avalista);
    	    	
    	Funcoes.alteraTitulo("Ficha");
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void cancAvalista() {
    	this.cliente.getAvalistas().clear();
    	if(this.avalistaOld!=null)
    		this.cliente.addAvalista(this.avalistaOld);
    	
    	Funcoes.alteraTitulo("Ficha");
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public Avalista getAvalista() {
		return avalista;
	}

	public void setAvalista(Avalista avalista) {
		this.avalista = avalista;
	}

	public void preencherListaUsuarioAvalista() {
		List<Usuario> listaUsu = usuarioBean.listUsuarioPorVendedor(this.avalista.getVendedor(), Funcoes.getQtdeRegMax());
		
		usuVendedorList = new ArrayList<SelectItem>();
		usuVendedorList.add(new SelectItem(new Usuario(),"Selecione..."));
		for(Usuario usu:listaUsu){
			usuVendedorList.add(new SelectItem(usu,usu.getNome()));
		}
	}

	public void pesquisarCEPResAvalista() {
		CEPService service = new CEPService();
		
		String endereco = "";
		try {
			endereco = service.obterLogradouro(this.avalista.getCepres(), "ccmsistema", "123456");
			String[] result = endereco.split(",");
			
			this.avalista.setLogradourores(result[0].trim());
			this.avalista.setBairrores(result[1].trim());
			this.avalista.setCidaderes(result[2].trim());
			this.avalista.setUfres(result[3].trim());
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CEP não cadastrado", "CEP não cadastrado");
			FacesContext.getCurrentInstance().addMessage("cepRes", message);	
		}
	}
	
	public void pesquisarCEPComAvalista() {
		CEPService service = new CEPService();
		
		String endereco = "";
		try {
			endereco = service.obterLogradouro(this.avalista.getCepcom(), "ccmsistema", "123456");
			String[] result = endereco.split(",");
			
			this.avalista.setLogradourocom(result[0].trim());
			this.avalista.setBairrocom(result[1].trim());
			this.avalista.setCidadecom(result[2].trim());
			this.avalista.setUfcom(result[3].trim());
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CEP não cadastrado", "CEP não cadastrado");
			FacesContext.getCurrentInstance().addMessage("cepCom", message);	
		}
	}

	public void preencherContaAvalista() {
		if(this.avalista.getContaCorrente()!=null && !this.avalista.getContaCorrente().equals("") && this.avalista.getContaCorrente().indexOf("-")<0){
			this.avalista.setContaCorrente(this.avalista.getContaCorrente().substring(0,
											this.avalista.getContaCorrente().length()-1)+"-"+
											this.avalista.getContaCorrente().substring(
											this.avalista.getContaCorrente().length()-1));
		}
		
		Pattern p = Pattern.compile("\\d{3,12}-\\d{1,1}");
		Matcher m = p.matcher(this.avalista.getContaCorrente());
		if(!m.matches()){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conta corrente inválida", "Conta corrente inválida");
			FacesContext.getCurrentInstance().addMessage("contaCorrente", message);	
		}
	}

	public boolean isExisteRegistroAvalista() {
		existeRegistroAvalista = this.cliente.getAvalistas()!=null && this.cliente.getAvalistas().size()>0;
		return existeRegistroAvalista;
	}

	public void setExisteRegistroAvalista(boolean existeRegistroAvalista) {
		this.existeRegistroAvalista = existeRegistroAvalista;
	}

	public void editarAvalista(ActionEvent actionEvent) {
    	this.avalista = this.getCliente().getAvalistas().iterator().next();
    	this.avalistaOld = new Avalista(this.avalista.getId(),this.avalista.getDtEntrada(),this.avalista.getStatus(),this.avalista.getArquivo(),
    			this.avalista.getNome(), this.avalista.getDtNasc(),this.avalista.getCpf(),this.avalista.getEmail(),
    			this.avalista.getIdentidade(), this.avalista.getDtEmissao(),this.avalista.getPai(),this.avalista.getMae(),
    			this.avalista.getEstadoCivil(),this.avalista.getConjuge(),this.avalista.getCpfConjuge(),
    			this.avalista.getIdentidadeConjuge(), this.avalista.getDtNascConjuge(),this.avalista.getEmpresa(),
    			this.avalista.getDtAdmissao(),this.avalista.getCargo(),this.avalista.getCargoDesc(),this.avalista.getCnpj(),
    			this.avalista.getRenda(), this.avalista.getRenda2(),this.avalista.getComprovadaPor(),
    			this.avalista.getRefPessoalNome1(),this.avalista.getRefPessoalFone1(),
    			this.avalista.getRefPessoalNome2(),this.avalista.getRefPessoalFone2(),this.avalista.getAgencia(),
    			this.avalista.getBanco(),this.avalista.getContaCorrente(), this.avalista.getDesde(), this.avalista.getInclusao(),
    			this.avalista.getAlteracao(), this.avalista.getVendedor(), this.avalista.getBancoFin(),this.avalista.getCepres(),
    			this.avalista.getLogradourores(), this.avalista.getNumEndres(),this.avalista.getComplementores(),
    			this.avalista.getBairrores(),this.avalista.getCidaderes(),this.avalista.getUfres(),
    			this.avalista.getTempoResAnosres(), this.avalista.getTempoResMesesres(),this.avalista.getTpResidenciares(),
    			this.avalista.getCepcom(),this.avalista.getLogradourocom(), this.avalista.getNumEndcom(),
    			this.avalista.getComplementocom(),this.avalista.getBairrocom(),this.avalista.getCidadecom(),
    			this.avalista.getUfcom(), this.avalista.getTempocomAnoscom(), this.avalista.getTempocomMesescom(),
    			this.avalista.getTpcomidenciacom(),this.avalista.getTelefoneRes(),this.avalista.getCelular(),
    			this.avalista.getTelefoneCom(),this.avalista.getTelefoneCom1(),this.avalista.getNumeroBeneficio(),
    			this.avalista.getNomeBeneficio(), this.avalista.getCriador(), this.avalista.getAlterado(),
    			this.avalista.getUsuinc(),this.avalista.getUsualt(), this.avalista.getInativo(), this.avalista.getCliente());
    	
    	Funcoes.alteraTitulo("Ficha - Avalista");
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaAddAvalista.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }
	
	public void removerAvalista(ActionEvent actionEvent) {
    	this.getCliente().getAvalistas().iterator().next().setCliente(null);
    	this.getCliente().setAvalistas(new ArrayList<Avalista>());
	}
	
	public void pesquisarCEPPJ() {
		CEPService service = new CEPService();
		
		String endereco = "";
		try {
			endereco = service.obterLogradouro(this.cliente.getCepPJ(), "ccmsistema", "123456");
			String[] result = endereco.split(",");
			
			this.cliente.setLogradouroPJ(result[0].trim());
			this.cliente.setBairroPJ(result[1].trim());
			this.cliente.setCidadePJ(result[2].trim());
			this.cliente.setUfPJ(result[3].trim());
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CEP não cadastrado", "CEP não cadastrado");
			FacesContext.getCurrentInstance().addMessage("cepPJ", message);	
		}
	}

	public boolean isCarroAbaixo2002() {
		return carroAbaixo2002;
	}

	public void setCarroAbaixo2002(boolean carroAbaixo2002) {
		this.carroAbaixo2002 = carroAbaixo2002;
	}

	public void validarAnoFabMod() {
		String ano = this.cliente.getAnoFabMod();
		
		String[] result = ano.split("/");
		
		if(Integer.parseInt(result[1])<Integer.parseInt(result[0])){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ano Fabricação/Ano Modelo inválido", "Ano Fabricação/Ano Modelo inválido");
			FacesContext.getCurrentInstance().addMessage("anoFabMod", message);	
		}
		
		if(Integer.parseInt(result[1])<=2002){
			this.carroAbaixo2002 = true;
		}else{
			this.carroAbaixo2002 = false;
		}
	}

	public void visualizarAvalista(ActionEvent actionEvent) {
    	this.avalista = this.getCliente().getAvalistas().iterator().next();
    	
    	Funcoes.alteraTitulo("Ficha - Avalista");
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaViewAvalista.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }
	
	public void voltarAvalista() {
    	Funcoes.alteraTitulo("Ficha");
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaVisualizar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public String getCnpjProc() {
		return cnpjProc;
	}

	public void setCnpjProc(String cnpjProc) {
		this.cnpjProc = cnpjProc;
	}

	public List<String> getListCargo() {
		listCargo = new ArrayList<String>();
		
		listCargo.add("Administrador");
		listCargo.add("Almoxarife");
		listCargo.add("Analista de Comércio Exterior");
		listCargo.add("Analista de Compras");
		listCargo.add("Analista de Contabilidade");
		listCargo.add("Analista de Controle de Qualidade");
		listCargo.add("Analista de Crédito e Cobrança");
		listCargo.add("Analista de Custo e Orçamento");
		listCargo.add("Analista de Dep. Pessoal / Folha de Pgto.");
		listCargo.add("Analista de Faturamento");
		listCargo.add("Analista de Garantia da Qualidade");
		listCargo.add("Analista de Help Desk");
		listCargo.add("Analista de Logística");
		listCargo.add("Analista de Marketing");
		listCargo.add("Analista de PCP");
		listCargo.add("Analista de Recursos Humanos");
		listCargo.add("Analista de Segurança de Sistemas");
		listCargo.add("Analista de Sistemas");
		listCargo.add("Analista de Suporte Técnico");
		listCargo.add("Analista de Tesouraria");
		listCargo.add("Analista de Vendas");
		listCargo.add("Analista Financeiro");
		listCargo.add("Analista Fiscal");
		listCargo.add("Assistente Administrativo");
		listCargo.add("Assistente de Administração de Vendas");
		listCargo.add("Assistente de Comércio Exterior");
		listCargo.add("Assistente de Compras");
		listCargo.add("Assistente de Contabilidade");
		listCargo.add("Assistente de Crédito e Cobrança");
		listCargo.add("Assistente de Dep. Pessoal");
		listCargo.add("Assistente de Faturamento");
		listCargo.add("Assistente de Garantia da Qualidade");
		listCargo.add("Assistente de Logística");
		listCargo.add("Assistente de Marketing");
		listCargo.add("Assistente de Recursos Humanos");
		listCargo.add("Assistente de Tesouraria");
		listCargo.add("Assistente de Vendas");
		listCargo.add("Assistente Financeiro");
		listCargo.add("Assistente Fiscal");
		listCargo.add("Auxiliar Administrativo");
		listCargo.add("Auxiliar de Almoxarifado");
		listCargo.add("Auxiliar de Comércio Exterior");
		listCargo.add("Auxiliar de Compras");
		listCargo.add("Auxiliar de Contabilidade");
		listCargo.add("Auxiliar de Controle de Qualidade");
		listCargo.add("Auxiliar de Crédito e Cobrança");
		listCargo.add("Auxiliar de Custo e Orçamento");
		listCargo.add("Auxiliar de Dep. Pessoal");
		listCargo.add("Auxiliar de Enfermagem do Trabalho");
		listCargo.add("Auxiliar de Escritório");
		listCargo.add("Auxiliar de Expedição");
		listCargo.add("Auxiliar de Logística");
		listCargo.add("Auxiliar de Manutenção");
		listCargo.add("Auxiliar de Marketing");
		listCargo.add("Auxiliar de PCP");
		listCargo.add("Auxiliar de Produção");
		listCargo.add("Auxiliar de Tesouraria");
		listCargo.add("Auxiliar de Vendas");
		listCargo.add("Auxiliar Financeiro");
		listCargo.add("Auxiliar Fiscal");
		listCargo.add("Comprador");
		listCargo.add("Coordenador de Comércio Exterior");
		listCargo.add("Coordenador de Compras");
		listCargo.add("Coordenador de Contabilidade");
		listCargo.add("Coordenador de Controle de Qualidade");
		listCargo.add("Coordenador de Crédito e Cobrança");
		listCargo.add("Coordenador de Custo e Orçamento");
		listCargo.add("Coordenador de Garantia da Qualidade");
		listCargo.add("Coordenador de Logística");
		listCargo.add("Coordenador de Manutenção");
		listCargo.add("Coordenador de Marketing");
		listCargo.add("Coordenador de PCP");
		listCargo.add("Coordenador de Produção");
		listCargo.add("Coordenador de Recursos Humanos");
		listCargo.add("Coordenador de Serviços Administrativos");
		listCargo.add("Coordenador de Suporte Técnico");
		listCargo.add("Coordenador de Teleatendimento");
		listCargo.add("Coordenador de Tesouraria");
		listCargo.add("Coordenador de TI");
		listCargo.add("Coordenador de Vendas");
		listCargo.add("Coordenador Financeiro");
		listCargo.add("Coordenador Fiscal");
		listCargo.add("Copeira");
		listCargo.add("Encarregado de Serviços Administrativos");
		listCargo.add("Estagiário (Penúltimo Ano)");
		listCargo.add("Estagiário (Último Ano)");
		listCargo.add("Faturista");
		listCargo.add("Inspetor de Controle de Qualidade");
		listCargo.add("Líder de Equipe (6 H)");
		listCargo.add("Médico do Trabalho (4 H)");
		listCargo.add("Mensageiro (Menor)");
		listCargo.add("Metrologista");
		listCargo.add("Motorista de Diretoria");
		listCargo.add("Motorista de Veículo Leve");
		listCargo.add("Operador de Teleatendimento (6 H)");
		listCargo.add("Porteiro");
		listCargo.add("Programador de Manutenção");
		listCargo.add("Programador de PCP");
		listCargo.add("Programador de Produção");
		listCargo.add("Secretária Bilíngüe");
		listCargo.add("Secretária de Diretoria");
		listCargo.add("Secretária Português");
		listCargo.add("Supervisor de Almoxarifado");
		listCargo.add("Supervisor de Controle de Qualidade");
		listCargo.add("Supervisor de Expedição");
		listCargo.add("Supervisor de Manutenção");
		listCargo.add("Supervisor de Produção");
		listCargo.add("Supervisor de Vendas");
		listCargo.add("Técnico de Informática");
		listCargo.add("Técnico de Segurança do Trabalho");
		listCargo.add("Telefonista / Recepcionista (8 H)");
		listCargo.add("Vendedor");
		listCargo.add("Gerente");
		listCargo.add("Diretor");
		listCargo.add("Empresário");
		listCargo.add("Outros cargos");
		
		return listCargo;
	}

	public void setListCargo(List<String> listCargo) {
		this.listCargo = listCargo;
	}

}
