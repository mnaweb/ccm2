package br.com.ccm.service;

import java.util.List;

import javax.ejb.Remote;

import br.com.ccm.model.Grupo;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Usuario;

@Remote
public interface IGrupoBean{
	
	public List<Grupo> listGrupo();
	
	public Grupo getGrupoById(Long id);
	
	public List<Modulo> listModulo(int qtdereg);
	
	public Grupo salvar(Grupo grupo, Usuario usuarioLogado);
	
	public void deletar(Grupo Grupo);
	
}
