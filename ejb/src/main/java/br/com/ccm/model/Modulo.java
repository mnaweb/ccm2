package br.com.ccm.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "ts_modulo")
public class Modulo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1481411763645158256L;
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "ds_titulo")
	private String titulo;
	@Column(name = "nr_tipo")
	private Long tipo;
	@Column(name = "ds_tarefa")
	private String tarefa;
	@Column(name = "ds_pagina1")
	private String pagina1;
	@Column(name = "ds_pagina2")
	private String pagina2;
	@Column(name = "nr_guia")
	private Long guia;
	@Column(name = "nr_posicao")
	private Long posicao;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@Column(name = "tg_inativo")
	private Long inativo;
	@OneToMany(targetEntity=Permissao.class,mappedBy="modulo",cascade=CascadeType.MERGE)
	private Collection<Permissao> permissoes;
	
	public Modulo() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Long getTipo() {
		return tipo;
	}
	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}
	public Long getGuia() {
		return guia;
	}
	public void setGuia(Long guia) {
		this.guia = guia;
	}
	public Long getPosicao() {
		return posicao;
	}
	public void setPosicao(Long posicao) {
		this.posicao = posicao;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Long getInativo() {
		return inativo;
	}
	public void setInativo(Long inativo) {
		this.inativo = inativo;
	}
	public Collection<Permissao> getPermissoes() {
		if(permissoes==null)
			permissoes = new ArrayList<Permissao>();
		
		return permissoes;
	}
	public void setPermissoes(Collection<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	public String getPagina1() {
		return pagina1;
	}
	public void setPagina1(String pagina1) {
		this.pagina1 = pagina1;
	}
	public String getPagina2() {
		return pagina2;
	}
	public void setPagina2(String pagina2) {
		this.pagina2 = pagina2;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}
	public String getTarefa() {
		return tarefa;
	}
	public void setTarefa(String tarefa) {
		this.tarefa = tarefa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Modulo other = (Modulo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
