package br.com.ccm.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;

import br.com.ccm.model.Avalista;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

@Remote
public interface IAvalistaBean{
	
	public List<Avalista> listAvalista();
	
	public Avalista getAvalistaById(Long id);

	public Avalista salvar(Avalista Avalista, Usuario usuarioLogado, Empresa empresa);
	
	public void deletar(Avalista Avalista);
	
	public ArrayList<Avalista> listaAll();

	public List<Avalista> listarAvalistaByCliente(Cliente cliente, int qtdereg);
	
}
