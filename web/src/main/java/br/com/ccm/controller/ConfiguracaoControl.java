package br.com.ccm.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IConfiguracaoSimuladorBean;
import br.com.ccm.service.IIntervaloBean;
import br.com.ccm.service.ITipoClienteBean;
import br.com.ccm.service.ITipoVeiculoBean;
import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.ConfiguracaoSimulador;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.TipoVeiculo;

@Named(value="mbConfiguracaoSimulador")
@ConversationScoped
public class ConfiguracaoControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5690636436369060328L;

	public ConfiguracaoControl(){
	}

	@EJB
	private IConfiguracaoSimuladorBean configuracaoSimuladorBean;
	
	@EJB
	private ITipoClienteBean tipoClienteBean;
	
	@EJB
	private ITipoVeiculoBean tipoVeiculoBean;
	
	@EJB
	private IIntervaloBean intervaloBean;

    private ConfiguracaoSimulador configuracaoSimulador = new ConfiguracaoSimulador();
    private Coeficiente coeficiente;
    private List<Coeficiente> coeficientes;
    
    private List<TipoVeiculo> listaTipoVeiculos;
    private List<TipoCliente> listaTipoClientes;
    private List<Intervalo> listaIntervalos;
    private List<String> listaPrazo;
    
	@Inject
	private Conversation conversation;
	
    @PostConstruct
    public void init(){
		if (conversation.isTransient()) {
			conversation.begin();

	    	List<ConfiguracaoSimulador> listAux= configuracaoSimuladorBean.listaAll();
			
			if(listAux==null || listAux.size()==0){
				configuracaoSimulador = new ConfiguracaoSimulador();
			}else{
				configuracaoSimulador = listAux.get(0);
			}
			
			setCoeficientes(configuracaoSimulador.getCoeficientes());
		}
    }
    
	public IConfiguracaoSimuladorBean getConfiguracaoSimuladorBean() {
		return configuracaoSimuladorBean;
	}

	public void setConfiguracaoSimuladorBean(IConfiguracaoSimuladorBean configuracaoSimuladorBean) {
		this.configuracaoSimuladorBean = configuracaoSimuladorBean;
	}

	public ConfiguracaoSimulador getConfiguracaoSimulador() {		
		return configuracaoSimulador;
	}

	public void setConfiguracaoSimulador(ConfiguracaoSimulador ConfiguracaoSimulador) {
		this.configuracaoSimulador = ConfiguracaoSimulador;
	}
	
	public ITipoClienteBean getTipoClienteBean() {
		return tipoClienteBean;
	}

	public void setTipoClienteBean(ITipoClienteBean tipoClienteBean) {
		this.tipoClienteBean = tipoClienteBean;
	}

	public ITipoVeiculoBean getTipoVeiculoBean() {
		return tipoVeiculoBean;
	}

	public void setTipoVeiculoBean(ITipoVeiculoBean tipoVeiculoBean) {
		this.tipoVeiculoBean = tipoVeiculoBean;
	}

	public IIntervaloBean getIntervaloBean() {
		return intervaloBean;
	}

	public void setIntervaloBean(IIntervaloBean intervaloBean) {
		this.intervaloBean = intervaloBean;
	}

	public Coeficiente getCoeficiente() {
		return coeficiente;
	}

	public void setCoeficiente(Coeficiente coeficiente) {
		this.coeficiente = coeficiente;
	}

	public void salvar(ActionEvent actionEvent){
		try {
			configuracaoSimuladorBean.salvar(configuracaoSimulador, getCoeficientes(), Funcoes.getUsuarioLogado());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return;
		}

		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro atualizado com sucesso.", "Registro atualizado com sucesso.");
		FacesContext.getCurrentInstance().addMessage(null, message);
		
		Funcoes.setarAcesso("configuracao");	
    	Funcoes.alteraPag1("simulador/configuracao.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
	}
	
	public void addCoeficiente(){
		if(getCoeficientes()==null)
			setCoeficientes(new ArrayList<Coeficiente>());
		
		getCoeficientes().add(new Coeficiente());
	}

	public List<TipoVeiculo> getListaTipoVeiculos() {
		listaTipoVeiculos = tipoVeiculoBean.listaAll();
		
		return listaTipoVeiculos;
	}

	public void setListaTipoVeiculos(List<TipoVeiculo> listaTipoVeiculos) {
		this.listaTipoVeiculos = listaTipoVeiculos;
	}

	public List<TipoCliente> getListaTipoClientes() {
		listaTipoClientes = tipoClienteBean.listaAll();
		
		return listaTipoClientes;
	}

	public void setListaTipoClientes(List<TipoCliente> listaTipoClientes) {
		this.listaTipoClientes = listaTipoClientes;
	}

	public List<Intervalo> getListaIntervalos() {
		listaIntervalos = intervaloBean.listaAll();
		
		return listaIntervalos;
	}

	public List<String> getListaPrazo() {
		listaPrazo = new ArrayList<String>();
		
		listaPrazo.add("12");
		listaPrazo.add("18");
		listaPrazo.add("24");
		listaPrazo.add("30");
		listaPrazo.add("36");
		listaPrazo.add("42");
		listaPrazo.add("48");
		listaPrazo.add("60");
		
		return listaPrazo;
	}

	public void setListaPrazo(List<String> listaPrazo) {
		this.listaPrazo = listaPrazo;
	}

	public void setListaIntervalos(List<Intervalo> listaIntervalos) {
		this.listaIntervalos = listaIntervalos;
	}
	
	public void removerCoeficiente(){
		
		Coeficiente reg = getCoeficiente();
		
		getCoeficientes().remove(reg);
	}

	public List<Coeficiente> getCoeficientes() {
		return coeficientes;
	}

	public void setCoeficientes(List<Coeficiente> coeficientes) {
		this.coeficientes = coeficientes;
	}
	
	public void rowEditListener(RowEditEvent event) throws AplicacaoException{

	    Coeficiente coe = (Coeficiente) event.getObject();
	    
	    for(Coeficiente obj : coeficientes){
	    	if(coe.getTipoCliente().equals(obj.getTipoCliente()) && 
	    		coe.getTipoVeiculo().equals(obj.getTipoVeiculo()) &&
	    		coe.getIntervalo().equals(obj.getIntervalo()) &&
	    		coe.getPrazo().equals(obj.getPrazo()) &&
	    		coe.isPontoDeEquilibrio()==obj.isPontoDeEquilibrio() &&
	    		!coe.getId().equals(obj.getId())){

		    		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ja existe um registro com os mesmos dados.", "Ja existe um registro com os mesmos dados.");
		    		FacesContext.getCurrentInstance().addMessage(null, message);
		    		
		    		throw new FacesException("Ja existe um registro com os mesmos dados.");	    		
	    	}
	    }
	}
	
}