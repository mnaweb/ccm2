package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.ITipoClienteDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.ITipoClienteBean;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.Usuario;

@Stateless
public class TipoClienteBean implements ITipoClienteBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private ITipoClienteDao dao;

	public ArrayList<TipoCliente> listaAll(){
		return (ArrayList<TipoCliente>) dao.listAllByOrder("descricao");
	}
	
	public TipoCliente salvar(TipoCliente tipoCliente, Usuario usuarioLogado) throws AplicacaoException{
		
		if(tipoCliente.getId()==0){
			tipoCliente.setInclusao(new Date());
			tipoCliente.setCriador(usuarioLogado);
			
			dao.save(tipoCliente);
		}else{
			tipoCliente.setAlteracao(new Date());
			
			dao.save(tipoCliente);
		}
		
		return tipoCliente;
	}
	
	public void deletar(TipoCliente tipoCliente){
		dao.delete(tipoCliente);
	}
	
	public TipoCliente getById(Long id){
		return dao.find(id);
	}
	
}
