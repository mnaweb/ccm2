package br.com.ccm.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value = "validatorBanco")
public class ValidatorBanco implements Validator{

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException { 
	    String msgErro = "Banco inválido!";
	    
	    if(value==null || value.equals("")){
	    	return;
	    }
	    
	    String numBanco = String.valueOf((Integer) value);

		String bancoValido = "204;654;246;356;25;641;m01;213;29;38;740;107;35;31;36;739;96;394;318;752;248;218;237;225;m15;44;263;473;222;412;40;266;745;241;m19;215;753;756;748;75;721;505;229;3;083-3;m21;707;300;495;494;m06;24;456;214;1;27;33;47;37;39;41;4;265;m03;224;626;175;252;m18;233;734;m07;612;m22;63;m11;604;320;653;630;249;341;376;74;217;76;757;600;212;243;m12;392;389;755;746;m10;738;66;151;45;m17;208;623;611;613;643;724;735;638;m24;747;633;741;m16;72;453;422;353;250;743;749;366;637;12;347;464;082-5;m20;634;m13;247;116;m14;m23;655;610;370;21;719;479;073-6;78;069-8;70;104;477;081-7;m08;487;751;210;64;62;399;492;488;65;254;";
		String[] listBanco = bancoValido.split(";");
		
		boolean retorno = false;
		for(String obj : listBanco){
			if(obj.equalsIgnoreCase(numBanco)){
				retorno = true;
				break;
			}
		}

		if(!retorno)
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msgErro, null)); 
	}
}
