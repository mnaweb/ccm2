package br.com.ccm.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IGrupoBean;
import br.com.ccm.service.IModuloBean;
import br.com.ccm.model.Grupo;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;

@Named(value="mbGrupo")
@SessionScoped
public class GrupoControl implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -5133007025724256510L;
	
	private String tableState = null;
    private List<Grupo> dataModel;
    private List<Permissao> dataModelItem;
    private Grupo selection = new Grupo();
    private Permissao selectionItem = new Permissao();
    private Comparator<Grupo> dateInclusao;
    private Comparator<Grupo> dateAlteracao;
    
    @EJB
    private IGrupoBean grupoBean;
    
    private Grupo grupo = new Grupo();
    private Permissao permissao;
    private List<SelectItem> moduloLista = new ArrayList<SelectItem>();
    private long idTemp;

    public GrupoControl(){
    }

	public IGrupoBean getGrupoBean() {
		return grupoBean;
	}

	public void setGrupoBean(IGrupoBean grupoBean) {
		this.grupoBean = grupoBean;
	}
	
	public Comparator<Grupo> getDateInclusao(){
        if (dateInclusao == null){
            dateInclusao = new Comparator<Grupo>(){
                public int compare(Grupo o1, Grupo o2) {
                    return o1.getInclusao().compareTo(o2.getInclusao());
                }
            };
        }
        return dateInclusao;
    }

    public Comparator<Grupo> getDateAlteracao(){
        if (dateAlteracao == null){
            dateAlteracao = new Comparator<Grupo>(){
                public int compare(Grupo o1, Grupo o2) {
                    return o1.getAlteracao().compareTo(o2.getAlteracao());
                }
            };
        }
        return dateAlteracao;
    }

    public String getTableState() {
        if (tableState == null){
            //try to get state from cookies
            Cookie[] cookies = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getCookies();
            if (cookies != null){
                for (Cookie c : cookies){
                    if (c.getName().equals("extdtGrupo")){
                    	tableState = c.getValue();
                        break;
                    }
                }
            }
        }
        return tableState;
    }

    public void setTableState(String tableState) {
    	this.tableState = tableState;
    	//save state in cookies
		Cookie stateCookie = new Cookie("extdGrupo", this.tableState);
		stateCookie.setMaxAge(30 * 24 * 60 * 60);
		((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(stateCookie);
    }
    
    public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public Grupo getSelection() {
        return selection;
    }
	
    public long getIdTemp() {
		return idTemp;
	}

	public void setIdTemp(long idTemp) {
		this.idTemp = idTemp;
	}

	public void setSelection(Grupo selection) {
        this.selection = selection;
    }

	public Permissao getSelectionItem() {
		return selectionItem;
	}

	public void setSelectionItem(Permissao selectionItem) {
		this.selectionItem = selectionItem;
	}

	public List<Grupo> getDataModel(){
    	
		List<Grupo> lista = grupoBean.listGrupo();
    	
		dataModel = lista;
        return dataModel;
    }

	public void setDataModel(List<Grupo> dataModel) {
		this.dataModel = dataModel;
	}

	public String salvar(){
		
		try{
			for(Permissao p : grupo.getPermissoes()){
				if(p.getId()<0){
					p.setId(0L);
				}
			}
		}catch (Exception e) {}
		
		grupoBean.salvar(grupo, Funcoes.getUsuarioLogado());
		
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public void deletar(ActionEvent actionEvent){
		grupoBean.deletar(getSelection());
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.grupo = new Grupo();
    	
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar(ActionEvent actionEvent) {

    	Grupo grupoAux = getSelection();
    	
		this.grupo = grupoBean.getGrupoById(grupoAux.getId());
		
		this.grupo.getPermissoes();
		
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    // Itens da permissão
	public List<Permissao> getDataModelItem(){
		List<Permissao> lista = new ArrayList<Permissao>();
		
		try{
			if(grupo.getPermissoes().size()<=0 && grupo.getId()!=0){
				this.grupo = grupoBean.getGrupoById(grupo.getId());
				lista = (List<Permissao>)grupo.getPermissoes();
			}else{
				lista = (List<Permissao>)grupo.getPermissoes();
			}
		}catch (Exception e) {
			if(grupo.getId()!=0){
				this.grupo = grupoBean.getGrupoById(grupo.getId());
				lista = (List<Permissao>)grupo.getPermissoes();
			}	
			
		}
		
		dataModelItem = lista;
        return dataModelItem;
    }
	
	public void setDataModelItem(List<Permissao> dataModelItem) {
		this.dataModelItem = dataModelItem;
	}
    
	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	public List<SelectItem> getModuloLista() {
		moduloLista = new ArrayList<SelectItem>();
		
		List<Modulo> lista = grupoBean.listModulo(Funcoes.getQtdeRegMax());
		
		moduloLista.add(new SelectItem(new Modulo(),"Selecione..."));
		
		for(Modulo reg:lista){
			if(reg.getTipo()==IModuloBean.TIPO_TAREFA_MENU)
				moduloLista.add(new SelectItem(reg,reg.getTitulo()));
			else
				moduloLista.add(new SelectItem(reg,"-   "+reg.getTitulo()));
		}
		return moduloLista;
	}

	public void setModuloLista(List<SelectItem> moduloLista) {
		this.moduloLista = moduloLista;
	}

	public String salvarItem(){
		if(permissao.getId()==0){
			this.idTemp--;
			permissao.setId(this.idTemp);
			grupo.addPermissao(permissao);
		}
		
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		return "crud";
	}

	public String cancelarItem(){
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public void deletarItem(ActionEvent actionEvent){
		permissao = getSelectionItem();
		grupo.getPermissoes().remove(permissao);
	}
	
	public void criarItem(ActionEvent actionEvent) {
    	this.permissao = new Permissao();
    	
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editarItem(ActionEvent actionEvent) {
		this.permissao = getSelectionItem();
		
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

}