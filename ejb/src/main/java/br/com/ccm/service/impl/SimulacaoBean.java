package br.com.ccm.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.ISimulacaoDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.ICoeficienteBean;
import br.com.ccm.service.ISimulacaoBean;
import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.Simulacao;
import br.com.ccm.model.Usuario;

@Stateless
public class SimulacaoBean implements ISimulacaoBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private ISimulacaoDao dao;
	
	@EJB
	private ICoeficienteBean coeficienteBean;
	
	public ArrayList<Simulacao> listaAll(){
		return (ArrayList<Simulacao>) dao.findAll();
	}

	@Override
	public Simulacao calcular(Simulacao simulacao, Usuario usuarioLogado, int qtdereg) throws AplicacaoException{
		BigDecimal valorFinanciamento = simulacao.getValorVeiculo().subtract(simulacao.getValorEntrada());
		
		BigDecimal percPontoDeEquilibrio = simulacao.getValorEntrada().divide(simulacao.getValorVeiculo(), BigDecimal.ROUND_HALF_EVEN);
		percPontoDeEquilibrio = new BigDecimal(1).subtract(percPontoDeEquilibrio);
		percPontoDeEquilibrio = percPontoDeEquilibrio.add(new BigDecimal(-0.00001));
		percPontoDeEquilibrio = percPontoDeEquilibrio.multiply(new BigDecimal(100));
		
		List<Coeficiente> listCoeficiente = coeficienteBean.listaCoeficiente(simulacao.getTipoCliente(), simulacao.getTipoVeiculo(),
																				simulacao.getIntervalo(), percPontoDeEquilibrio, qtdereg);
		
		for(Coeficiente coe : listCoeficiente){
			int prazo = Integer.parseInt(coe.getPrazo());
			
			switch (prazo) {
				case 12:
					simulacao.setPrazo12((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
				case 18:
					simulacao.setPrazo18((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
				case 24:
					simulacao.setPrazo24((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
				case 30:
					simulacao.setPrazo30((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
				case 36:
					simulacao.setPrazo36((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
				case 42:
					simulacao.setPrazo42((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
				case 48:
					simulacao.setPrazo48((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
				case 60:
					simulacao.setPrazo60((valorFinanciamento.add(coe.getConfiguracaoSimulador().getVlTc())).multiply(coe.getVlCoeficiente()));
					break;
			}
		}
		
		this.salvar(simulacao, usuarioLogado);
		
		return simulacao;
	}
		
	public Simulacao salvar(Simulacao simulacao, Usuario usuarioLogado) throws AplicacaoException{
		
		if(simulacao.getId()==0){
			simulacao.setInclusao(new Date());
			simulacao.setCriador(usuarioLogado);
			
			dao.save(simulacao);
		}else{
			simulacao.setAlteracao(new Date());
			
			dao.save(simulacao);
		}
		
		return simulacao;
	}
	
	public void deletar(Simulacao simulacao){
		dao.delete(simulacao);
	}
	
	public Simulacao getById(Long id){
		return dao.find(id);
	}

}