package br.com.ccm.dao;

import java.util.List;

import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

public interface IUsuarioDao extends IGenericDAO<Usuario>{
	
	public Usuario getUsuarioNomeESenha(String nome, String senha, Usuario usuarioLogado, Empresa empresa, int qtdeReg);
	
	public List<Usuario> listUsuarioPorVendedor(Vendedor vendedor, int qtdeReg);

	public List<Usuario> listAll(Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	public Usuario getUsuarioEmDuplicidade(String nome, Empresa empresa, Long id, int qtdeReg);

}
