package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.IObsDao;
import br.com.ccm.service.IObsBean;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class ObsBean implements IObsBean,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7985941462307960864L;
	
	@EJB
	private IObsDao dao;

	@Override
	public List<Obs> listObsVendedor(Vendedor vendedor, int lido, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.listObsVendedor(vendedor, lido, usuarioLogado,empresa, qtdereg);
	}

	@Override
	public long getQtdeObsParaVendedor(Vendedor vendedor, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.getQtdeObsParaVendedor(vendedor, usuarioLogado,empresa, qtdereg);
	}

	@Override
	public List<Obs> listObsFichaVendedor(Vendedor vendedor, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.listObsFichaVendedor(vendedor, fichaId, usuarioLogado,empresa, qtdereg);
	}
	
	@Override
	public Obs getById(Long id){
		return dao.find(id);
	}

	@Override
	public long getQtdeObsParaCcm(Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.getQtdeObsParaCcm(usuarioLogado,empresa, qtdereg);
	}
	
	@Override
	public List<Obs> listObsCcm( int lido, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.listObsCcm(lido,usuarioLogado,empresa, qtdereg);
	}

	@Override
	public List<Obs> listObsLocatario( int lido, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.listObsLocatario(lido,usuarioLogado,empresa, qtdereg);
	}

	@Override
	public void salvarObs(Obs obs, Usuario usuarioLogado){
		Usuario usu = usuarioLogado;
		//Loja
		if(!usu.getVendedor().isCcm()){
			obs.setAvisarCcm(true);
			obs.setAvisarLocatario(true);
		}
		//Locador
		if(!usu.getAdmin() && usu.getVendedor().isCcm()){
			obs.setAvisarCcm(true);
		}
		dao.save(obs);
	}

	@Override
	public long getQtdeObsParaLocatario(Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.getQtdeObsParaLocatario(usuarioLogado,empresa, qtdereg);
	}

	@Override
	public List<Obs> listObsFichaLocatario(Empresa empresaPortal, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.listObsFichaLocatario(empresaPortal, fichaId,usuarioLogado,empresa, qtdereg);
	}

}
