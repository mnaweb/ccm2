package br.com.ccm.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.ejb.Remote;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.Usuario;

@Remote
public interface IPrincipalBean{
	
	public Usuario logar(String nome, String senha, Empresa empresa, int qtdereg) throws NoSuchAlgorithmException;

	public List<Modulo> getListModulo(Usuario usuario, int qtdereg);

	public List<Permissao> listPermissaoUsuario(Usuario usuario, String tarefa, int qtdereg);

}