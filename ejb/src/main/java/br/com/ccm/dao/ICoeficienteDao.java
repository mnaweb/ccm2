package br.com.ccm.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.TipoVeiculo;

@Local
public interface ICoeficienteDao extends IGenericDAO<Coeficiente>{

	List<Coeficiente> listaCoeficiente(TipoCliente tipoCliente, TipoVeiculo tipoVeiculo, Intervalo intervalo, BigDecimal percPontoDeEquilibrio, int qtdeReg);

}
