package br.com.ccm.dao.impl;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.ccm.dao.ITipoClienteDao;
import br.com.ccm.model.TipoCliente;

@Stateless
public class TipoClienteDao extends GenericDAO<TipoCliente> implements ITipoClienteDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460340813643414204L;

	public TipoClienteDao() {
		super(TipoCliente.class);
	}

}
