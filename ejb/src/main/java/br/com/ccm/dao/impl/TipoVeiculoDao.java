package br.com.ccm.dao.impl;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.ccm.dao.ITipoVeiculoDao;
import br.com.ccm.model.TipoVeiculo;

@Stateless
public class TipoVeiculoDao extends GenericDAO<TipoVeiculo> implements ITipoVeiculoDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460340813643414204L;

	public TipoVeiculoDao() {
		super(TipoVeiculo.class);
	}

}
