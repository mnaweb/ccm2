package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IEmpresaDao;
import br.com.ccm.model.Empresa;

@Stateless
public class EmpresaDao extends GenericDAO<Empresa> implements IEmpresaDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7951141855800422481L;

	public EmpresaDao() {
		super(Empresa.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Empresa> listAllByAtivo(int qtdeReg) {
		return execQuery(" from Empresa where ativo=true order by nome",null,qtdeReg);
	}
}
