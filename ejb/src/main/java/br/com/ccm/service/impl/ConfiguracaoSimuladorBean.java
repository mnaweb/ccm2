package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.ccm.dao.IConfiguracaoSimuladorDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.ICoeficienteBean;
import br.com.ccm.service.IConfiguracaoSimuladorBean;
import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.ConfiguracaoSimulador;
import br.com.ccm.model.Usuario;

@Stateless
public class ConfiguracaoSimuladorBean implements IConfiguracaoSimuladorBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private ICoeficienteBean coeficienteBean;
		
	@EJB
	private IConfiguracaoSimuladorDao dao;
	
	public ArrayList<ConfiguracaoSimulador> listaAll(){
		return (ArrayList<ConfiguracaoSimulador>) dao.findAll();
	}
	
	public ConfiguracaoSimulador salvar(ConfiguracaoSimulador configuracaoSimulador, List<Coeficiente> coeficientes, Usuario usuarioLogado) throws AplicacaoException{
		
		configuracaoSimulador.setCoeficientes(null);
		for(Coeficiente obj : coeficientes){
			Coeficiente cloneObj = (Coeficiente) obj.clone();
			configuracaoSimulador.addCoeficiente(cloneObj);
		}
		
		try{
			ConfiguracaoSimulador configuracaoSimuladorOld = dao.find(configuracaoSimulador.getId());
			
			coeficiente:
			for(Coeficiente old : configuracaoSimuladorOld.getCoeficientes()){
				for(Coeficiente newObj : configuracaoSimuladorOld.getCoeficientes()){
					if(old.getId()==newObj.getId()){
						continue coeficiente;///?? mesmo coeficiente j?? cadastrado. Retorna para o proximo.
					}
				}
				coeficienteBean.deletar(old);///Remove item que n??o faz mais parte da configura????o
			}
		}catch (Exception e) {}
		
		if(configuracaoSimulador.getId()==0){
			configuracaoSimulador.setInclusao(new Date());
			configuracaoSimulador.setCriador(usuarioLogado);
			
			dao.save(configuracaoSimulador);
		}else{
			configuracaoSimulador.setAlteracao(new Date());
			
			dao.save(configuracaoSimulador);
		}
		
		return configuracaoSimulador;
	}
	
	public void deletar(ConfiguracaoSimulador configuracaoSimulador){
		dao.delete(configuracaoSimulador);
	}
	
	public ConfiguracaoSimulador getById(Long id){
		return dao.find(id);
	}

}
