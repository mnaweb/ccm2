package br.com.ccm.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import br.com.ccm.dao.IEmpresaDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.IEmpresaBean;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

@Stateless
public class EmpresaBean implements IEmpresaBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private IEmpresaDao dao;

	@Override
	public ArrayList<Empresa> listaAll(){
		return (ArrayList<Empresa>) dao.listAllByOrder("nome");
	}
	
	@Override
	public ArrayList<Empresa> listAllByAtivo(int qtdereg){
		return (ArrayList<Empresa>) dao.listAllByAtivo(qtdereg);
	}
	
	public Empresa salvar(Empresa empresa, Usuario usuarioLogado) throws AplicacaoException{
		if(empresa.getImage()==null){
			throw new AplicacaoException("O logotipo ?? obrigat??rio!");
		}
		
		if(empresa.getId()==0){
			empresa.setInclusao(new Date());
			empresa.setCriador(usuarioLogado);
			
			dao.save(empresa);
		}else{
			empresa.setAlteracao(new Date());
			
			dao.save(empresa);
		}
		
		this.criarPasta(empresa);
		
		return empresa;
	}
	
	public void deletar(Empresa empresa){
		dao.delete(empresa);
	}
	
	public void criarPasta(Empresa empresa){
		HttpServletRequest request = (HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String realPath = request.getSession().getServletContext().getRealPath("");
		
		String arquivoCli = realPath+"/"+empresa.getUrl()+"/index.jsp"; 
		
		if(!new File(arquivoCli).exists()){

            // cria diretorio
			File tmpDir = new File(realPath+"/"+empresa.getUrl());
            if (!tmpDir.exists())
                tmpDir.mkdirs();

            File arquivoDest = new File(arquivoCli);
            try {
				FileOutputStream fos = new FileOutputStream(arquivoDest);
				PrintStream p = new PrintStream(fos);
	            
				p.println("<html>");
				p.println("<head>");
				p.println("<title>"+empresa.getNome()+"</title>");
				p.println("</head>");
				p.println("<frameset>");
				p.println("<frame src=\"<%=request.getContextPath()%>/login.jsf?idEmpresa="+empresa.getId()+"\" scrolling='yes'/>");
				p.println("<noframes>");
				p.println("<body>Este site utiliza recursos (frame) n??o suportados pelo");
				p.println("seu browser.");
				p.println("</body>");
				p.println("</noframes>");
				p.println("</frameset>");
				p.println("</html>");

	            p.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}  

		}
		
		String arquivoImg = realPath+"/images/Logo_"+empresa.getUrl()+".png"; 

		if(!new File(arquivoImg).exists()){
			(new File(arquivoImg)).delete();
		}
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(arquivoImg);
            fos.write(empresa.getImage());
            fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Empresa getById(Long id){
		return dao.find(id);
	}

}
