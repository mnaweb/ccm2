package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.ITipoVeiculoDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.ITipoVeiculoBean;
import br.com.ccm.model.TipoVeiculo;
import br.com.ccm.model.Usuario;

@Stateless
public class TipoVeiculoBean implements ITipoVeiculoBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private ITipoVeiculoDao dao;

	public ArrayList<TipoVeiculo> listaAll(){
		return (ArrayList<TipoVeiculo>) dao.listAllByOrder("descricao");
	}
	
	public TipoVeiculo salvar(TipoVeiculo tipoVeiculo, Usuario usuarioLogado) throws AplicacaoException{
		
		if(tipoVeiculo.getId()==0){
			tipoVeiculo.setInclusao(new Date());
			tipoVeiculo.setCriador(usuarioLogado);
			
			dao.save(tipoVeiculo);
		}else{
			tipoVeiculo.setAlteracao(new Date());
			
			dao.save(tipoVeiculo);
		}
		
		return tipoVeiculo;
	}
	
	public void deletar(TipoVeiculo tipoVeiculo){
		dao.delete(tipoVeiculo);
	}
	
	public TipoVeiculo getById(Long id){
		return dao.find(id);
	}

}