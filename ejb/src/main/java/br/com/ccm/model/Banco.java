package br.com.ccm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cd_banco")
public class Banco implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2712478858905882541L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_id")
	private Long id;
	@Column(name = "nr_banco")
	private long numBanco;
	@Column(name = "ds_nome")
	private String nome;
	@Column(name = "ds_email")
	private String email;
	@Column(name = "ds_operador")
	private String operador;
	@Column(name = "ds_telefone1")
	private String telefone1;
	@Column(name = "ds_telefone2")
	private String telefone2;
	@Column(name = "ds_centralDeRelacionamento")
	private String centralDeRelacionamento;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@ManyToOne
	@JoinColumn(name="fk_empresaPortal")
	private Empresa empresaPortal;
	
	public Banco() {
		id = new Long(0);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getNumBanco() {
		return numBanco;
	}
	public void setNumBanco(long numBanco) {
		this.numBanco = numBanco;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}
	public Empresa getEmpresaPortal() {
		return empresaPortal;
	}
	public void setEmpresaPortal(Empresa empresaPortal) {
		this.empresaPortal = empresaPortal;
	}
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public String getTelefone1() {
		return telefone1;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public String getTelefone2() {
		return telefone2;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	public String getCentralDeRelacionamento() {
		return centralDeRelacionamento;
	}
	public void setCentralDeRelacionamento(String centralDeRelacionamento) {
		this.centralDeRelacionamento = centralDeRelacionamento;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banco other = (Banco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
