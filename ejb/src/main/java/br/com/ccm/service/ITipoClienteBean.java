package br.com.ccm.service;

import java.util.ArrayList;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.Usuario;

@Remote
public interface ITipoClienteBean{

	public ArrayList<TipoCliente> listaAll();
	
	public TipoCliente salvar(TipoCliente tipoCliente, Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(TipoCliente tipoCliente);
	
	public TipoCliente getById(Long id);

}
