package br.com.ccm.exception;

public class AplicacaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8047027798057101939L;

	private String message;

	public AplicacaoException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
