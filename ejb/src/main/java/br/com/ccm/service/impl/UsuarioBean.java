package br.com.ccm.service.impl;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.IUsuarioDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.IBancoBean;
import br.com.ccm.service.IClienteBean;
import br.com.ccm.service.IUsuarioBean;
import br.com.ccm.service.IVendedorBean;
import br.com.ccm.util.Funcoes;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class UsuarioBean implements IUsuarioBean,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5321817951932157341L;

	@EJB
	private IUsuarioDao dao;
	
	@EJB
	private IClienteBean clienteBean;

	@EJB
	private IBancoBean bancoBean;
	
	@EJB
	private IVendedorBean vendedorBean;
	
	public Usuario getUsuarioNomeESenha(String nome, String senha, Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return dao.getUsuarioNomeESenha(nome, senha, usuarioLogado, empresa, qtdereg);
	}

	public ArrayList<Usuario> listaAll(){
		return (ArrayList<Usuario>) dao.findAll();
	}
	
	public void salvar(Usuario usuario, Usuario usuarioLogado, Empresa empresa, int qtdereg) throws NoSuchAlgorithmException, AplicacaoException{
		
		if(usuario.getSenha().length()<=10)
			usuario.setSenha(Funcoes.md5(usuario.getSenha()));
		
		if(usuario.getEmpresaPortal()==null){
			usuario.setEmpresaPortal(empresa);
		}
		
		if(usuario.getEmpresaPortal().getId()==0 && usuarioLogado.getAdmin()){
			usuario.setEmpresaPortal(null);
		}
		
		if(usuario.getId()==0){
			usuario.setInclusao(new Date());
		}else{
			usuario.setAlteracao(new Date());
		}
		
		Usuario usuDup = this.getUsuarioEmDuplicidade(usuario.getLogin(), usuario.getEmpresaPortal(), usuario.getId(), qtdereg);
		
		if(usuDup!=null){
			throw new AplicacaoException("J?? existe um usu??rio com este login!");
		}
		
		dao.save(usuario);
	}

	public void deletar(Usuario usuario, int qtdereg) throws AplicacaoException{
		String msg = "N??o foi poss??vel excluir.\nOs seguintes itens est??o vinculados:\n";

		List<Cliente> list = clienteBean.listFichasByUsuario(usuario, qtdereg);
		if(list.size()>0){
			msg = msg +"\nFichas:\n";
			int mensagem = 0;
			for (Cliente cliente : list) {				
				msg=msg+"- "+cliente.getId()+"\n";
				mensagem++;
				if(mensagem==3) break;
			}
			throw new AplicacaoException(msg);
		}

		List<Vendedor> listVen = vendedorBean.listVendedorByUsuario(usuario, qtdereg);
		if(listVen.size()>0){
			msg = msg +"\nVendedor:\n";
			int mensagem = 0;
			for (Vendedor vendedor : listVen) {				
				msg=msg+"- "+vendedor.getNome()+"\n";
				mensagem++;
				if(mensagem==3) break;
			}
			throw new AplicacaoException(msg);
		}

		List<Banco> listBan = bancoBean.listBancosByUsuario(usuario, qtdereg);
		if(listBan.size()>0){
			msg = msg +"\nVendedor:\n";
			int mensagem = 0;
			for (Banco banco : listBan) {				
				msg=msg+"- "+banco.getNome()+"\n";
				mensagem++;
				if(mensagem==3) break;
			}
			throw new AplicacaoException(msg);
		}
		
		dao.delete(usuario);
	}

	public Usuario getById(Long id){
		return dao.find(id);
	}

	public List<Usuario> listUsuarioPorVendedor(Vendedor vendedor, int qtdereg){
		return dao.listUsuarioPorVendedor(vendedor,qtdereg);
	}

	public Usuario getUsuarioEmDuplicidade(String nome, Empresa empresa, Long id, int qtdereg){
		return dao.getUsuarioEmDuplicidade(nome, empresa, id, qtdereg);
	}
}
