package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IObsDao;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class ObsDao extends GenericDAO<Obs> implements IObsDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5302619209947459760L;

	public ObsDao() {
		super(Obs.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Obs> listObsVendedor(Vendedor vendedor, int lido, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";
		if(vendedor!=null){
			where += " and o.cliente.vendedor.id = :venId";
			params.put("venId",new Long(vendedor.getId()));
			
			if(vendedor.isApresentarFichasUsuario() && vendedor.getId()==usuarioLogado.getVendedor().getId()){
				where += " and o.cliente.criador.id = :usuId";
				params.put("usuId",new Long(usuarioLogado.getId()));
			}
		}

		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and o.cliente.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		switch(lido){
			case 1: ///Lido
				where += " and o.lido = :lido";
				params.put("lido",new Boolean(true));
			case 2: ///No Lido
				where += " and o.lido = :lido";
				params.put("lido",new Boolean(false));
		}
		
		where += " and o.avisarVendedor = :avisarVendedor";
		params.put("avisarVendedor",new Boolean(true));
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Obs> listaCli = execQuery("Select o from Obs o "+where+" order by o.inclusao desc", params, qtdeReg);

		return listaCli;
	}

	@Override
	public long getQtdeObsParaVendedor(Vendedor vendedor, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where o.cliente.vendedor.id = :venId and o.lido = :lido and o.avisarVendedor = :avisarVendedor";
		params.put("venId",new Long(vendedor==null?0:vendedor.getId()));
		if(vendedor!=null && vendedor.isApresentarFichasUsuario() && vendedor.getId()==usuarioLogado.getVendedor().getId()){
			where += " and o.cliente.criador.id = :usuId";
			params.put("usuId",new Long(usuarioLogado.getId()));
		}
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and o.cliente.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		params.put("lido",new Boolean(false));
		params.put("avisarVendedor",new Boolean(true));
		
		Long qtdeFichas = (Long) execQueryObj("Select count(o.id) from Obs o "+where, params, qtdeReg);

		return qtdeFichas;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Obs> listObsFichaVendedor(Vendedor vendedor, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";
		if(vendedor!=null){
			where += " and o.cliente.vendedor.id = :venId";
			params.put("venId",new Long(vendedor.getId()));
			
			if(vendedor.isApresentarFichasUsuario() && vendedor.getId()==usuarioLogado.getVendedor().getId()){
				where += " and o.cliente.criador.id = :usuId";
				params.put("usuId",new Long(usuarioLogado.getId()));
			}
		}

		if(fichaId!=0){
			where += " and o.cliente.id = :fichaId";
			params.put("fichaId",new Long(fichaId));
		}
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and o.cliente.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		where += " and (o.avisarCcm = :avisarCcm or o.avisarVendedor = :avisarVendedor)";
		params.put("avisarCcm",new Boolean(true));
		params.put("avisarVendedor",new Boolean(true));
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Obs> listaCli = execQuery("Select o from Obs o "+where+" order by o.inclusao desc", params, qtdeReg);

		return listaCli;
	}

	@Override
	public long getQtdeObsParaCcm(Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where o.lido = :lido and o.avisarCcm = :avisarCcm";
		params.put("lido",new Boolean(false));
		params.put("avisarCcm",new Boolean(true));
		
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and o.cliente.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		Long qtdeFichas = (Long) execQueryObj("Select count(o.id) from Obs o "+where, params, qtdeReg);

		return qtdeFichas;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Obs> listObsCcm(int lido, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";

		switch(lido){
			case 1: ///Lido
				where += " and o.lido = :lido";
				params.put("lido",new Boolean(true));
			case 2: ///Nao Lido
				where += " and o.lido = :lido";
				params.put("lido",new Boolean(false));
		}
		
		where += " and o.avisarCcm = :avisarCcm";
		params.put("avisarCcm",new Boolean(true));
		
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and o.cliente.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Obs> listaCli = execQuery("Select o from Obs o "+where+" order by o.inclusao desc", params, qtdeReg);

		return listaCli;
	}

	@Override
	public long getQtdeObsParaLocatario(Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = " where o.lidoLocatario = :lido and o.avisarLocatario = :avisarLocatario";
		params.put("lido",new Boolean(false));
		params.put("avisarLocatario",new Boolean(true));
		
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and o.cliente.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		Long qtdeFichas = (Long) execQueryObj("Select count(o.id) from Obs o "+where, params, qtdeReg);

		return qtdeFichas;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Obs> listObsLocatario(int lido, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";

		switch(lido){
			case 1: ///Lido
				where += " and o.lidoLocatario = :lido";
				params.put("lido",new Boolean(true));
			case 2: ///Nao Lido
				where += " and o.lidoLocatario = :lido";
				params.put("lido",new Boolean(false));
		}
		
		where += " and o.avisarLocatario = :avisarLocatario";
		params.put("avisarLocatario",new Boolean(true));
		
		if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
			where += " and o.cliente.empresaPortal.id = :idEmp";
			params.put("idEmp",empresa.getId());
		}

		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Obs> listaCli = execQuery("Select o from Obs o "+where+" order by o.inclusao desc", params, qtdeReg);

		return listaCli;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Obs> listObsFichaLocatario(Empresa empresaPortal, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";
		if(empresaPortal!=null){
			where += " and o.cliente.empresaPortal.id = :empId";
			params.put("empId",new Long(empresaPortal.getId()));
		}

		if(fichaId!=0){
			where += " and o.cliente.id = :fichaId";
			params.put("fichaId",new Long(fichaId));
		}

		where += " and (o.avisarLocatario = :avisarLocatario or o.avisarVendedor = :avisarVendedor)";
		params.put("avisarLocatario",new Boolean(true));
		params.put("avisarVendedor",new Boolean(true));
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Obs> listaCli = execQuery("Select o from Obs o "+where+" order by o.inclusao desc", params, qtdeReg);

		return listaCli;
	}

}