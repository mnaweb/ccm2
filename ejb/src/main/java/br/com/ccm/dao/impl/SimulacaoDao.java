package br.com.ccm.dao.impl;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.ccm.dao.ISimulacaoDao;
import br.com.ccm.model.Simulacao;

@Stateless
public class SimulacaoDao extends GenericDAO<Simulacao> implements ISimulacaoDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460340813643414204L;

	public SimulacaoDao() {
		super(Simulacao.class);
	}

}
