package br.com.ccm.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IGrupoBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.service.IUsuarioBean;
import br.com.ccm.service.IVendedorBean;
import br.com.ccm.model.Grupo;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Named(value="mbUsuario")
@ConversationScoped
public class UsuarioControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3792293837000402075L;

	public UsuarioControl(){
	}

	@EJB
	private IUsuarioBean usuarioBean;
	@EJB
	private IVendedorBean vendedorBean;
	@EJB
	private IGrupoBean grupoBean;
	@EJB
	private IPrincipalBean principalBean;
	
    private Comparator<Usuario> dateInclusao;
    private Comparator<Usuario> dateAlteracao;
    private List<Usuario> dataModel;
    private Usuario selection = new Usuario();
    private Usuario usuario = new Usuario();
    private Usuario usuarioTrocarSenha;
	private List<SelectItem> vendedorLista = new ArrayList<SelectItem>(); 
	private List<SelectItem> grupoLista = new ArrayList<SelectItem>();

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario Usuario) {
		this.usuario = Usuario;
	}

	public List<Usuario> getDataModel(){
    	
		List<Usuario> lista = usuarioBean.listaAll();
    	
		dataModel = lista;
        return dataModel;
    }

    public Usuario getSelection() {
        return selection;
    }

    public void setSelection(Usuario selection) {
        this.selection = selection;
    }
    
    public Comparator<Usuario> getDateInclusao(){
        if (dateInclusao == null){
            dateInclusao = new Comparator<Usuario>(){
                public int compare(Usuario o1, Usuario o2) {
                    return o1.getInclusao().compareTo(o2.getInclusao());
                }
            };
        }
        return dateInclusao;
    }

    public Comparator<Usuario> getDateAlteracao(){
        if (dateAlteracao == null){
            dateAlteracao = new Comparator<Usuario>(){
                public int compare(Usuario o1, Usuario o2) {
                    return o1.getAlteracao().compareTo(o2.getAlteracao());
                }
            };
        }
        return dateAlteracao;
    }
    
	public String salvar() throws NoSuchAlgorithmException{
		try {
			usuarioBean.salvar(usuario, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), "login");
			return null;
		}
		
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String deletar(ActionEvent actionEvent){
		
		usuario = getSelection();
		
		try{
			usuarioBean.deletar(usuario, Funcoes.getQtdeRegMax());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}
		
		conversation.end();
		
		return "usuarioConsultar";
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.usuario = new Usuario();

    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar(ActionEvent actionEvent) {
		this.usuario = getSelection();

		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public List<SelectItem> getVendedorLista() {
		vendedorLista = new ArrayList<SelectItem>();

		List<Vendedor> listAux = vendedorBean.listaAll();
		for(Vendedor obj:listAux){
			vendedorLista.add(new SelectItem(obj,obj.getNome()));
		}
		
		return vendedorLista;
	}

	public void setVendedorLista(List<SelectItem> vendedorLista) {
		this.vendedorLista = vendedorLista;
	}

	public List<SelectItem> getGrupoLista() {
		grupoLista = new ArrayList<SelectItem>();

		List<Grupo> listAux = grupoBean.listGrupo();
		for(Grupo obj:listAux){
			grupoLista.add(new SelectItem(obj,obj.getGrupo()));
		}
		
		return grupoLista;
	}

	public void setGrupoLista(List<SelectItem> grupoLista) {
		this.grupoLista = grupoLista;
	}

    public Usuario getUsuarioTrocarSenha() {
    	if(usuarioTrocarSenha==null){
    		usuarioTrocarSenha = Funcoes.getUsuarioLogado();
    	}
		return usuarioTrocarSenha;
	}

	public void setUsuarioTrocarSenha(Usuario usuarioTrocarSenha) {
		this.usuarioTrocarSenha = usuarioTrocarSenha;
	}

	public String editarTocarSenha() {
		return "trocarSenha";
    }

	public void trocarSenha(ActionEvent actionEvent) throws NoSuchAlgorithmException{
		Usuario usuarioAux = usuarioBean.getById(usuarioTrocarSenha.getId());
		
		usuarioAux.setSenha(usuarioTrocarSenha.getSenha());
		
		try {
			usuarioBean.salvar(usuarioAux, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), "login");
			return;
		}
		
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro atualizado com sucesso.", "Registro atualizado com sucesso.");
		FacesContext.getCurrentInstance().addMessage(null, message);
		
	}

	public String cancelarTroca(){
		return "Principal";
	}

}