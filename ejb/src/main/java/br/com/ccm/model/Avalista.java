package br.com.ccm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="cd_avalista")
public class Avalista implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 126671749132609885L;
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "dt_entrada")
	private Date dtEntrada;
	@Column(name = "ds_status")
	private String status;
	@Column(name = "ds_arquivo")
	private String arquivo;
	@Column(name = "ds_nome")
	private String nome;
	@Column(name = "dt_nasc")
	private Date dtNasc;
	@Column(name = "ds_cpf")
	private String cpf;
	@Column(name = "ds_email")
	private String email;
	@Column(name = "ds_identidade")
	private String identidade;
	@Column(name = "dt_emissao")
	private Date dtEmissao;
	@Column(name = "ds_pai")
	private String pai;
	@Column(name = "ds_mae")
	private String mae;
	@Column(name = "ds_estadocivil")
	private String estadoCivil;
	@Column(name = "ds_conjuge")
	private String conjuge;
	@Column(name = "ds_cpfconjuge")
	private String cpfConjuge;
	@Column(name = "ds_identidadeconjuge")
	private String identidadeConjuge;
	@Column(name = "dt_nascconjuge")
	private Date dtNascConjuge;
	@Column(name = "ds_empresa")
	private String empresa;
	@Column(name = "dt_admissao")
	private Date dtAdmissao;
	@Column(name = "ds_cargo")
	private String cargo;
	@Column(name = "ds_cargodesc")
	private String cargoDesc;
	@Column(name = "ds_cnpj")
	private String cnpj;
	@Column(name = "vl_renda")
	private BigDecimal renda;
	@Column(name = "vl_renda2")
	private BigDecimal renda2;
	@Column(name = "ds_comprovadapor")
	private String comprovadaPor;
	@Column(name = "ds_refpesnome1")
	private String refPessoalNome1;
	@Column(name = "ds_refpesFone1")
	private String refPessoalFone1;
	@Column(name = "ds_refpesnome2")
	private String refPessoalNome2;
	@Column(name = "ds_refpesFone2")
	private String refPessoalFone2;
	@Column(name = "ds_agencia")
	private String agencia;	
	@Column(name = "nr_banco")
	private int banco;
	@Column(name = "ds_contaCorrente")
	private String contaCorrente;
	@Column(name = "dt_desde")
	private Date desde;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_vendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="fk_banco")
	private Banco bancoFin;
	@Column(name = "ds_cepres")
	private String cepres;
	@Column(name = "ds_logradourores")
	private String logradourores;
	@Column(name = "nr_endres")
	private int numEndres;
	@Column(name = "ds_complementores")
	private String complementores;
	@Column(name = "ds_bairrores")
	private String bairrores;
	@Column(name = "ds_cidaderes")
	private String cidaderes;
	@Column(name = "ds_ufres")
	private String ufres;
	@Column(name = "nr_temporesanosres")
	private int tempoResAnosres;
	@Column(name = "nr_temporesmesesres")
	private int tempoResMesesres;
	@Column(name = "ds_tpresidenciares")
	private String tpResidenciares;
	@Column(name = "ds_cepcom")
	private String cepcom;
	@Column(name = "ds_logradourocom")
	private String logradourocom;
	@Column(name = "nr_endcom")
	private int numEndcom;
	@Column(name = "ds_complementocom")
	private String complementocom;
	@Column(name = "ds_bairrocom")
	private String bairrocom;
	@Column(name = "ds_cidadecom")
	private String cidadecom;
	@Column(name = "ds_ufcom")
	private String ufcom;
	@Column(name = "nr_tempocomanoscom")
	private int tempocomAnoscom;
	@Column(name = "nr_tempocommesescom")
	private int tempocomMesescom;
	@Column(name = "ds_tpcomidenciacom")
	private String tpcomidenciacom;
	@Column(name = "ds_telefoneRes")
	private String telefoneRes;
	@Column(name = "ds_celular")
	private String celular;
	@Column(name = "ds_telefoneCom")
	private String telefoneCom;
	@Column(name = "ds_telefoneCom1")
	private String telefoneCom1;
	@Column(name = "ds_numeroBeneficio")
	private String numeroBeneficio;
	@Column(name = "ds_nomeBeneficio")
	private String nomeBeneficio;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@ManyToOne
	@JoinColumn(name="fk_alterado")
	private Usuario alterado;
	@Column(name = "ds_usuinc")
	private String usuinc;
	@Column(name = "ds_usualt")
	private String usualt;
	@Column(name = "tg_inativo")
	private Long inativo;
	@ManyToOne
	@JoinColumn(name="fk_cliente")
	private Cliente cliente;
	
	public Avalista() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Avalista(Long id, Date dtEntrada, String status, String arquivo,
			String nome, Date dtNasc, String cpf, String email,
			String identidade, Date dtEmissao, String pai, String mae,
			String estadoCivil, String conjuge, String cpfConjuge,
			String identidadeConjuge, Date dtNascConjuge, String empresa,
			Date dtAdmissao, String cargo, String cargoDesc, String cnpj,
			BigDecimal renda, BigDecimal renda2, String comprovadaPor,
			String refPessoalNome1, String refPessoalFone1,
			String refPessoalNome2, String refPessoalFone2, String agencia,
			int banco, String contaCorrente, Date desde, Date inclusao,
			Date alteracao, Vendedor vendedor, Banco bancoFin, String cepres,
			String logradourores, int numEndres, String complementores,
			String bairrores, String cidaderes, String ufres,
			int tempoResAnosres, int tempoResMesesres, String tpResidenciares,
			String cepcom, String logradourocom, int numEndcom,
			String complementocom, String bairrocom, String cidadecom,
			String ufcom, int tempocomAnoscom, int tempocomMesescom,
			String tpcomidenciacom, String telefoneRes, String celular,
			String telefoneCom, String telefoneCom1, String numeroBeneficio,
			String nomeBeneficio, Usuario criador, Usuario alterado,
			String usuinc, String usualt, Long inativo, Cliente cliente) {
		super();
		this.id = id;
		this.dtEntrada = dtEntrada;
		this.status = status;
		this.arquivo = arquivo;
		this.nome = nome;
		this.dtNasc = dtNasc;
		this.cpf = cpf;
		this.email = email;
		this.identidade = identidade;
		this.dtEmissao = dtEmissao;
		this.pai = pai;
		this.mae = mae;
		this.estadoCivil = estadoCivil;
		this.conjuge = conjuge;
		this.cpfConjuge = cpfConjuge;
		this.identidadeConjuge = identidadeConjuge;
		this.dtNascConjuge = dtNascConjuge;
		this.empresa = empresa;
		this.dtAdmissao = dtAdmissao;
		this.cargo = cargo;
		this.cargoDesc = cargoDesc;
		this.cnpj = cnpj;
		this.renda = renda;
		this.renda2 = renda2;
		this.comprovadaPor = comprovadaPor;
		this.refPessoalNome1 = refPessoalNome1;
		this.refPessoalFone1 = refPessoalFone1;
		this.refPessoalNome2 = refPessoalNome2;
		this.refPessoalFone2 = refPessoalFone2;
		this.agencia = agencia;
		this.banco = banco;
		this.contaCorrente = contaCorrente;
		this.desde = desde;
		this.inclusao = inclusao;
		this.alteracao = alteracao;
		this.vendedor = vendedor;
		this.bancoFin = bancoFin;
		this.cepres = cepres;
		this.logradourores = logradourores;
		this.numEndres = numEndres;
		this.complementores = complementores;
		this.bairrores = bairrores;
		this.cidaderes = cidaderes;
		this.ufres = ufres;
		this.tempoResAnosres = tempoResAnosres;
		this.tempoResMesesres = tempoResMesesres;
		this.tpResidenciares = tpResidenciares;
		this.cepcom = cepcom;
		this.logradourocom = logradourocom;
		this.numEndcom = numEndcom;
		this.complementocom = complementocom;
		this.bairrocom = bairrocom;
		this.cidadecom = cidadecom;
		this.ufcom = ufcom;
		this.tempocomAnoscom = tempocomAnoscom;
		this.tempocomMesescom = tempocomMesescom;
		this.tpcomidenciacom = tpcomidenciacom;
		this.telefoneRes = telefoneRes;
		this.celular = celular;
		this.telefoneCom = telefoneCom;
		this.telefoneCom1 = telefoneCom1;
		this.numeroBeneficio = numeroBeneficio;
		this.nomeBeneficio = nomeBeneficio;
		this.criador = criador;
		this.alterado = alterado;
		this.usuinc = usuinc;
		this.usualt = usualt;
		this.inativo = inativo;
		this.cliente = cliente;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDtNasc() {
		return dtNasc;
	}

	public void setDtNasc(Date dtNasc) {
		this.dtNasc = dtNasc;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdentidade() {
		return identidade;
	}

	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}

	public Date getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getCpfConjuge() {
		return cpfConjuge;
	}

	public void setCpfConjuge(String cpfConjuge) {
		this.cpfConjuge = cpfConjuge;
	}

	public String getIdentidadeConjuge() {
		return identidadeConjuge;
	}

	public void setIdentidadeConjuge(String identidadeConjuge) {
		this.identidadeConjuge = identidadeConjuge;
	}

	public Date getDtNascConjuge() {
		return dtNascConjuge;
	}

	public void setDtNascConjuge(Date dtNascConjuge) {
		this.dtNascConjuge = dtNascConjuge;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Date getDtAdmissao() {
		return dtAdmissao;
	}

	public void setDtAdmissao(Date dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public BigDecimal getRenda() {
		return renda;
	}

	public void setRenda(BigDecimal renda) {
		this.renda = renda;
	}

	public BigDecimal getRenda2() {
		return renda2;
	}

	public void setRenda2(BigDecimal renda2) {
		this.renda2 = renda2;
	}

	public String getComprovadaPor() {
		return comprovadaPor;
	}

	public void setComprovadaPor(String comprovadaPor) {
		this.comprovadaPor = comprovadaPor;
	}

	public String getRefPessoalNome1() {
		return refPessoalNome1;
	}

	public void setRefPessoalNome1(String refPessoalNome1) {
		this.refPessoalNome1 = refPessoalNome1;
	}

	public String getRefPessoalFone1() {
		return refPessoalFone1;
	}

	public void setRefPessoalFone1(String refPessoalFone1) {
		this.refPessoalFone1 = refPessoalFone1;
	}

	public String getRefPessoalNome2() {
		return refPessoalNome2;
	}

	public void setRefPessoalNome2(String refPessoalNome2) {
		this.refPessoalNome2 = refPessoalNome2;
	}

	public String getRefPessoalFone2() {
		return refPessoalFone2;
	}

	public void setRefPessoalFone2(String refPessoalFone2) {
		this.refPessoalFone2 = refPessoalFone2;
	}

	public int getBanco() {
		return banco;
	}

	public void setBanco(int banco) {
		this.banco = banco;
	}

	public Banco getBancoFin() {
		return bancoFin;
	}

	public void setBancoFin(Banco bancoFin) {
		this.bancoFin = bancoFin;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Long getInativo() {
		return inativo;
	}
	public void setInativo(Long inativo) {
		this.inativo = inativo;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Avalista other = (Avalista) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public String getCepres() {
		return cepres;
	}

	public void setCepres(String cepres) {
		this.cepres = cepres;
	}

	public String getLogradourores() {
		return logradourores;
	}

	public void setLogradourores(String logradourores) {
		this.logradourores = logradourores;
	}

	public int getNumEndres() {
		return numEndres;
	}

	public void setNumEndres(int numEndres) {
		this.numEndres = numEndres;
	}

	public String getComplementores() {
		return complementores;
	}

	public void setComplementores(String complementores) {
		this.complementores = complementores;
	}

	public String getBairrores() {
		return bairrores;
	}

	public void setBairrores(String bairrores) {
		this.bairrores = bairrores;
	}

	public String getCidaderes() {
		return cidaderes;
	}

	public void setCidaderes(String cidaderes) {
		this.cidaderes = cidaderes;
	}

	public String getUfres() {
		return ufres;
	}

	public void setUfres(String ufres) {
		this.ufres = ufres;
	}

	public int getTempoResAnosres() {
		return tempoResAnosres;
	}

	public void setTempoResAnosres(int tempoResAnosres) {
		this.tempoResAnosres = tempoResAnosres;
	}

	public int getTempoResMesesres() {
		return tempoResMesesres;
	}

	public void setTempoResMesesres(int tempoResMesesres) {
		this.tempoResMesesres = tempoResMesesres;
	}

	public String getTpResidenciares() {
		return tpResidenciares;
	}

	public void setTpResidenciares(String tpResidenciares) {
		this.tpResidenciares = tpResidenciares;
	}

	public String getCepcom() {
		return cepcom;
	}

	public void setCepcom(String cepcom) {
		this.cepcom = cepcom;
	}

	public String getLogradourocom() {
		return logradourocom;
	}

	public void setLogradourocom(String logradourocom) {
		this.logradourocom = logradourocom;
	}

	public int getNumEndcom() {
		return numEndcom;
	}

	public void setNumEndcom(int numEndcom) {
		this.numEndcom = numEndcom;
	}

	public String getComplementocom() {
		return complementocom;
	}

	public void setComplementocom(String complementocom) {
		this.complementocom = complementocom;
	}

	public String getBairrocom() {
		return bairrocom;
	}

	public void setBairrocom(String bairrocom) {
		this.bairrocom = bairrocom;
	}

	public String getCidadecom() {
		return cidadecom;
	}

	public void setCidadecom(String cidadecom) {
		this.cidadecom = cidadecom;
	}

	public String getUfcom() {
		return ufcom;
	}

	public void setUfcom(String ufcom) {
		this.ufcom = ufcom;
	}

	public int getTempocomAnoscom() {
		return tempocomAnoscom;
	}

	public void setTempocomAnoscom(int tempocomAnoscom) {
		this.tempocomAnoscom = tempocomAnoscom;
	}

	public int getTempocomMesescom() {
		return tempocomMesescom;
	}

	public void setTempocomMesescom(int tempocomMesescom) {
		this.tempocomMesescom = tempocomMesescom;
	}

	public String getTpcomidenciacom() {
		return tpcomidenciacom;
	}

	public void setTpcomidenciacom(String tpcomidenciacom) {
		this.tpcomidenciacom = tpcomidenciacom;
	}

	public String getTelefoneRes() {
		return telefoneRes;
	}

	public void setTelefoneRes(String telefoneRes) {
		this.telefoneRes = telefoneRes;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefoneCom() {
		return telefoneCom;
	}

	public void setTelefoneCom(String telefoneCom) {
		this.telefoneCom = telefoneCom;
	}

	public String getTelefoneCom1() {
		return telefoneCom1;
	}

	public void setTelefoneCom1(String telefoneCom1) {
		this.telefoneCom1 = telefoneCom1;
	}

	public Date getDtEntrada() {
		return dtEntrada;
	}

	public void setDtEntrada(Date dtEntrada) {
		this.dtEntrada = dtEntrada;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	public Usuario getAlterado() {
		return alterado;
	}

	public void setAlterado(Usuario alterado) {
		this.alterado = alterado;
	}

	public String getUsuinc() {
		return usuinc;
	}

	public void setUsuinc(String usuinc) {
		this.usuinc = usuinc;
	}

	public String getUsualt() {
		return usualt;
	}

	public void setUsualt(String usualt) {
		this.usualt = usualt;
	}

	public String getNumeroBeneficio() {
		return numeroBeneficio;
	}

	public void setNumeroBeneficio(String numeroBeneficio) {
		this.numeroBeneficio = numeroBeneficio;
	}

	public String getNomeBeneficio() {
		return nomeBeneficio;
	}

	public void setNomeBeneficio(String nomeBeneficio) {
		this.nomeBeneficio = nomeBeneficio;
	}

	public String getCargoDesc() {
		return cargoDesc;
	}

	public void setCargoDesc(String cargoDesc) {
		this.cargoDesc = cargoDesc;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
