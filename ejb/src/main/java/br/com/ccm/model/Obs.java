package br.com.ccm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cd_obs")
public class Obs implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4771849339194854338L;
	
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name="fk_cliente")
	private Cliente cliente;
	@Column(name = "ds_obs")
	@Lob
	private String observacao;
	@Column(name = "fl_avisarVendedor")
	private boolean avisarVendedor;
	@Column(name = "fl_avisarLocatario")
	private boolean avisarLocatario;
	@Column(name = "fl_avisarCcm")
	private boolean avisarCcm;
	@Column(name = "fl_lido")
	private boolean lido;
	@Column(name = "fl_lidoLocatario")
	private boolean lidoLocatario;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	
	public Obs() {
		id = new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

	public boolean isAvisarVendedor() {
		return avisarVendedor;
	}

	public void setAvisarVendedor(boolean avisarVendedor) {
		this.avisarVendedor = avisarVendedor;
	}

	public boolean isLido() {
		return lido;
	}

	public void setLido(boolean lido) {
		this.lido = lido;
	}
	
	public boolean isAvisarCcm() {
		return avisarCcm;
	}

	public void setAvisarCcm(boolean avisarCcm) {
		this.avisarCcm = avisarCcm;
	}
	
	public boolean isAvisarLocatario() {
		return avisarLocatario;
	}

	public void setAvisarLocatario(boolean avisarLocatario) {
		this.avisarLocatario = avisarLocatario;
	}

	public boolean isLidoLocatario() {
		return lidoLocatario;
	}

	public void setLidoLocatario(boolean lidoLocatario) {
		this.lidoLocatario = lidoLocatario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Obs other = (Obs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
