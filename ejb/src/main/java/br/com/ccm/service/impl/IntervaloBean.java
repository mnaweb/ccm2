package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.IIntervaloDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.IIntervaloBean;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.Usuario;

@Stateless
public class IntervaloBean implements IIntervaloBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private IIntervaloDao dao;
	
	public ArrayList<Intervalo> listaAll(){
		return (ArrayList<Intervalo>) dao.listAllByOrder("descricao");
	}
	
	public Intervalo salvar(Intervalo intervalo, Usuario usuarioLogado) throws AplicacaoException{
		
		if(intervalo.getId()==0){
			intervalo.setInclusao(new Date());
			intervalo.setCriador(usuarioLogado);
			
			dao.save(intervalo);
		}else{
			intervalo.setAlteracao(new Date());
			
			dao.save(intervalo);
		}
		
		return intervalo;
	}
	
	public void deletar(Intervalo intervalo){
		dao.delete(intervalo);
	}
	
	public Intervalo getById(Long id){
		return dao.find(id);
	}


}
