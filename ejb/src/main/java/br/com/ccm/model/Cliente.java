package br.com.ccm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Past;

@Entity
@Table(name="cd_cliente")
public class Cliente  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 613180511917726291L;
	
	public final static String STATUS_CHECAGEM		= "Checagem";
	public final static String STATUS_PENDENTE		= "Pendente";
	public final static String STATUS_NEGOCIACAO	= "Negociacao";
	public final static String STATUS_ANALISE		= "Analise";
	public final static String STATUS_APROVADA		= "Aprovada";
	public final static String STATUS_RECUSADA		= "Recusada";
	public final static String STATUS_CANCELADA		= "Cancelada";
	public final static String STATUS_EFETIVADA		= "Efetivada";
	public final static String STATUS_APROVADO_CANC = "Aprovada-Canc.";
	
	public final static String TIPO_PESSOA_PF		= "PF";
	public final static String TIPO_PESSOA_PJ		= "PJ";
	
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "dt_entrada")
	private Date dtEntrada;
	@Column(name = "ds_status")
	private String status;
	@Column(name = "ds_tipo")
	private String tipo;
	@Column(name = "ds_arquivo")
	private String arquivo;
	@Column(name = "ds_nome")
	private String nome;
	@Past(message="O campo 'Data Nascimento' nao pode ser superior a data atual")
	@Column(name = "dt_nasc")
	private Date dtNasc;
	@Column(name = "ds_cpf")
	private String cpf;
	@Column(name = "ds_email")
	private String email;
	@Column(name = "ds_identidade")
	private String identidade;
	@Past(message="O campo 'Data emissao' nao pode ser superior a data atual")
	@Column(name = "dt_emissao")
	private Date dtEmissao;
	@Column(name = "ds_pai")
	private String pai;
	@Column(name = "ds_mae")
	private String mae;
	@Column(name = "ds_estadocivil")
	private String estadoCivil;
	@Column(name = "ds_conjuge")
	private String conjuge;
	@Column(name = "ds_cpfconjuge")
	private String cpfConjuge;
	@Column(name = "ds_identidadeconjuge")
	private String identidadeConjuge;
	@Column(name = "dt_nascconjuge")
	private Date dtNascConjuge;
	@Column(name = "ds_empresa")
	private String empresa;
	@Column(name = "dt_admissao")
	private Date dtAdmissao;
	@Column(name = "ds_cargo")
	private String cargo;
	@Column(name = "ds_cargodesc")
	private String cargoDesc;
	@Column(name = "ds_cnpj")
	private String cnpj;
	@Column(name = "vl_renda")
	private BigDecimal renda;
	@Column(name = "vl_renda2")
	private BigDecimal renda2;
	@Column(name = "ds_comprovadapor")
	private String comprovadaPor;
	@Column(name = "ds_refpesnome1")
	private String refPessoalNome1;
	@Column(name = "ds_refpesFone1")
	private String refPessoalFone1;
	@Column(name = "ds_refpesnome2")
	private String refPessoalNome2;
	@Column(name = "ds_refpesFone2")
	private String refPessoalFone2;
	@Column(name = "ds_agencia")
	private String agencia;	
	@Column(name = "nr_banco")
	private int banco;
	@Column(name = "ds_contaCorrente")
	private String contaCorrente;
	@Column(name = "dt_desde")
	@Past(message="O campo 'Desde' nao pode ser superior a data atual")
	private Date desde;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_vendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="fk_banco")
	private Banco bancoFin;
	@Column(name = "ds_cepres")
	private String cepres;
	@Column(name = "ds_logradourores")
	private String logradourores;
	@Column(name = "nr_endres")
	private int numEndres;
	@Column(name = "ds_complementores")
	private String complementores;
	@Column(name = "ds_bairrores")
	private String bairrores;
	@Column(name = "ds_cidaderes")
	private String cidaderes;
	@Column(name = "ds_ufres")
	private String ufres;
	@Column(name = "nr_temporesanosres")
	private int tempoResAnosres;
	@Column(name = "nr_temporesmesesres")
	private int tempoResMesesres;
	@Column(name = "ds_tpresidenciares")
	private String tpResidenciares;
	@Column(name = "ds_cepcom")
	private String cepcom;
	@Column(name = "ds_logradourocom")
	private String logradourocom;
	@Column(name = "nr_endcom")
	private int numEndcom;
	@Column(name = "ds_complementocom")
	private String complementocom;
	@Column(name = "ds_bairrocom")
	private String bairrocom;
	@Column(name = "ds_cidadecom")
	private String cidadecom;
	@Column(name = "ds_ufcom")
	private String ufcom;
	@Column(name = "nr_tempocomanoscom")
	private int tempocomAnoscom;
	@Column(name = "nr_tempocommesescom")
	private int tempocomMesescom;
	@Column(name = "ds_tpcomidenciacom")
	private String tpcomidenciacom;
	@Column(name = "ds_telefoneRes")
	private String telefoneRes;
	@Column(name = "ds_celular")
	private String celular;
	@Column(name = "ds_telefoneCom")
	private String telefoneCom;
	@Column(name = "ds_telefoneCom1")
	private String telefoneCom1;
	@Column(name = "ds_marca")
	private String marca;
	@Column(name = "ds_veiculo")
	private String veiculo;
	@Column(name = "ds_anofabmod")
	private String anoFabMod;
	@Column(name = "vl_total")
	private BigDecimal vlTotal;
	@Column(name = "vl_entrada")
	private BigDecimal vlEntrada;
	@Column(name = "vl_parcela")
	private BigDecimal vlParcela;
	@Column(name = "ds_prazo")
	private String prazo;
	@Column(name = "ds_numeroBeneficio")
	private String numeroBeneficio;
	@Column(name = "ds_nomeBeneficio")
	private String nomeBeneficio;
	
	@Column(name = "ds_cnpjPJ")
	private String cnpjPJ;
	@Column(name = "ds_razaoSocialPJ")
	private String razaoSocialPJ;
	@Column(name = "ds_telefonePJ")
	private String telefonePJ;
	@Column(name = "ds_cepPJ")
	private String cepPJ;
	@Column(name = "ds_logradouroPJ")
	private String logradouroPJ;
	@Column(name = "nr_endPJ")
	private Integer numEndPJ;
	@Column(name = "ds_complementoPJ")
	private String complementoPJ;
	@Column(name = "ds_bairroPJ")
	private String bairroPJ;
	@Column(name = "ds_cidadePJ")
	private String cidadePJ;
	@Column(name = "ds_ufPJ")
	private String ufPJ;
	@Column(name = "vl_faturamentoMensalPJ")
	private BigDecimal faturamentoMensalPJ;
	@Column(name = "vl_capitalSocialPJ")
	private BigDecimal capitalSocialPJ;
	@Column(name = "qt_quantidadeFuncionariosPJ")
	private Integer quantidadeFuncionariosPJ;
	@Column(name = "ds_atividadeEmpresaPJ")
	private String atividadeEmpresaPJ;
	@Column(name = "ds_placa")
	private String placa;
	@Column(name = "ds_renavam")
	private String renavam;
	
	@Column(name = "dt_efetivada")
	private Date efetivada;
	@Column(name = "vl_percRetorno")
	private BigDecimal percRetorno;
	@Column(name = "vl_impostos")
	private BigDecimal impostos;
	@Column(name = "vl_custoFicha")
	private BigDecimal custoFicha;
	@Column(name = "vl_custoFichaPerc")
	private BigDecimal custoFichaPerc;
	@Column(name = "ds_sexo")
	private String sexo;
	@Column(name = "nr_estadoCarro")
	private Integer estadoCarro;
	
	@OneToMany(targetEntity=Obs.class,mappedBy="cliente",cascade=CascadeType.MERGE)
	private Collection<Obs> obs;

	@OneToMany(targetEntity=Avalista.class,mappedBy="cliente",cascade={CascadeType.MERGE, CascadeType.REMOVE})
	private Collection<Avalista> avalistas;
	
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@ManyToOne
	@JoinColumn(name="fk_alterado")
	private Usuario alterado;
	@Column(name = "ds_usuinc")
	private String usuinc;
	@Column(name = "ds_usualt")
	private String usualt;
	@Column(name = "tg_inativo")
	private Long inativo;
	@Column(name = "fl_inativo")
	private boolean cancelar;
	@Transient
	private boolean possuiHistorico;
	@Transient
	private Date dataAcao;
	@Transient
	private String acao;
	@Transient
	private long numRevisao;
	@ManyToOne
	@JoinColumn(name="fk_empresaPortal")
	private Empresa empresaPortal;

	
	public Cliente() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDtNasc() {
		return dtNasc;
	}

	public void setDtNasc(Date dtNasc) {
		this.dtNasc = dtNasc;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdentidade() {
		return identidade;
	}

	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}

	public Date getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getCpfConjuge() {
		return cpfConjuge;
	}

	public void setCpfConjuge(String cpfConjuge) {
		this.cpfConjuge = cpfConjuge;
	}

	public String getIdentidadeConjuge() {
		return identidadeConjuge;
	}

	public void setIdentidadeConjuge(String identidadeConjuge) {
		this.identidadeConjuge = identidadeConjuge;
	}

	public Date getDtNascConjuge() {
		return dtNascConjuge;
	}

	public void setDtNascConjuge(Date dtNascConjuge) {
		this.dtNascConjuge = dtNascConjuge;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Date getDtAdmissao() {
		return dtAdmissao;
	}

	public void setDtAdmissao(Date dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public BigDecimal getRenda() {
		return renda;
	}

	public void setRenda(BigDecimal renda) {
		this.renda = renda;
	}

	public BigDecimal getRenda2() {
		return renda2;
	}

	public void setRenda2(BigDecimal renda2) {
		this.renda2 = renda2;
	}

	public String getComprovadaPor() {
		return comprovadaPor;
	}

	public void setComprovadaPor(String comprovadaPor) {
		this.comprovadaPor = comprovadaPor;
	}

	public String getRefPessoalNome1() {
		return refPessoalNome1;
	}

	public void setRefPessoalNome1(String refPessoalNome1) {
		this.refPessoalNome1 = refPessoalNome1;
	}

	public String getRefPessoalFone1() {
		return refPessoalFone1;
	}

	public void setRefPessoalFone1(String refPessoalFone1) {
		this.refPessoalFone1 = refPessoalFone1;
	}

	public String getRefPessoalNome2() {
		return refPessoalNome2;
	}

	public void setRefPessoalNome2(String refPessoalNome2) {
		this.refPessoalNome2 = refPessoalNome2;
	}

	public String getRefPessoalFone2() {
		return refPessoalFone2;
	}

	public void setRefPessoalFone2(String refPessoalFone2) {
		this.refPessoalFone2 = refPessoalFone2;
	}

	public int getBanco() {
		return banco;
	}

	public void setBanco(int banco) {
		this.banco = banco;
	}

	public Banco getBancoFin() {
		return bancoFin;
	}

	public void setBancoFin(Banco bancoFin) {
		this.bancoFin = bancoFin;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getEfetivada() {
		return efetivada;
	}

	public void setEfetivada(Date efetivada) {
		this.efetivada = efetivada;
	}

	public BigDecimal getImpostos() {
		return impostos;
	}

	public void setImpostos(BigDecimal impostos) {
		this.impostos = impostos;
	}

	public BigDecimal getPercRetorno() {
		return percRetorno;
	}

	public void setPercRetorno(BigDecimal percRetorno) {
		this.percRetorno = percRetorno;
	}

	public BigDecimal getCustoFicha() {
		return custoFicha;
	}

	public void setCustoFicha(BigDecimal custoFicha) {
		this.custoFicha = custoFicha;
	}

	public BigDecimal getCustoFichaPerc() {
		return custoFichaPerc;
	}

	public void setCustoFichaPerc(BigDecimal custoFichaPerc) {
		this.custoFichaPerc = custoFichaPerc;
	}

	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Long getInativo() {
		return inativo;
	}
	public void setInativo(Long inativo) {
		this.inativo = inativo;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCepres() {
		return cepres;
	}

	public void setCepres(String cepres) {
		this.cepres = cepres;
	}

	public String getLogradourores() {
		return logradourores;
	}

	public void setLogradourores(String logradourores) {
		this.logradourores = logradourores;
	}

	public int getNumEndres() {
		return numEndres;
	}

	public void setNumEndres(int numEndres) {
		this.numEndres = numEndres;
	}

	public String getComplementores() {
		return complementores;
	}

	public void setComplementores(String complementores) {
		this.complementores = complementores;
	}

	public String getBairrores() {
		return bairrores;
	}

	public void setBairrores(String bairrores) {
		this.bairrores = bairrores;
	}

	public String getCidaderes() {
		return cidaderes;
	}

	public void setCidaderes(String cidaderes) {
		this.cidaderes = cidaderes;
	}

	public String getUfres() {
		return ufres;
	}

	public void setUfres(String ufres) {
		this.ufres = ufres;
	}

	public int getTempoResAnosres() {
		return tempoResAnosres;
	}

	public void setTempoResAnosres(int tempoResAnosres) {
		this.tempoResAnosres = tempoResAnosres;
	}

	public int getTempoResMesesres() {
		return tempoResMesesres;
	}

	public void setTempoResMesesres(int tempoResMesesres) {
		this.tempoResMesesres = tempoResMesesres;
	}

	public String getTpResidenciares() {
		return tpResidenciares;
	}

	public void setTpResidenciares(String tpResidenciares) {
		this.tpResidenciares = tpResidenciares;
	}

	public String getCepcom() {
		return cepcom;
	}

	public void setCepcom(String cepcom) {
		this.cepcom = cepcom;
	}

	public String getLogradourocom() {
		return logradourocom;
	}

	public void setLogradourocom(String logradourocom) {
		this.logradourocom = logradourocom;
	}

	public int getNumEndcom() {
		return numEndcom;
	}

	public void setNumEndcom(int numEndcom) {
		this.numEndcom = numEndcom;
	}

	public String getComplementocom() {
		return complementocom;
	}

	public void setComplementocom(String complementocom) {
		this.complementocom = complementocom;
	}

	public String getBairrocom() {
		return bairrocom;
	}

	public void setBairrocom(String bairrocom) {
		this.bairrocom = bairrocom;
	}

	public String getCidadecom() {
		return cidadecom;
	}

	public void setCidadecom(String cidadecom) {
		this.cidadecom = cidadecom;
	}

	public String getUfcom() {
		return ufcom;
	}

	public void setUfcom(String ufcom) {
		this.ufcom = ufcom;
	}

	public int getTempocomAnoscom() {
		return tempocomAnoscom;
	}

	public void setTempocomAnoscom(int tempocomAnoscom) {
		this.tempocomAnoscom = tempocomAnoscom;
	}

	public int getTempocomMesescom() {
		return tempocomMesescom;
	}

	public void setTempocomMesescom(int tempocomMesescom) {
		this.tempocomMesescom = tempocomMesescom;
	}

	public String getTpcomidenciacom() {
		return tpcomidenciacom;
	}

	public void setTpcomidenciacom(String tpcomidenciacom) {
		this.tpcomidenciacom = tpcomidenciacom;
	}

	public String getTelefoneRes() {
		return telefoneRes;
	}

	public void setTelefoneRes(String telefoneRes) {
		this.telefoneRes = telefoneRes;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefoneCom() {
		return telefoneCom;
	}

	public void setTelefoneCom(String telefoneCom) {
		this.telefoneCom = telefoneCom;
	}

	public String getTelefoneCom1() {
		return telefoneCom1;
	}

	public void setTelefoneCom1(String telefoneCom1) {
		this.telefoneCom1 = telefoneCom1;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}

	public String getAnoFabMod() {
		return anoFabMod;
	}

	public void setAnoFabMod(String anoFabMod) {
		this.anoFabMod = anoFabMod;
	}

	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	public BigDecimal getVlEntrada() {
		return vlEntrada;
	}

	public void setVlEntrada(BigDecimal vlEntrada) {
		this.vlEntrada = vlEntrada;
	}

	public BigDecimal getVlParcela() {
		return vlParcela;
	}

	public void setVlParcela(BigDecimal vlParcela) {
		this.vlParcela = vlParcela;
	}

	public String getPrazo() {
		return prazo;
	}

	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}

	public Date getDtEntrada() {
		return dtEntrada;
	}

	public void setDtEntrada(Date dtEntrada) {
		this.dtEntrada = dtEntrada;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Collection<Obs> getObs() {
		return obs;
	}

	public void setObs(Collection<Obs> obs) {
		this.obs = obs;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	public boolean isCancelar() {
		return cancelar;
	}

	public void setCancelar(boolean cancelar) {
		this.cancelar = cancelar;
	}

	public boolean isPossuiHistorico() {
		return possuiHistorico;
	}

	public void setPossuiHistorico(boolean possuiHistorico) {
		this.possuiHistorico = possuiHistorico;
	}

	public Usuario getAlterado() {
		return alterado;
	}

	public void setAlterado(Usuario alterado) {
		this.alterado = alterado;
	}

	public Date getDataAcao() {
		return dataAcao;
	}

	public void setDataAcao(Date dataAcao) {
		this.dataAcao = dataAcao;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getUsuinc() {
		return usuinc;
	}

	public void setUsuinc(String usuinc) {
		this.usuinc = usuinc;
	}

	public String getUsualt() {
		return usualt;
	}

	public void setUsualt(String usualt) {
		this.usualt = usualt;
	}

	public String getNumeroBeneficio() {
		return numeroBeneficio;
	}

	public void setNumeroBeneficio(String numeroBeneficio) {
		this.numeroBeneficio = numeroBeneficio;
	}

	public long getNumRevisao() {
		return numRevisao;
	}

	public void setNumRevisao(long numRevisao) {
		this.numRevisao = numRevisao;
	}

	public String getNomeBeneficio() {
		return nomeBeneficio;
	}

	public void setNomeBeneficio(String nomeBeneficio) {
		this.nomeBeneficio = nomeBeneficio;
	}

	public String getCargoDesc() {
		return cargoDesc;
	}

	public void setCargoDesc(String cargoDesc) {
		this.cargoDesc = cargoDesc;
	}

	public Collection<Avalista> getAvalistas() {
		return avalistas;
	}

	public void setAvalistas(Collection<Avalista> avalistas) {
		this.avalistas = avalistas;
	}

	public String getCnpjPJ() {
		return cnpjPJ;
	}

	public void setCnpjPJ(String cnpjPJ) {
		this.cnpjPJ = cnpjPJ;
	}

	public String getRazaoSocialPJ() {
		return razaoSocialPJ;
	}

	public void setRazaoSocialPJ(String razaoSocialPJ) {
		this.razaoSocialPJ = razaoSocialPJ;
	}

	public String getTelefonePJ() {
		return telefonePJ;
	}

	public void setTelefonePJ(String telefonePJ) {
		this.telefonePJ = telefonePJ;
	}

	public String getCepPJ() {
		return cepPJ;
	}

	public void setCepPJ(String cepPJ) {
		this.cepPJ = cepPJ;
	}

	public String getLogradouroPJ() {
		return logradouroPJ;
	}

	public void setLogradouroPJ(String logradouroPJ) {
		this.logradouroPJ = logradouroPJ;
	}

	public Integer getNumEndPJ() {
		return numEndPJ;
	}

	public void setNumEndPJ(Integer numEndPJ) {
		this.numEndPJ = numEndPJ;
	}

	public String getComplementoPJ() {
		return complementoPJ;
	}

	public void setComplementoPJ(String complementoPJ) {
		this.complementoPJ = complementoPJ;
	}

	public String getBairroPJ() {
		return bairroPJ;
	}

	public void setBairroPJ(String bairroPJ) {
		this.bairroPJ = bairroPJ;
	}

	public String getCidadePJ() {
		return cidadePJ;
	}

	public void setCidadePJ(String cidadePJ) {
		this.cidadePJ = cidadePJ;
	}

	public String getUfPJ() {
		return ufPJ;
	}

	public void setUfPJ(String ufPJ) {
		this.ufPJ = ufPJ;
	}

	public BigDecimal getFaturamentoMensalPJ() {
		return faturamentoMensalPJ;
	}

	public void setFaturamentoMensalPJ(BigDecimal faturamentoMensalPJ) {
		this.faturamentoMensalPJ = faturamentoMensalPJ;
	}

	public BigDecimal getCapitalSocialPJ() {
		return capitalSocialPJ;
	}

	public void setCapitalSocialPJ(BigDecimal capitalSocialPJ) {
		this.capitalSocialPJ = capitalSocialPJ;
	}

	public Integer getQuantidadeFuncionariosPJ() {
		return quantidadeFuncionariosPJ;
	}

	public void setQuantidadeFuncionariosPJ(Integer quantidadeFuncionariosPJ) {
		this.quantidadeFuncionariosPJ = quantidadeFuncionariosPJ;
	}

	public String getAtividadeEmpresaPJ() {
		return atividadeEmpresaPJ;
	}

	public void setAtividadeEmpresaPJ(String atividadeEmpresaPJ) {
		this.atividadeEmpresaPJ = atividadeEmpresaPJ;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public Empresa getEmpresaPortal() {
		return empresaPortal;
	}

	public void setEmpresaPortal(Empresa empresaPortal) {
		this.empresaPortal = empresaPortal;
	}
	
	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getEstadoCarro() {
		return estadoCarro;
	}

	public void setEstadoCarro(Integer estadoCarro) {
		this.estadoCarro = estadoCarro;
	}

	public boolean addAvalista(Avalista a){
		if(this.avalistas==null)
			this.avalistas=new ArrayList<Avalista>();
		
		if(!this.equals(a.getCliente())){
			if(a.getCliente()!=null && a.getCliente().getAvalistas()!=null){
				a.getCliente().getAvalistas().remove(a);
			}
			a.setCliente(this);
		}
		return avalistas.add(a);
	}

	public boolean addObs(Obs o){
		if(this.obs==null)
			this.obs=new ArrayList<Obs>();
		
		if(!this.equals(o.getCliente())){
			if(o.getCliente()!=null && o.getCliente().getObs()!=null){
				o.getCliente().getObs().remove(o);
			}
			o.setCliente(this);
		}
		return obs.add(o);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
