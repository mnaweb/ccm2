package br.com.ccm.dao;

import java.util.List;

import br.com.ccm.model.Modulo;

public interface IModuloDao extends IGenericDAO<Modulo>{

	public List<Modulo> listModulo(int qtdeReg);
	
}
