package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IAvalistaDao;
import br.com.ccm.model.Avalista;
import br.com.ccm.model.Cliente;

@Stateless
public class AvalistaDao extends GenericDAO<Avalista> implements IAvalistaDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6195161985482205032L;

	public AvalistaDao() {
		super(Avalista.class);
	}

	@SuppressWarnings("unchecked")
	public List<Avalista> listarAvalistaByCliente(Cliente cliente, int qtdeReg){
		HashMap<String, Object> params = new HashMap<String, Object>();

		String where = "";
		if(cliente!=null){
			where += " and a.cliente.id = :cliId";
			params.put("cliId",new Long(cliente.getId()));
		}
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		return execQuery("Select a from Avalista a "+where, params, qtdeReg);
	}
	
}
