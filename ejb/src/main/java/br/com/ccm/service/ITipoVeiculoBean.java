package br.com.ccm.service;

import java.util.ArrayList;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.TipoVeiculo;
import br.com.ccm.model.Usuario;

@Remote
public interface ITipoVeiculoBean{

	public ArrayList<TipoVeiculo> listaAll();
	
	public TipoVeiculo salvar(TipoVeiculo tipoVeiculo, Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(TipoVeiculo tipoVeiculo);
	
	public TipoVeiculo getById(Long id);

}
