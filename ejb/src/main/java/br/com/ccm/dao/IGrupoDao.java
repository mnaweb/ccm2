package br.com.ccm.dao;

import java.util.List;

import br.com.ccm.model.Grupo;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Usuario;

public interface IGrupoDao extends IGenericDAO<Grupo>{
	
	public List<Modulo> listPermissao(Usuario usuario, int qtdeReg);
	
}
