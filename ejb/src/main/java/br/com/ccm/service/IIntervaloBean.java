package br.com.ccm.service;

import java.util.ArrayList;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.Usuario;

@Remote
public interface IIntervaloBean{

	public ArrayList<Intervalo> listaAll();
	
	public Intervalo salvar(Intervalo intervalo, Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(Intervalo intervalo);
	
	public Intervalo getById(Long id);

}
