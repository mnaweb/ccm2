package br.com.ccm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="ts_usuario")
public class Usuario implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8362352539148792326L;
	@Id
	@Column(name="pk_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="ds_login")
	private String login;
	@Column(name="ds_nome")
	private String nome;
	@Column(name="ds_senha")
	private String senha;
	@Column(name="dh_inclusao")
	private Date inclusao;
	@Column(name="dh_alteracao")
	private Date alteracao;
	@Column(name="tg_admin")
	private boolean admin;
	@Column(name="ds_cor")
	private String cor;
	@Column(name="ds_telefone1")
	private String telefone1;
	@Column(name="ds_telefone2")
	private String telefone2;
	@ManyToOne
	@JoinColumn(name="fk_grupo")
	private Grupo grupo;
	@ManyToOne
	@JoinColumn(name="fk_vendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="fk_empresaPortal")
	private Empresa empresaPortal;
	
	public Usuario() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public boolean getAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	
	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	
	public Empresa getEmpresaPortal() {
		return empresaPortal;
	}

	public void setEmpresaPortal(Empresa empresaPortal) {
		this.empresaPortal = empresaPortal;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
