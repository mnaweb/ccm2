package br.com.ccm.jsf;

import java.lang.reflect.Method;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.el.ELResolver;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.ccm.controller.PrincipalControl;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

public class Funcoes {

	public static void alteraTitulo(String pagina){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		value.setPagina1(pagina);
	}

	public static void abaAtiva(String aba){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		value.setAbaAtiva(aba);
	}

	public static void alteraPag1(String pagina){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		value.setPagina1(pagina);
	}

	public static void alteraPag2(String pagina){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		value.setPagina2(pagina);
	}
   
	public static Object getManagerBean(String managerBean){
		FacesContext fc = FacesContext.getCurrentInstance();
		ELResolver elResolver = fc.getApplication().getELResolver();
		return elResolver.getValue(fc.getELContext(), null, managerBean);
	}
   
	public static void setarAcesso(String tarefa){
		PrincipalControl principalControl = (PrincipalControl) getManagerBean("mbPrincipal");
		
		principalControl.setarAcesso(tarefa);
	}
   
	public static void executarMetodo(String managerBean, String metodo){
		Object obj = getManagerBean(managerBean);
		
		Method[] m = obj.getClass().getMethods();
		
		for(Method mExe:m){
			if(mExe.getName().toUpperCase().equalsIgnoreCase(metodo)){
				try {
					mExe.invoke(obj,null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	public static String getValorDoParamAdd(String chave){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		
		String retorno = null;
		
		if(value.getParamAdd()!=null){
			String[] params = value.getParamAdd().split("\\s");
			
			for(String obj : params){
				String[] criterio = obj.split("=");
				if(criterio[0].equalsIgnoreCase(chave)){
					retorno = criterio[1]; 
				}
			}
		}
		return retorno;
	}

	public static void createMessageJSFErro(String texto, String obj){
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, texto, texto);
		FacesContext.getCurrentInstance().addMessage(obj, message);	
	}
	
	public static String getMessageFromResourceBundle(String key) {
		ResourceBundle bundle = null;
		String bundleName = "br.com.ccm.i18n.msg";
		
		String message = "";
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		
		try {
			bundle = ResourceBundle.getBundle(bundleName, locale, getCurrentLoader(bundleName));
		} catch (MissingResourceException e) {
			// bundle with this name not found;
		}
		if (bundle == null)
			return null;
		try {
			message = bundle.getString(key);
		} catch (Exception e) {
		}
		return message;
	}
	
	public static ClassLoader getCurrentLoader(Object fallbackClass) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if (loader == null)
			loader = fallbackClass.getClass().getClassLoader();
		return loader;
	}
	
	public static int getQtdeRegMax(){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		return value.getQtdereg();
	}

	public static Usuario getUsuarioLogado(){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		return value.getUsuario();
	}

	public static Empresa getEmpresa(){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		return value.getEmpresa();
	}

	public static String getLogoEmp(){
		PrincipalControl value = (PrincipalControl) getManagerBean("mbPrincipal");
		return value.getLogoEmp();
	}

}