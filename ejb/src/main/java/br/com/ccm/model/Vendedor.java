package br.com.ccm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cd_vendedor")
public class Vendedor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5648877522886349319L;
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "ds_nome")
	private String nome;
	@Column(name = "ds_email")
	private String email;
	@Column(name = "ds_telefone1")
	private String telefone1;
	@Column(name = "ds_telefone2")
	private String telefone2;
	@Column(name = "ds_logradouro")
	private String logradouro;
	@Column(name = "ds_agencia")
	private String agencia;	
	@Column(name = "nr_banco")
	private Integer banco;
	@Column(name = "ds_contaCorrente")
	private String contaCorrente;
	@Column(name = "ds_arquivo")
	private String arquivo;
	@Column(name = "fl_ccm")
	private boolean ccm;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@JoinColumn(name="fl_apresfichausu")
	private boolean apresentarFichasUsuario;
	@ManyToOne
	@JoinColumn(name="fk_empresaPortal")
	private Empresa empresaPortal;
	
	public Vendedor() {
		id = new Long(0);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isCcm() {
		return ccm;
	}
	public void setCcm(boolean ccm) {
		this.ccm = ccm;
	}
	public String getTelefone1() {
		return telefone1;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public String getTelefone2() {
		return telefone2;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public Integer getBanco() {
		return banco;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	public String getArquivo() {
		return arquivo;
	}
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}
	
	public boolean isApresentarFichasUsuario() {
		return apresentarFichasUsuario;
	}
	public void setApresentarFichasUsuario(boolean apresentarFichasUsuario) {
		this.apresentarFichasUsuario = apresentarFichasUsuario;
	}
	
	public Empresa getEmpresaPortal() {
		return empresaPortal;
	}
	public void setEmpresaPortal(Empresa empresaPortal) {
		this.empresaPortal = empresaPortal;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendedor other = (Vendedor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
