package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.ICoeficienteDao;
import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.TipoVeiculo;

@Stateless
public class CoeficienteDao extends GenericDAO<Coeficiente> implements ICoeficienteDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460340813643414204L;

	public CoeficienteDao() {
		super(Coeficiente.class);
	}

	@Override
	public List<Coeficiente> listaCoeficiente(TipoCliente tipoCliente, TipoVeiculo tipoVeiculo, Intervalo intervalo, BigDecimal percPontoDeEquilibrio, int qtdeReg){
	
		HashMap<String, Object> params = new HashMap<String, Object>();
		
		params.put("tipoCliente",new Long(tipoCliente.getId()));
		params.put("tipoVeiculo",new Long(tipoVeiculo.getId()));
		params.put("intervalo",new Long(intervalo.getId()));
		params.put("percPontoDeEquilibrio",percPontoDeEquilibrio);
		
		String query = "select coe from Coeficiente coe" +
						" where coe.tipoCliente.id	=:tipoCliente"+
						" and coe.tipoVeiculo.id	=:tipoVeiculo" +
						" and coe.intervalo.id	=:intervalo"+
						" and ((coe.configuracaoSimulador.percPontoDeEquilibrio<=:percPontoDeEquilibrio and coe.pontoDeEquilibrio=true)" +
						"   or (coe.configuracaoSimulador.percPontoDeEquilibrio>:percPontoDeEquilibrio and coe.pontoDeEquilibrio=false))" +
						" order by coe.prazo";
		
		@SuppressWarnings("unchecked")
		List<Coeficiente> listaCoe = execQuery(query, params, qtdeReg);

		return listaCoe;
	}
}
