package br.com.ccm.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IModuloBean;
import br.com.ccm.model.Modulo;

@Named(value="mbModulo")
@RequestScoped
public class ModuloControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6236327218630195014L;

	public ModuloControl(){
	}

	@EJB
	private IModuloBean moduloBean;
	
    private Comparator<Modulo> dateInclusao;
    private Comparator<Modulo> dateAlteracao;
    private List<Modulo> dataModel;
    private Modulo selection = new Modulo();
    private Modulo modulo = new Modulo();
	private List<SelectItem> tipoLista = new ArrayList<SelectItem>(); 
	
    public IModuloBean getModuloBean() {
		return moduloBean;
	}

	public void setModuloBean(IModuloBean moduloBean) {
		this.moduloBean = moduloBean;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public List<Modulo> getDataModel(){
		List<Modulo> lista = moduloBean.listModulo(Funcoes.getQtdeRegMax());
    	
		dataModel = lista;
        return dataModel;
    }

    public Modulo getSelection() {
        return selection;
    }

    public void setSelection(Modulo selection) {
        this.selection = selection;
    }

    public Comparator<Modulo> getDateInclusao(){
        if (dateInclusao == null){
            dateInclusao = new Comparator<Modulo>(){
                public int compare(Modulo o1, Modulo o2) {
                    return o1.getInclusao().compareTo(o2.getInclusao());
                }
            };
        }
        return dateInclusao;
    }

    public Comparator<Modulo> getDateAlteracao(){
        if (dateAlteracao == null){
            dateAlteracao = new Comparator<Modulo>(){
                public int compare(Modulo o1, Modulo o2) {
                    return o1.getAlteracao().compareTo(o2.getAlteracao());
                }
            };
        }
        return dateAlteracao;
    }
    
	public String salvar(){
		moduloBean.salvar(modulo, Funcoes.getUsuarioLogado());
		
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public void deletar(ActionEvent actionEvent){
		modulo = getSelection();
		
		moduloBean.deletar(modulo);
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.modulo = new Modulo();

    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar(ActionEvent actionEvent) {
		this.modulo = getSelection();
		
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public List<SelectItem> getTipoLista() {
        tipoLista = new ArrayList<SelectItem>();

        tipoLista.add(new SelectItem(new Integer(IModuloBean.TIPO_TAREFA_MENU),"Menu principal"));
        tipoLista.add(new SelectItem(new Integer(IModuloBean.TIPO_TAREFA_CRUD),"Tela de cadastro"));
        tipoLista.add(new SelectItem(new Integer(IModuloBean.TIPO_TAREFA_RELATORIO),"Tela de relatório"));
        tipoLista.add(new SelectItem(new Integer(IModuloBean.TIPO_TAREFA_GENERICA),"Tela generica"));

		return tipoLista;
	}

	public void setTipoLista(List<SelectItem> tipoLista) {
		this.tipoLista = tipoLista;
	}
	

}
