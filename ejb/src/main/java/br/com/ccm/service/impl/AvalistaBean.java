package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.ccm.dao.IAvalistaDao;
import br.com.ccm.model.Avalista;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.service.IAvalistaBean;

@Stateless
public class AvalistaBean implements IAvalistaBean,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1821878974537020608L;


	@EJB
	private IAvalistaDao dao;
			
	public List<Avalista> listAvalista(){
		return (ArrayList<Avalista>) dao.findAll();
	}
	public Avalista getAvalistaById(Long id){
		return dao.find(id);
	}

	public Avalista salvar(Avalista Avalista, Usuario usuarioLogado, Empresa empresa){
		if(Avalista.getId()==0){
			Avalista.setInclusao(new Date());
			if(Avalista.getCriador()==null || Avalista.getCriador().getId()==0)
				Avalista.setCriador(usuarioLogado);
			
			Avalista.setUsuinc(Avalista.getCriador().getNome());
		}else{
			Avalista.setAlteracao(new Date());
			Avalista.setAlterado(usuarioLogado);
			Avalista.setUsualt(Avalista.getAlterado().getNome());
		}
		
		dao.save(Avalista);
		
		return Avalista;
	}
	
	public void deletar(Avalista Avalista){
		dao.delete(Avalista);
	}
	
	public ArrayList<Avalista> listaAll(){
		return (ArrayList<Avalista>) dao.findAll();
	}

	public List<Avalista> listarAvalistaByCliente(Cliente cliente, int qtdereg){
		return dao.listarAvalistaByCliente(cliente, qtdereg);
	}
}
