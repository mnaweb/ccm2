package br.com.ccm.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.TipoVeiculo;
import br.com.ccm.model.Usuario;

@Remote
public interface ICoeficienteBean{

	public ArrayList<Coeficiente> listaAll();
	
	public Coeficiente salvar(Coeficiente coeficiente, Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(Coeficiente coeficiente);
	
	public Coeficiente getById(Long id);

	List<Coeficiente> listaCoeficiente(TipoCliente tipoCliente, TipoVeiculo tipoVeiculo, Intervalo intervalo, BigDecimal percPontoDeEquilibrio, int qtdereg);

}
