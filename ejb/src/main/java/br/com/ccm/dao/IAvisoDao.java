package br.com.ccm.dao;

import java.util.List;
import javax.ejb.Local;
import br.com.ccm.model.Aviso;

@Local
public interface IAvisoDao extends IGenericDAO<Aviso>{

	public List<Aviso> listAllByAtivo(int qtdeReg);	

}
