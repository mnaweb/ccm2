package br.com.ccm.service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Remote;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

@Remote
public interface IBancoBean{

	public ArrayList<Banco> listaAll(Usuario usuarioLogado, Empresa empresa, int qtdereg);
	
	public ArrayList<Banco> listAllByEmpresa(Empresa empresa, int qtdereg);
	
	public Banco salvar(Banco banco,Usuario usuarioLogado, Empresa empresa);
	
	public void deletar(Banco banco, int qtdereg) throws AplicacaoException;

	public List<Banco> listBancosByUsuario(Usuario usuario, int qtdereg);
	
}
