package br.com.ccm.service;

import java.util.ArrayList;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Aviso;
import br.com.ccm.model.Usuario;

@Remote
public interface IAvisoBean{

	public ArrayList<Aviso> listaAll();
	
	public Aviso salvar(Aviso aviso, Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(Aviso aviso);
	
	public Aviso getById(Long id);

	public ArrayList<Aviso> listAllByAtivo(int qtdereg);

}
