package br.com.ccm.dao;

import java.util.List;
import br.com.ccm.model.Avalista;
import br.com.ccm.model.Cliente;

public interface IAvalistaDao extends IGenericDAO<Avalista>{
	
	public List<Avalista> listarAvalistaByCliente(Cliente cliente, int qtdeReg);
	
}
