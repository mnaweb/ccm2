package br.com.ccm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "ts_permissao")
public class Permissao  implements Serializable{
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -1409502048529861831L;
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "fl_incluir")
	private boolean incluir;
	@Column(name = "fl_alterar")
	private boolean alterar;
	@Column(name = "fl_remover")
	private boolean remover;
	@ManyToOne
	@JoinColumn(name="fk_grupo")
	private Grupo grupo;
	@ManyToOne
	@JoinColumn(name="fk_modulo")
	private Modulo modulo;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@Column(name = "fk_criador")
	private Long criador;
	@Column(name = "tg_inativo")
	private Long inativo;
	
	public Permissao() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public boolean isIncluir() {
		return incluir;
	}

	public void setIncluir(boolean incluir) {
		this.incluir = incluir;
	}

	public boolean isAlterar() {
		return alterar;
	}

	public void setAlterar(boolean alterar) {
		this.alterar = alterar;
	}

	public boolean isRemover() {
		return remover;
	}

	public void setRemover(boolean remover) {
		this.remover = remover;
	}

	public Date getInclusao() {
		return inclusao;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public Modulo getModulo() {
		return modulo;
	}
	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Long getCriador() {
		return criador;
	}
	public void setCriador(Long criador) {
		this.criador = criador;
	}
	public Long getInativo() {
		return inativo;
	}
	public void setInativo(Long inativo) {
		this.inativo = inativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permissao other = (Permissao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
