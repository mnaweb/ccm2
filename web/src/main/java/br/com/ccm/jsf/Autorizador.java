package br.com.ccm.jsf;

import java.io.Serializable;

import javax.el.ELResolver;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import br.com.ccm.controller.PrincipalControl;

public class Autorizador implements PhaseListener,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 394021459356831528L;

	public static final String loginPage = "/sistema/login.xhtml";
	public static final String loginSite = "/site/loginSite.xhtml";

	@Override
	public void afterPhase(PhaseEvent event) {
		FacesContext fc = event.getFacesContext();

		if(fc.getViewRoot().getViewId().toUpperCase().indexOf("SISTEMA")>=0){
			if (fc.getViewRoot().getViewId().equals(loginPage)) {
				return;
			}
	
			ELResolver elResolver = fc.getApplication().getELResolver();
			PrincipalControl principalControl = (PrincipalControl) elResolver.getValue(fc
					.getELContext(), null, "mbPrincipal");
	
			if (principalControl == null || principalControl.getUsuario()==null) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Usuario não logado","Usuario não logado");
			
				fc.addMessage("formLogin", message);
	
				NavigationHandler nav = fc.getApplication().getNavigationHandler();
				nav.handleNavigation(fc, null, "Login");
				fc.renderResponse();
			}else{
				return;
			}
		}

	}

	@Override
	public void beforePhase(PhaseEvent arg0) {

	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}