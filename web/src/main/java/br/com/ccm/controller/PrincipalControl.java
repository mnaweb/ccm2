package br.com.ccm.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.model.DefaultMenuModel;
import org.primefaces.model.MenuModel;

import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IAvisoBean;
import br.com.ccm.service.IEmpresaBean;
import br.com.ccm.service.IModuloBean;
import br.com.ccm.service.IPermissaoBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.model.Aviso;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.PermissaoEnum;
import br.com.ccm.model.Usuario;

@ManagedBean(name="mbPrincipal")
@SessionScoped
public class PrincipalControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 256596959503843023L;
	
	private List<Modulo> listModulo;
	private Usuario usuario;
	private String nome;
	private String senha;
	private String cor;
	private MenuModel menu = new DefaultMenuModel();
	private String pagina1;
	private String pagina2;
	private String paginaAtiva;
	private String tituloFormAtivo;
	private String paramAdd;
	
	private List<Aviso> listAvisos;
	
	private Empresa empresa;
		
	private String titulo;
	
	@EJB
	private IPrincipalBean principalBean;
	private String abaAtiva;
	
	private int qtdereg;
	private boolean usuarioccm;
	
	private String logoEmp;
	private Long idEmpresa;
	private List<SelectItem> empresaLista = new ArrayList<SelectItem>();
	
	@EJB
	private IEmpresaBean empresaBean;
	
	private String tema;
	
	public static String ABA_CONSULTAR="PaginaConsultar";
	public static String ABA_DIGITAR="PaginaDigitar";

	@EJB
	private IPermissaoBean permissaoBean;

	@EJB
	private IAvisoBean avisoBean;

	public PrincipalControl(){
	}
	
	public IPrincipalBean getPrincipalBean() {
		return principalBean;
	}

	public void setPrincipalBean(IPrincipalBean principalBean) {
		this.principalBean = principalBean;
	}

	public IEmpresaBean getEmpresaBean() {
		return empresaBean;
	}

	public void setEmpresaBean(IEmpresaBean empresaBean) {
		this.empresaBean = empresaBean;
	}
	
	public IAvisoBean getAvisoBean() {
		return avisoBean;
	}

	public void setAvisoBean(IAvisoBean avisoBean) {
		this.avisoBean = avisoBean;
	}

	public List<Modulo> getListModulo(){
		listModulo = principalBean.getListModulo(usuario, getQtdereg());
		
		return listModulo;
	}
	public void setListModulo(List<Modulo> listModulo) {
		this.listModulo = listModulo;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getPagina1() {
		return pagina1;
	}
	public void setPagina1(String pagina1) {
		this.pagina1 = pagina1;
	}
	public String getPagina2() {
		return pagina2;
	}
	public void setPagina2(String pagina2) {
		this.pagina2 = pagina2;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getCor() {
    	if(usuario!=null){
    		cor=usuario.getCor();
    	}
        if(cor==null){
        	cor="blueSky";
        }
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
		Cookie stateCookie = new Cookie("estiloCor", this.cor);
		stateCookie.setMaxAge(30 * 24 * 60 * 60);
		((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(stateCookie);
	}

	public MenuModel getMenu() {
		menu = montarMenu();
		
		return menu;
	}

	public void setMenu(MenuModel menu) {
		this.menu = menu;
	}
		
	public MenuModel montarMenu(){
		List<Modulo> lista = getListModulo();
		
		MenuModel m = new DefaultMenuModel();
		Submenu grupo = null;
		MenuItem item = null;

		long guia = 0;
		for(Modulo reg:lista){
			if(guia!=reg.getGuia()){
				if(guia!=0){
					m.addSubmenu(grupo);
				}
				grupo = new Submenu();
				grupo.setLabel(reg.getTitulo());
				
				guia=reg.getGuia();
			}
			if(reg.getTipo()==IModuloBean.TIPO_TAREFA_CRUD){
				item = new MenuItem();
				item.setValue(reg.getTitulo());
				item.setActionExpression(FacesContext.getCurrentInstance().getApplication().getExpressionFactory()
						.createMethodExpression(FacesContext.getCurrentInstance().getELContext(),"#{mbPrincipal.abrirTelaCrud}", java.lang.Object.class,new Class[]{}));
						
				UIParameter param1 = new UIParameter();
				param1.setName("pagina1");
				param1.setValue("/sistema/"+reg.getPagina1());

				UIParameter param2 = new UIParameter();
				param2.setName("pagina2");
				param2.setValue("/sistema/"+reg.getPagina2());

				UIParameter paramTitulo = new UIParameter();
				paramTitulo.setName("titulo");
				paramTitulo.setValue(reg.getTitulo());

				UIParameter pTarefa = new UIParameter();
				pTarefa.setName("tarefa");
				pTarefa.setValue(reg.getTarefa());
				
				UIParameter paramAdd = new UIParameter();
				paramAdd.setName("paramAdd");
				paramAdd.setValue("");

				item.getChildren().add(param1);
				item.getChildren().add(param2);
				item.getChildren().add(paramTitulo);
				item.getChildren().add(pTarefa);
				item.getChildren().add(paramAdd);
				
				grupo.getChildren().add(item);
			}
		}
		if(guia!=0){
			m.addSubmenu(grupo);
		}

		return m;
	}
	
	public String logar() throws NoSuchAlgorithmException{
		usuario = principalBean.logar(nome,senha,empresa,getQtdereg());
		
		if(usuario!=null){
			usuarioccm = usuario.getVendedor()==null?false:usuario.getVendedor().isCcm();

			try {
				if(usuario.getVendedor().getArquivo()!=null && !usuario.getVendedor().getArquivo().equals("")){
					HttpServletRequest request = (HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
					String realPath = request.getSession().getServletContext().getRealPath("");
					
					if(!new File(realPath + usuario.getVendedor().getArquivo()).exists()){
						File fileBack = new File(Funcoes.getMessageFromResourceBundle("caminho.logo")+"/sistema/img_usuario/"+usuario.getVendedor().getArquivo().replaceAll("/sistema/img_usuario/", ""));
			            FileInputStream fin;
						fin = new FileInputStream(fileBack);
						File arquivoDest = new File(realPath + usuario.getVendedor().getArquivo());
			            FileOutputStream fos = new FileOutputStream(arquivoDest);
			            final byte[] bytes = new byte[1000000];  
			            for (int lidos = -1; (lidos = fin.read(bytes, 0, 1000000)) != -1; fos.write(bytes, 0, lidos)) {}  
			            fos.flush();
			            fos.close();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			setListAvisos(avisoBean.listAllByAtivo(getQtdereg()));
			
			return "Principal";
		}else{
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado.", "Usuário não encontrado.");
			FacesContext.getCurrentInstance().addMessage(null, message);

			return null;
		}
	}
	public String abrirTelaCrud(){
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getParameter("pagina1")!=null){
			String pagina1 = new String( req.getParameter("pagina1") );
			this.pagina1 = pagina1;
		}
		if(req.getParameter("pagina2")!=null){
			String pagina2 = new String( req.getParameter("pagina2") );
			this.pagina2 = pagina2;
		}
		if(req.getParameter("titulo")!=null){
			String titulo = new String( req.getParameter("titulo") );
			this.titulo = titulo;
		}
		if(req.getParameter("aba")!=null){
			String aba = new String( req.getParameter("aba") );
			this.abaAtiva = aba;
		}
		if(req.getParameter("paramAdd")!=null){
			String paramAdd = new String( req.getParameter("paramAdd") );
			this.paramAdd = paramAdd;
		}else{
			this.paramAdd = "";
		}
		if(req.getParameter("tarefa")!=null){
			String tarefa = new String( req.getParameter("tarefa") );
			setarAcesso(tarefa);
		}

		if(pagina1!=null){
			paginaAtiva = pagina1;
			tituloFormAtivo = "Consultar";
		}
		return "crud";
	}
	public String getAbaAtiva() {
		if(abaAtiva==null)
			abaAtiva=ABA_CONSULTAR;

		if(abaAtiva.equals(""))
			abaAtiva=ABA_CONSULTAR;

		return abaAtiva;
	}
	public void setAbaAtiva(String abaAtiva) {
		if(abaAtiva.equals(ABA_DIGITAR)){
			paginaAtiva = pagina2;
			tituloFormAtivo = "Digitar";
		}else{
			paginaAtiva = pagina1;
			tituloFormAtivo = "Consultar";
		}
		this.abaAtiva = abaAtiva;
	}

	public String sair(){
		this.usuario	= null;
		
		return "Login";
	}

	public String getPaginaAtiva() {
		return paginaAtiva;
	}

	public void setPaginaAtiva(String paginaAtiva) {
		this.paginaAtiva = paginaAtiva;
	}

	public String getTituloFormAtivo() {
		return tituloFormAtivo;
	}

	public void setTituloFormAtivo(String tituloFormAtivo) {
		this.tituloFormAtivo = tituloFormAtivo;
	}

	public void abrirTelaCrud2(ActionEvent actionEvent){

		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getParameter("pagina1")!=null){
			String pagina1 = new String( req.getParameter("pagina1") );
			this.pagina1 = pagina1;
		}
		if(req.getParameter("pagina2")!=null){
			String pagina2 = new String( req.getParameter("pagina2") );
			this.pagina2 = pagina2;
		}
		if(req.getParameter("titulo")!=null){
			String titulo = new String( req.getParameter("titulo") );
			this.titulo = titulo;
		}
		if(req.getParameter("paramAdd")!=null){
			String paramAdd = new String( req.getParameter("paramAdd") );
			this.paramAdd = paramAdd;
		}
		if(req.getParameter("aba")!=null){
			String aba = new String( req.getParameter("aba") );
			setAbaAtiva(aba);
		}
		if(pagina1!=null && (paginaAtiva==null || paginaAtiva.equals(""))){
			paginaAtiva = pagina1;
			tituloFormAtivo = "Consultar";
		}

	}
	public String abrirTelaGenerica(){
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getParameter("pagina1")!=null){
			String pagina1 = new String( req.getParameter("pagina1") );
			this.pagina1 = pagina1;
		}
		if(req.getParameter("titulo")!=null){
			String titulo = new String( req.getParameter("titulo") );
			this.titulo = titulo;
		}
		if(req.getParameter("paramAdd")!=null){
			String paramAdd = new String( req.getParameter("paramAdd") );
			this.paramAdd = paramAdd;
		}

		String executarMetodo = "";
		if(req.getParameter("executarMetodo")!=null){
			executarMetodo = new String( req.getParameter("executarMetodo") );
		}
		String managerBean = "";
		if(req.getParameter("managerBean")!=null){
			managerBean = new String( req.getParameter("executarMetodo") );
		}
		if(!executarMetodo.equals("") && !managerBean.equals("")){
			Funcoes.executarMetodo(managerBean, executarMetodo);
		}
		
		paginaAtiva = pagina1;
		
		return "generica";
	}

	public int getQtdereg() {
		if(qtdereg==0)
			qtdereg=500;
		
		return qtdereg;
	}

	public void setQtdereg(int qtdereg) {
		this.qtdereg = qtdereg;
	}

	public boolean isUsuarioccm() {
		return usuarioccm;
	}

	public void setUsuarioccm(boolean usuarioccm) {
		this.usuarioccm = usuarioccm;
	}

	public void setarAcesso(String tarefa){
		if(!getUsuario().getAdmin()){
			List<Permissao> listPermissao = principalBean.listPermissaoUsuario(usuario, tarefa, getQtdereg());
		}
	}
	
	public void site(ActionEvent actionEvent) throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletResponse res = (HttpServletResponse) context.getExternalContext().getResponse();
		res.sendRedirect(empresa.getRelatorioUrl());
	}

	public String getParamAdd() {
		return paramAdd;
	}

	public void setParamAdd(String paramAdd) {
		this.paramAdd = paramAdd;
	}

	public Empresa getEmpresa() {
		if(empresa==null){
			this.empresa = new Empresa();
			
	    	this.empresa.setIndexMsgTxt1("Veículos a partir de 2002");
	    	this.empresa.setIndexMsgTxt2("Até 100% do valor de mercado(FIPE);");
	    	this.empresa.setIndexMsgTxt3("Prazo: 12 a 60 meses;");
	    	this.empresa.setIndexMsgTxt4("Após a aprovação da proposta, o veículo será vistoriado a domicílio.");
	    	this.empresa.setIndexMsgTxt5("O crédito será efetuado na conta-corrente do vendedor do veículo(DUT) ou na conta-corrente da revenda, caso emita a NF;");
	    	this.empresa.setIndexMsgTxt6("Os processos (completos) que forem enviados até às 12 horas, serão efetivados no mesmo dia e o crédito será efetuado a partir das 18 horas na conta corrente.");
	
	    	this.empresa.setMenuDadosTxt1("Fone:(11) 2765-5547");
	    	this.empresa.setMenuDadosTxt2("Fone:(11) 2063-5099");
	    	this.empresa.setMenuDadosTxt3("comercial@consultoriaccm.com.br");
	    	this.empresa.setMenuDadosTxt4("Horário de atendimento:");
	    	this.empresa.setMenuDadosTxt5("Seg. a sexta 8:30hs às 18hs");
	    	this.empresa.setMenuDadosTxt6("Sábado 8:30 às 13hs  (11)7844-0003 / (11)7844-3850");
	
	    	this.empresa.setRelatorioUrl("http://www.consultoriaccm.com.br/");
	    	this.empresa.setRelatorioEmail("comercial@consultoriaccm.com.br");
	    	this.empresa.setRelatorioFone1("(11)2765-5547");
	    	this.empresa.setRelatorioFone2("(11)2063-5099");
		}
    	
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getLogoEmp() {
		if(this.empresa!=null && this.empresa.getId()!=0)
			logoEmp = "/images/Logo_"+this.empresa.getUrl()+".png";
		else
			logoEmp = Funcoes.getMessageFromResourceBundle("logo_padrao");
		
		return logoEmp;
	}

	public void setLogoEmp(String logoEmp) {
		this.logoEmp = logoEmp;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		if(idEmpresa!=null && !idEmpresa.equals(0))
			this.empresa = empresaBean.getById(idEmpresa);
		else
			this.empresa = null;
		
		this.idEmpresa = idEmpresa;
	}

	public String getTema() {
		if(this.empresa!=null && this.empresa.getId()!=0)
			tema = this.empresa.getTema();
		else
			tema = Funcoes.getMessageFromResourceBundle("theme");
		
		if(tema==null || tema.equals(""))
			tema = "home";
		
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}

	public List<SelectItem> getEmpresaLista() {
		empresaLista = new ArrayList<SelectItem>();

		List<Empresa> listAux = empresaBean.listAllByAtivo(getQtdereg());
		empresaLista.add(new SelectItem(new Empresa(),"Todas"));
		for(Empresa obj:listAux){
			empresaLista.add(new SelectItem(obj,obj.getNome()));
		}
				
		return empresaLista;
	}

	public void setEmpresaLista(List<SelectItem> empresaLista) {
		this.empresaLista = empresaLista;
	}
	
	public IPermissaoBean getPermissaoBean() {
		return permissaoBean;
	}

	public void setPermissaoBean(IPermissaoBean permissaoBean) {
		this.permissaoBean = permissaoBean;
	}

	public boolean verifAcesso(String tarefa,int acao){
		if(usuario.getAdmin())
			return true;
		else
			return permissaoBean.verifAcesso(usuario, tarefa, acao, getQtdereg());
	}

	public List<Aviso> getListAvisos() {
		return listAvisos;
	}

	public void setListAvisos(List<Aviso> listAvisos) {
		this.listAvisos = listAvisos;
	}
	
	public boolean isPossuiAvisos(){
		return this.listAvisos.size()>0;
	}
	
	public String getMsgAviso(){
		String msg ="";
		for(Aviso aviso : this.listAvisos){
			String avisoMsg = aviso.getAviso().replaceAll("\r\n", "\\\\n");
			msg+=avisoMsg+"\\n\\n--------------- >< -----------------\\n\\n";
		}
		return msg;
	}
}
