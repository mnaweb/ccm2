package br.com.ccm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="sm_simulacao")
public class Simulacao  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5059542716578017911L;
	
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name="fk_intervalo")
	private Intervalo intervalo;
	@ManyToOne
	@JoinColumn(name="fk_tipoCliente")
	private TipoCliente tipoCliente;
	@ManyToOne
	@JoinColumn(name="fk_tipoVeiculo")
	private TipoVeiculo tipoVeiculo;
	@Column(name = "ds_prazo")
	private String prazo;
	@Column(name = "vl_entrada")
	private BigDecimal valorEntrada;
	@Column(name = "vl_veiculo")
	private BigDecimal valorVeiculo;
	@Column(name = "vl_prazo12")
	private BigDecimal prazo12;
	@Column(name = "vl_prazo18")
	private BigDecimal prazo18;
	@Column(name = "vl_prazo24")
	private BigDecimal prazo24;
	@Column(name = "vl_prazo30")
	private BigDecimal prazo30;
	@Column(name = "vl_prazo36")
	private BigDecimal prazo36;
	@Column(name = "vl_prazo42")
	private BigDecimal prazo42;
	@Column(name = "vl_prazo48")
	private BigDecimal prazo48;
	@Column(name = "vl_prazo60")
	private BigDecimal prazo60;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	
	public Simulacao() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Intervalo getIntervalo() {
		return intervalo;
	}

	public void setIntervalo(Intervalo intervalo) {
		this.intervalo = intervalo;
	}

	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public String getPrazo() {
		return prazo;
	}

	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}

	public BigDecimal getValorEntrada() {
		return valorEntrada;
	}

	public void setValorEntrada(BigDecimal valorEntrada) {
		this.valorEntrada = valorEntrada;
	}

	public BigDecimal getValorVeiculo() {
		return valorVeiculo;
	}

	public void setValorVeiculo(BigDecimal valorVeiculo) {
		this.valorVeiculo = valorVeiculo;
	}

	public BigDecimal getPrazo12() {
		return prazo12;
	}

	public void setPrazo12(BigDecimal prazo12) {
		this.prazo12 = prazo12;
	}

	public BigDecimal getPrazo18() {
		return prazo18;
	}

	public void setPrazo18(BigDecimal prazo18) {
		this.prazo18 = prazo18;
	}

	public BigDecimal getPrazo24() {
		return prazo24;
	}

	public void setPrazo24(BigDecimal prazo24) {
		this.prazo24 = prazo24;
	}

	public BigDecimal getPrazo30() {
		return prazo30;
	}

	public void setPrazo30(BigDecimal prazo30) {
		this.prazo30 = prazo30;
	}

	public BigDecimal getPrazo36() {
		return prazo36;
	}

	public void setPrazo36(BigDecimal prazo36) {
		this.prazo36 = prazo36;
	}

	public BigDecimal getPrazo42() {
		return prazo42;
	}

	public void setPrazo42(BigDecimal prazo42) {
		this.prazo42 = prazo42;
	}

	public BigDecimal getPrazo48() {
		return prazo48;
	}

	public void setPrazo48(BigDecimal prazo48) {
		this.prazo48 = prazo48;
	}

	public BigDecimal getPrazo60() {
		return prazo60;
	}

	public void setPrazo60(BigDecimal prazo60) {
		this.prazo60 = prazo60;
	}

	public Usuario getCriador() {
		return criador;
	}

	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

	public Date getInclusao() {
		return inclusao;
	}

	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}

	public Date getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Simulacao other = (Simulacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
