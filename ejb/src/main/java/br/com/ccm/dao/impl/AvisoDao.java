package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IAvisoDao;
import br.com.ccm.model.Aviso;

@Stateless
public class AvisoDao extends GenericDAO<Aviso> implements IAvisoDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460340813643414204L;

	public AvisoDao() {
		super(Aviso.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Aviso> listAllByAtivo(int qtdeReg) {
		return execQuery(" from Aviso where ativo=true order by aviso", null, qtdeReg);
	}
}
