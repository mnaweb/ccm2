package br.com.ccm.dao;

import javax.ejb.Local;

import br.com.ccm.model.Simulacao;

@Local
public interface ISimulacaoDao extends IGenericDAO<Simulacao>{	

}
