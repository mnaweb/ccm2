package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.IModuloDao;
import br.com.ccm.service.IModuloBean;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Usuario;

@Stateless
public class ModuloBean implements IModuloBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6057722578609313799L;

	@EJB
	private IModuloDao dao;
		
	public static int TIPO_TAREFA_MENU = 0;
	public static int TIPO_TAREFA_CRUD = 1;
	public static int TIPO_TAREFA_RELATORIO = 2;
	public static int TIPO_TAREFA_GENERICA = 3;
	
	public List<Modulo> listModulo(int qtdereg){
		return dao.listModulo(qtdereg);
	}
	
	public Modulo salvar(Modulo modulo, Usuario usuarioLogado){
		if(modulo.getId()==0){
			modulo.setInclusao(new Date());
			modulo.setCriador(usuarioLogado);
		}else{
			modulo.setAlteracao(new Date());
		}
		
		dao.save(modulo);
		
		return modulo;
	}
	
	public void deletar(Modulo modulo){
		dao.delete(modulo);
	}

}
