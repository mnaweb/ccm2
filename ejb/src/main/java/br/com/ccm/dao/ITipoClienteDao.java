package br.com.ccm.dao;

import javax.ejb.Local;

import br.com.ccm.model.TipoCliente;

@Local
public interface ITipoClienteDao extends IGenericDAO<TipoCliente>{	

}
