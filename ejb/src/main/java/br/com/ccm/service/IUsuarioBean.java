package br.com.ccm.service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Remote;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Remote
public interface IUsuarioBean{

	public Usuario getUsuarioNomeESenha(String nome, String senha, Usuario usuarioLogado, Empresa empresa, int qtdereg);

	public ArrayList<Usuario> listaAll();
	
	public void salvar(Usuario usuario, Usuario usuarioLogado, Empresa empresa, int qtdereg) throws NoSuchAlgorithmException, AplicacaoException;

	public void deletar(Usuario usuario, int qtdereg) throws AplicacaoException;

	public Usuario getById(Long id);

	public List<Usuario> listUsuarioPorVendedor(Vendedor vendedor, int qtdereg);

	public Usuario getUsuarioEmDuplicidade(String nome, Empresa empresa, Long id, int qtdereg);
	
}
