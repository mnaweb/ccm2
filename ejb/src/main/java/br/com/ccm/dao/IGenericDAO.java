package br.com.ccm.dao;

import java.util.List;
import java.util.Map;

public interface IGenericDAO<T> {
	 
    public void save(T entity);
 
    public void delete(Object id);
 
    public T update(T entity);
 
    public T find(Long entityID);
 
    public List<T> findAll();
 
    public List execQuery(String hql,Map params, int qtdeRegMax);
        
    public Object execQueryObj(String hql,Map params, int qtdeRegMax);

    public List<T> listAllByOrder(String order);
}