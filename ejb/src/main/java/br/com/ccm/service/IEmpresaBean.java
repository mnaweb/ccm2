package br.com.ccm.service;

import java.util.ArrayList;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

@Remote
public interface IEmpresaBean{

	public ArrayList<Empresa> listaAll();
	
	public Empresa salvar(Empresa empresa,Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(Empresa empresa);
	
	public void criarPasta(Empresa empresa);
	
	public Empresa getById(Long id);

	public ArrayList<Empresa> listAllByAtivo(int qtdereg);

}
