package br.com.ccm.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.ICoeficienteDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.ICoeficienteBean;
import br.com.ccm.model.Coeficiente;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.TipoVeiculo;
import br.com.ccm.model.Usuario;

@Stateless
public class CoeficienteBean implements ICoeficienteBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private ICoeficienteDao dao;
	
	public ArrayList<Coeficiente> listaAll(){
		return (ArrayList<Coeficiente>) dao.findAll();
	}
	
	public Coeficiente salvar(Coeficiente coeficiente, Usuario usuarioLogado) throws AplicacaoException{
		
		if(coeficiente.getId()==0){
			coeficiente.setInclusao(new Date());
			coeficiente.setCriador(usuarioLogado);
			
			dao.save(coeficiente);
		}else{
			coeficiente.setAlteracao(new Date());
			
			dao.save(coeficiente);
		}
		
		return coeficiente;
	}
	
	public void deletar(Coeficiente coeficiente){
		dao.delete(coeficiente);
	}
	
	public Coeficiente getById(Long id){
		return dao.find(id);
	}

	@Override
	public List<Coeficiente> listaCoeficiente( TipoCliente tipoCliente, TipoVeiculo tipoVeiculo, Intervalo intervalo, BigDecimal percPontoDeEquilibrio, int qtdereg){
		return dao.listaCoeficiente(tipoCliente, tipoVeiculo, intervalo, percPontoDeEquilibrio, qtdereg);		
	}
}
