package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IUsuarioDao;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class UsuarioDao extends GenericDAO<Usuario> implements IUsuarioDao,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4823467710292904019L;

	public UsuarioDao() {
		super(Usuario.class);
	}

	@SuppressWarnings("unchecked")
	public Usuario getUsuarioNomeESenha(String nome, String senha, Usuario usuarioLogado, Empresa empresa, int qtdeReg){

		Usuario usu = null;
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("login",nome);
		params.put("senha",senha);
		
		String and = "";
		if(empresa!=null && empresa.getId()!=0){
			and = " and ((emp.id = :idEmp and emp.ativo=true) or usu.admin = true)";
			params.put("idEmp",empresa.getId());
		}

		List<Usuario> listaUsu = execQuery("Select usu from Usuario as usu left join usu.empresaPortal as emp where usu.login=:login and usu.senha=:senha "+and, params, qtdeReg);
		
		for(Usuario obj:listaUsu){
			usu = obj;
			break;
		}
		
		return usu;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> listUsuarioPorVendedor(Vendedor vendedor, int qtdeReg){

		HashMap<String, Long> params = new HashMap<String, Long>();
		params.put("venId",vendedor.getId());
		List<Usuario> listaUsu = execQuery("from Usuario where vendedor.id=:venId order by nome", params, qtdeReg);
				
		return listaUsu;
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> listAll(Usuario usuarioLogado, Empresa empresa, int qtdeReg){

		HashMap<String, Long> params = new HashMap<String, Long>();

		String where = "";
		if(usuarioLogado!=null){
			if(!usuarioLogado.getAdmin() || (empresa!=null && empresa.getId()!=0)){
				where += " and empresaPortal.id = :idEmp";
				params.put("idEmp",empresa.getId());
			}
		}
		
		if(!where.equals("")){
			where = " where "+where.substring(5);
		}
		
		List<Usuario> listaUsu = execQuery("from Usuario "+where+" order by nome", params, qtdeReg);
				
		return listaUsu;
	}

	@SuppressWarnings("unchecked")
	public Usuario getUsuarioEmDuplicidade(String nome, Empresa empresa, Long id, int qtdeReg){

		Usuario usu = null;
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("login",nome);
		params.put("id",id);
		params.put("emp",empresa==null?new Long(0):empresa.getId());
		List<Usuario> listaUsu = execQuery("from Usuario where login=:login and coalesce(empresaPortal.id,0)=:emp and id<>:id", params,qtdeReg);
		
		for(Usuario obj:listaUsu){
			usu = obj;
			break;
		}
		
		return usu;
	}

}
