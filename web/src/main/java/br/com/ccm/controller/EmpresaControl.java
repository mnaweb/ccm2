package br.com.ccm.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IEmpresaBean;
import br.com.ccm.model.Empresa;

@Named(value="mbEmpresa")
@SessionScoped
public class EmpresaControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7720254336076351767L;

	public EmpresaControl(){
	}

	@EJB
	private IEmpresaBean empresaBean;

    private List<Empresa> dataModel;
    private Empresa selection = new Empresa();
    private Empresa empresa = new Empresa();
    
    private Map<String, String> themes;

    private StreamedContent imgEmpresa;
    
    @PostConstruct  
    public void init() {  
        themes = new TreeMap<String, String>();  
        themes.put("Aristo", "aristo");  
        themes.put("Black-Tie", "black-tie");  
        themes.put("Blitzer", "blitzer");  
        themes.put("Bluesky", "bluesky");  
        themes.put("Casablanca", "casablanca");  
        themes.put("Cupertino", "cupertino");  
        themes.put("Dark-Hive", "dark-hive");  
        themes.put("Dot-Luv", "dot-luv");  
        themes.put("Eggplant", "eggplant");  
        themes.put("Excite-Bike", "excite-bike");  
        themes.put("Flick", "flick");  
        themes.put("Glass-X", "glass-x");  
        themes.put("Home", "home");  
        themes.put("Hot-Sneaks", "hot-sneaks");  
        themes.put("Humanity", "humanity");  
        themes.put("Le-Frog", "le-frog");  
        themes.put("Midnight", "midnight");  
        themes.put("Mint-Choc", "mint-choc");  
        themes.put("Overcast", "overcast");  
        themes.put("Pepper-Grinder", "pepper-grinder");  
        themes.put("Redmond", "redmond");  
        themes.put("Rocket", "rocket");  
        themes.put("Sam", "sam");  
        themes.put("Smoothness", "smoothness");  
        themes.put("South-Street", "south-street");  
        themes.put("Start", "start");  
        themes.put("Sunny", "sunny");  
        themes.put("Swanky-Purse", "swanky-purse");  
        themes.put("Trontastic", "trontastic");  
        themes.put("UI-Darkness", "ui-darkness");  
        themes.put("UI-Lightness", "ui-lightness");  
        themes.put("Vader", "vader");  
    }
    
	public IEmpresaBean getEmpresaBean() {
		return empresaBean;
	}

	public void setEmpresaBean(IEmpresaBean empresaBean) {
		this.empresaBean = empresaBean;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa Empresa) {
		this.empresa = Empresa;
	}

	public List<Empresa> getDataModel(){
    	
		List<Empresa> lista = empresaBean.listaAll();
    	
		dataModel = lista;
        return dataModel;
    }

    public Empresa getSelection() {
        return selection;
    }

    public void setSelection(Empresa selection) {
        this.selection = selection;
    }
    
	public String salvar(){
		try {
			empresaBean.salvar(empresa, Funcoes.getUsuarioLogado());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}

		Funcoes.setarAcesso("empresa");	
    	Funcoes.alteraPag1("ferramenta/empresaConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/empresaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.setarAcesso("empresa");	
    	Funcoes.alteraPag1("ferramenta/empresaConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/empresaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String deletar(ActionEvent actionEvent){
		
		empresa = getSelection();
		
		empresaBean.deletar(empresa);
		
		return "empresaConsultar";
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.empresa = new Empresa();
		
    	this.empresa.setIndexMsgTxt1("Veiculos a partir de 2002");
    	this.empresa.setIndexMsgTxt2("Ate 100% do valor de mercado(FIPE);");
    	this.empresa.setIndexMsgTxt3("Prazo: 12 a 60 meses;");
    	this.empresa.setIndexMsgTxt4("Apos a aprovacao da proposta, o veiculo sera vistoriado a domicilio.");
    	this.empresa.setIndexMsgTxt5("O credito sera efetuado na conta-corrente do vendedor do veiculo(DUT) ou na conta-corrente da revenda, caso emita a NF;");
    	this.empresa.setIndexMsgTxt6("Os processos (completos) que forem enviados ate as 12 horas, serao efetivados no mesmo dia e o credito sera efetuado a partir das 18 horas na conta corrente.");

    	this.empresa.setMenuDadosTxt1("Fone:(11) 2765-5547");
    	this.empresa.setMenuDadosTxt2("Fone:(11) 2063-5099");
    	this.empresa.setMenuDadosTxt3("comercial@consultoriaccm.com.br");
    	this.empresa.setMenuDadosTxt4("Horario de atendimento:");
    	this.empresa.setMenuDadosTxt5("Seg. a sexta 8:30hs as 18hs");
    	this.empresa.setMenuDadosTxt6("Sabado 8:30 as 13hs - (11)7844-0003 / (11)7844-3850");

    	this.empresa.setRelatorioUrl("http://www.consultoriaccm.com.br/");
    	this.empresa.setRelatorioEmail("comercial@consultoriaccm.com.br");
    	this.empresa.setRelatorioFone1("(11)2765-5547");
    	this.empresa.setRelatorioFone2("(11)2063-5099");
    	
    	Funcoes.setarAcesso("empresa");	
    	Funcoes.alteraPag1("ferramenta/empresaConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/empresaDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar() {
		this.empresa = getSelection();
		
		Funcoes.setarAcesso("empresa");	
    	Funcoes.alteraPag1("ferramenta/empresaConsultar.xhtml");
    	Funcoes.alteraPag2("ferramenta/empresaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public Map<String, String> getThemes() {
		return themes;
	}

	public void setThemes(Map<String, String> themes) {
		this.themes = themes;
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
            empresa.setImage(event.getFile().getContents());
            
            FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");  
            FacesContext.getCurrentInstance().addMessage("arq", msg);
        } catch (Exception ex) {  
        	ex.printStackTrace();
        }
	}

	public StreamedContent getImgEmpresa() {
		if(empresa.getImage()!=null)
			imgEmpresa = new DefaultStreamedContent(new ByteArrayInputStream(empresa.getImage()), "image/png");
		else
			imgEmpresa = new DefaultStreamedContent();
		
		return imgEmpresa;
	}

	public void setImgEmpresa(StreamedContent imgEmpresa) {
		this.imgEmpresa = imgEmpresa;
	}

    public void recriarPastas() {
    	List<Empresa> listaEmp = empresaBean.listaAll();
    	for(Empresa emp:listaEmp){
    		empresaBean.criarPasta(emp);
    	}
    }

}
