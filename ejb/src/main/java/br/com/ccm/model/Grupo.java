package br.com.ccm.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="ts_grupo")
public class Grupo  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6555008422498947501L;
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "ds_grupo")
	private String grupo;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@Column(name = "tg_inativo")
	private Long inativo;
	@OneToMany(targetEntity=Permissao.class,mappedBy="grupo",cascade=CascadeType.MERGE)
	private Collection<Permissao> permissoes;
	
	public Grupo() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Long getInativo() {
		return inativo;
	}
	public void setInativo(Long inativo) {
		this.inativo = inativo;
	}
	public Collection<Permissao> getPermissoes() {
		if(permissoes==null)
			permissoes = new ArrayList<Permissao>();

		return permissoes;
	}
	public void setPermissoes(Collection<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

	public boolean addPermissao(Permissao p){
		if(this.permissoes==null)
			this.permissoes=new ArrayList<Permissao>();
		
		if(!this.equals(p.getGrupo())){
			if(p.getGrupo()!=null && p.getGrupo().getPermissoes()!=null){
				p.getGrupo().getPermissoes().remove(p);
			}
			p.setGrupo(this);
		}
		return permissoes.add(p);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupo other = (Grupo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
