package br.com.ccm.service;

import java.util.ArrayList;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Simulacao;
import br.com.ccm.model.Usuario;


@Remote
public interface ISimulacaoBean{

	public ArrayList<Simulacao> listaAll();
	
	public Simulacao salvar(Simulacao simulacao, Usuario usuarioLogado) throws AplicacaoException;
	
	public void deletar(Simulacao simulacao);
	
	public Simulacao getById(Long id);

	Simulacao calcular(Simulacao simulacao, Usuario usuarioLogado, int qtdereg) throws AplicacaoException;

}
