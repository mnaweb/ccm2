package br.com.ccm.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;

import br.com.ccm.dao.IModuloDao;
import br.com.ccm.model.Modulo;

@Stateless
public class ModuloDao extends GenericDAO<Modulo> implements IModuloDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8305093278453844565L;

	public ModuloDao() {
		super(Modulo.class);
	}

	@SuppressWarnings("unchecked")
	public List<Modulo> listModulo(int qtdeReg){

		List<Modulo> listaMod = execQuery(" from Modulo order by guia, tipo, posicao", null, qtdeReg);

		return listaMod;
	}

}
