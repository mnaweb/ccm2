package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IIntervaloBean;
import br.com.ccm.service.ISimulacaoBean;
import br.com.ccm.service.ITipoClienteBean;
import br.com.ccm.service.ITipoVeiculoBean;
import br.com.ccm.model.Intervalo;
import br.com.ccm.model.Simulacao;
import br.com.ccm.model.TipoCliente;
import br.com.ccm.model.TipoVeiculo;

@Named(value="mbSimulacao")
@ConversationScoped
public class SimulacaoControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5690636436369060328L;

	public SimulacaoControl(){
	}

	@EJB
	private ISimulacaoBean simulacaoBean;
	
	@EJB
	private ITipoClienteBean tipoClienteBean;
	
	@EJB
	private ITipoVeiculoBean tipoVeiculoBean;
	
	@EJB
	private IIntervaloBean intervaloBean;
	
    private List<TipoVeiculo> listaTipoVeiculos;
    private List<TipoCliente> listaTipoClientes;
    private List<Intervalo> listaIntervalos;
    private Simulacao simulacao = new Simulacao();

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public ISimulacaoBean getSimulacaoBean() {
		return simulacaoBean;
	}

	public void setSimulacaoBean(ISimulacaoBean simulacaoBean) {
		this.simulacaoBean = simulacaoBean;
	}

	public ITipoClienteBean getTipoClienteBean() {
		return tipoClienteBean;
	}

	public void setTipoClienteBean(ITipoClienteBean tipoClienteBean) {
		this.tipoClienteBean = tipoClienteBean;
	}

	public ITipoVeiculoBean getTipoVeiculoBean() {
		return tipoVeiculoBean;
	}

	public void setTipoVeiculoBean(ITipoVeiculoBean tipoVeiculoBean) {
		this.tipoVeiculoBean = tipoVeiculoBean;
	}

	public IIntervaloBean getIntervaloBean() {
		return intervaloBean;
	}

	public void setIntervaloBean(IIntervaloBean intervaloBean) {
		this.intervaloBean = intervaloBean;
	}

	public Simulacao getSimulacao() {
		return simulacao;
	}

	public void setSimulacao(Simulacao Simulacao) {
		this.simulacao = Simulacao;
	}
    
	public List<TipoVeiculo> getListaTipoVeiculos() {
		listaTipoVeiculos = tipoVeiculoBean.listaAll();
		
		return listaTipoVeiculos;
	}

	public void setListaTipoVeiculos(List<TipoVeiculo> listaTipoVeiculos) {
		this.listaTipoVeiculos = listaTipoVeiculos;
	}

	public List<TipoCliente> getListaTipoClientes() {
		listaTipoClientes = tipoClienteBean.listaAll();
		
		return listaTipoClientes;
	}

	public void setListaTipoClientes(List<TipoCliente> listaTipoClientes) {
		this.listaTipoClientes = listaTipoClientes;
	}

	public void setListaIntervalos(List<Intervalo> listaIntervalos) {
		this.listaIntervalos = listaIntervalos;
	}

	public List<Intervalo> getListaIntervalos() {
		listaIntervalos = intervaloBean.listaAll();
		
		return listaIntervalos;
	}

	public void calcular(ActionEvent actionEvent){
		try {
			simulacaoBean.calcular(simulacao, Funcoes.getUsuarioLogado(), Funcoes.getQtdeRegMax());
			
			conversation.end();
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return;
		}
	}

	public void novo(ActionEvent actionEvent){
		this.simulacao = new Simulacao();
	}

}
