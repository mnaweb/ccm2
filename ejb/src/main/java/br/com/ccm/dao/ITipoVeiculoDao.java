package br.com.ccm.dao;

import javax.ejb.Local;

import br.com.ccm.model.TipoVeiculo;

@Local
public interface ITipoVeiculoDao extends IGenericDAO<TipoVeiculo>{	

}
