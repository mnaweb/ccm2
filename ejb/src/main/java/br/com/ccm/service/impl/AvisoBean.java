package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.IAvisoDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Aviso;
import br.com.ccm.model.Usuario;
import br.com.ccm.service.IAvisoBean;

@Stateless
public class AvisoBean implements IAvisoBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308377597382158769L;

	@EJB
	private IAvisoDao dao;
	
	public ArrayList<Aviso> listaAll(){
		return (ArrayList<Aviso>) dao.listAllByOrder("aviso");
	}
	
	public Aviso salvar(Aviso aviso, Usuario usuarioLogado) throws AplicacaoException{
		
		if(aviso.getId()==0){
			aviso.setInclusao(new Date());
			aviso.setCriador(usuarioLogado);
			
			dao.save(aviso);
		}else{
			aviso.setAlteracao(new Date());
			
			dao.save(aviso);
		}
		
		return aviso;
	}
	
	public void deletar(Aviso aviso){
		dao.delete(aviso);
	}
	
	public Aviso getById(Long id){
		return dao.find(id);
	}

	@Override
	public ArrayList<Aviso> listAllByAtivo(int qtdereg){
		return (ArrayList<Aviso>) dao.listAllByAtivo(qtdereg);
	}

}
