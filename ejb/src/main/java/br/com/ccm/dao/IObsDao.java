package br.com.ccm.dao;

import java.util.List;

import br.com.ccm.model.Empresa;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

public interface IObsDao extends IGenericDAO<Obs>{
	
	public List<Obs> listObsVendedor(Vendedor vendedor, int lido, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	public long getQtdeObsParaVendedor(Vendedor vendedor, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	public List<Obs> listObsFichaVendedor(Vendedor vendedor, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	public long getQtdeObsParaCcm(Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	public List<Obs> listObsCcm(int lido, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	long getQtdeObsParaLocatario(Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	List<Obs> listObsLocatario(int lido, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	List<Obs> listObsFichaLocatario(Empresa empresaPortal, long fichaId, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

}