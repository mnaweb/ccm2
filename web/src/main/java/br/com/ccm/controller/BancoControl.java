package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IBancoBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.model.Banco;

@Named(value="mbBanco")
@ConversationScoped
public class BancoControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5690636436369060328L;

	public BancoControl(){
	}

	@EJB
	private IBancoBean bancoBean;
	
    private List<Banco> dataModel;
    private Banco selection = new Banco();
    private Banco banco = new Banco();
    
	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}


	public IBancoBean getBancoBean() {
		return bancoBean;
	}

	public void setBancoBean(IBancoBean bancoBean) {
		this.bancoBean = bancoBean;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco Banco) {
		this.banco = Banco;
	}

	public List<Banco> getDataModel(){
    	
		List<Banco> lista = bancoBean.listaAll(Funcoes.getUsuarioLogado(),Funcoes.getEmpresa(),Funcoes.getQtdeRegMax());
    	
		dataModel = lista;
        return dataModel;
    }

    public Banco getSelection() {
        return selection;
    }

    public void setSelection(Banco selection) {
        this.selection = selection;
    }
    
	public String salvar(){
		if((Funcoes.getEmpresa()==null || Funcoes.getEmpresa().getId()==0) && banco.getId()==0){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Selecione uma empresa.", "Selecione uma empresa.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		bancoBean.salvar(banco, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa());

		Funcoes.setarAcesso("banco");	
    	Funcoes.alteraPag1("Cadastro/bancoConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/bancoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.setarAcesso("banco");	
    	Funcoes.alteraPag1("Cadastro/bancoConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/bancoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String deletar(ActionEvent actionEvent){
		
		banco = getSelection();
		
		try {
			bancoBean.deletar(banco, Funcoes.getQtdeRegMax());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}
		
		conversation.end();
		
		return "bancoConsultar";
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.banco = new Banco();
		
    	Funcoes.setarAcesso("banco");	
    	Funcoes.alteraPag1("Cadastro/bancoConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/bancoDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar() {
		this.banco = getSelection();
		
		Funcoes.setarAcesso("banco");	
    	Funcoes.alteraPag1("Cadastro/bancoConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/bancoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

}
