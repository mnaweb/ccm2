package br.com.ccm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="sm_configuracao")
public class ConfiguracaoSimulador  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5059542716578017911L;
	
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "vl_percPontoDeEquilibrio")
	private BigDecimal percPontoDeEquilibrio;
	@Column(name = "vl_TC")
	private BigDecimal vlTc;
	@OneToMany(targetEntity=Coeficiente.class,mappedBy="configuracaoSimulador",cascade=CascadeType.MERGE)
	private List<Coeficiente> coeficientes;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	
	public ConfiguracaoSimulador() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPercPontoDeEquilibrio() {
		return percPontoDeEquilibrio;
	}

	public void setPercPontoDeEquilibrio(BigDecimal percPontoDeEquilibrio) {
		this.percPontoDeEquilibrio = percPontoDeEquilibrio;
	}
	
	public BigDecimal getVlTc() {
		return vlTc;
	}

	public void setVlTc(BigDecimal vlTc) {
		this.vlTc = vlTc;
	}

	public List<Coeficiente> getCoeficientes() {
		return coeficientes;
	}

	public void setCoeficientes(List<Coeficiente> coeficientes) {
		this.coeficientes = coeficientes;
	}

	public Usuario getCriador() {
		return criador;
	}

	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

	public Date getInclusao() {
		return inclusao;
	}

	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}

	public Date getAlteracao() {
		return alteracao;
	}

	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}

	public boolean addCoeficiente(Coeficiente c){
		if(this.coeficientes==null)
			this.coeficientes=new ArrayList<Coeficiente>();
		
		if(!this.equals(c.getConfiguracaoSimulador())){
			if(c.getConfiguracaoSimulador()!=null && c.getConfiguracaoSimulador().getCoeficientes()!=null){
				c.getConfiguracaoSimulador().getCoeficientes().remove(c);
			}
			c.setConfiguracaoSimulador(this);
		}
		return coeficientes.add(c);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfiguracaoSimulador other = (ConfiguracaoSimulador) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
