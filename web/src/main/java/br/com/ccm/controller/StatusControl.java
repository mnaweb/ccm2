package br.com.ccm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;

import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IClienteBean;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Obs;

@Named(value="mbStatusFicha")
@SessionScoped
public class StatusControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1022864054224258801L;

	public StatusControl(){
		cliente = new Cliente();
		statusLista = new ArrayList<SelectItem>();
		obs = new Obs();

		cliente.setDtEntrada(new Date());
		
	}

	@EJB
	private IClienteBean clienteBean;
	
	private Cliente cliente;
	private List<SelectItem> statusLista;
	private Obs obs;

	public IClienteBean getClienteBean() {
		return clienteBean;
	}

	public void setClienteBean(IClienteBean clienteBean) {
		this.clienteBean = clienteBean;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Obs getObs() {
		return obs;
	}

	public void setObs(Obs obs) {
		this.obs = obs;
	}

	public List<SelectItem> getStatusLista() {
		statusLista = new ArrayList<SelectItem>();
		
		statusLista.add(new SelectItem(Cliente.STATUS_CHECAGEM,"Checagem"));
		statusLista.add(new SelectItem(Cliente.STATUS_PENDENTE,"Pendente"));
		statusLista.add(new SelectItem(Cliente.STATUS_NEGOCIACAO,"Negociação"));
		statusLista.add(new SelectItem(Cliente.STATUS_ANALISE,"Análise"));
		statusLista.add(new SelectItem(Cliente.STATUS_APROVADA,"Aprovada"));
		statusLista.add(new SelectItem(Cliente.STATUS_RECUSADA,"Recusada"));
		statusLista.add(new SelectItem(Cliente.STATUS_CANCELADA,"Cancelada"));
		statusLista.add(new SelectItem(Cliente.STATUS_EFETIVADA,"Efetivada"));
		statusLista.add(new SelectItem(Cliente.STATUS_APROVADO_CANC,"Aprovada-Canc."));

		return statusLista;
	}

	public void setStatusLista(List<SelectItem> statusLista) {
		this.statusLista = statusLista;
	}

	public String salvarStatus(){
		Cliente clienteAux = clienteBean.getClienteById(cliente.getId());
		
		clienteAux.setStatus(cliente.getStatus());
		clienteAux.setArquivo(cliente.getArquivo());
		clienteAux.setPercRetorno(cliente.getPercRetorno());
		clienteAux.setEfetivada(cliente.getEfetivada());
		
		clienteBean.salvar(clienteAux, obs, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa());
		
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String cancelarStatus(){
		
		Funcoes.setarAcesso("ficha");
    	Funcoes.alteraPag1("ficha/fichaConsultar.xhtml");
    	Funcoes.alteraPag2("ficha/fichaDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
    		HttpServletRequest request = (HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		
			String imagemURL = "/sistema/pdf/" + cliente.getId() + "/" + event.getFile().getFileName();
            String realPath = request.getSession().getServletContext().getRealPath(""); 
            
            // cria diretorio
			File tmpDir = new File(realPath + "/sistema/pdf/" + cliente.getId());
            if (!tmpDir.exists())
                tmpDir.mkdirs();

            // cria diretorio
			File tmpDir2 = new File(Funcoes.getMessageFromResourceBundle("caminho.logo")+"/sistema/pdf/" + cliente.getId());
            if (!tmpDir2.exists())
                tmpDir2.mkdirs();

            File arquivoDest = new File(realPath+imagemURL);
            FileOutputStream fos = new FileOutputStream(arquivoDest);  

            fos.write(event.getFile().getContents());  
            fos.flush();  
            fos.close();

            File arquivoDest2 = new File(Funcoes.getMessageFromResourceBundle("caminho.logo")+"/"+imagemURL);
            FileOutputStream fos2 = new FileOutputStream(arquivoDest2);  

            fos2.write(event.getFile().getContents());  
            fos2.flush();  
            fos2.close();

            cliente.setArquivo(imagemURL);
            
            FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");  
            FacesContext.getCurrentInstance().addMessage("arq", msg);
        } catch (Exception ex) {  
        	ex.printStackTrace();
        }
	}

}
