package br.com.ccm.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Remote
public interface IVendedorBean{

	public ArrayList<Vendedor> listaAll();
	
	public Vendedor salvar(Vendedor vendedor, Usuario usuarioLogado, Empresa empresa);
	
	public void deletar(Vendedor vendedor, int qtdereg) throws AplicacaoException;

	public List<Vendedor> listVendedorByUsuario(Usuario usuario, int qtdereg);
}
