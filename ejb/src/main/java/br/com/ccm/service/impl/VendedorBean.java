package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.dao.IVendedorDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.IClienteBean;
import br.com.ccm.service.IUsuarioBean;
import br.com.ccm.service.IVendedorBean;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

@Stateless
public class VendedorBean implements IVendedorBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6842178215575170579L;

	@EJB
	private IVendedorDao dao;
	
	@EJB
	private IClienteBean clienteBean;
	
	@EJB
	private IUsuarioBean usuarioBean;
	
	public ArrayList<Vendedor> listaAll(){
		return (ArrayList<Vendedor>) dao.findAll();
	}
	
	public Vendedor salvar(Vendedor vendedor, Usuario usuarioLogado, Empresa empresa){
		if(vendedor.getId()==0){
			if(vendedor.getEmpresaPortal()==null){
				vendedor.setEmpresaPortal(empresa);
			} 
			vendedor.setInclusao(new Date());
			vendedor.setCriador(usuarioLogado);
		}else{
			vendedor.setAlteracao(new Date());
		}

		if((vendedor.getEmpresaPortal()==null || vendedor.getEmpresaPortal().getId()==0) && usuarioLogado.getAdmin()){
			vendedor.setEmpresaPortal(null);
		}

		dao.save(vendedor);
		
		return vendedor;
	}
	
	public void deletar(Vendedor vendedor, int qtdereg) throws AplicacaoException{
		String msg = "N??o foi poss??vel excluir.\nOs seguintes itens est??o vinculados:\n";

		List<Cliente> list = clienteBean.listFichasByVendedor(vendedor, qtdereg);
		if(list.size()>0){
			msg = msg +"\nFichas:\n";
			int mensagem = 0;
			for (Cliente cliente : list) {				
				msg=msg+"- "+cliente.getId()+"\n";
				mensagem++;
				if(mensagem==3) break;
			}
			throw new AplicacaoException(msg);
		}
		
		List<Usuario> listUsu = usuarioBean.listUsuarioPorVendedor(vendedor, qtdereg);
		if(listUsu.size()>0){
			msg = msg +"\nUsu??rio:\n";
			int mensagem = 0;
			for (Usuario usuario : listUsu) {				
				msg=msg+"- "+usuario.getNome()+"\n";
				mensagem++;
				if(mensagem==3) break;
			}
			throw new AplicacaoException(msg);
		}
		
		dao.delete(vendedor);
	}

	public List<Vendedor> listVendedorByUsuario(Usuario usuario, int qtdereg){
		return dao.listVendedorByUsuario(usuario, qtdereg);
	}
}
