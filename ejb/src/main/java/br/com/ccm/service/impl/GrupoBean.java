package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.com.ccm.service.IGrupoBean;
import br.com.ccm.service.IModuloBean;
import br.com.ccm.service.IPermissaoBean;
import br.com.ccm.dao.IGrupoDao;
import br.com.ccm.model.Grupo;
import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.Usuario;

@Stateless
public class GrupoBean implements IGrupoBean,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3068905061868573009L;
	
	@EJB
	private IModuloBean moduloBean;
	
	@EJB
	private IPermissaoBean permissaoBean;
	
	@EJB
	private IGrupoDao dao;

	public List<Grupo> listGrupo(){
		return (ArrayList<Grupo>) dao.findAll();
	}
	
	public Grupo getGrupoById(Long id){
		return dao.find(id);
	}
	public List<Modulo> listModulo(int qtdereg){
		List<Modulo> lista = moduloBean.listModulo(qtdereg);
		
		return lista;
	}
	
	public Grupo salvar(Grupo grupo, Usuario usuarioLogado){
		if(grupo.getId()==0){
			grupo.setInclusao(new Date());
			grupo.setCriador(usuarioLogado);
		}else{
			grupo.setAlteracao(new Date());
		}
		
		try{
			Grupo grupoOld = dao.find(grupo.getId());
			
			for(Permissao obj : grupoOld.getPermissoes()){
				if(!grupo.getPermissoes().contains(obj)){
					permissaoBean.deletar(obj);
				}
			}
		}catch (Exception e) {}
		
		dao.save(grupo);
		
		return grupo;
	}
	
	public void deletar(Grupo grupo){
		dao.delete(grupo);
	}

}
