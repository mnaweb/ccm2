package br.com.ccm.dao;

import java.util.List;

import br.com.ccm.model.Modulo;
import br.com.ccm.model.Permissao;
import br.com.ccm.model.Usuario;

public interface IPermissaoDao extends IGenericDAO<Permissao>{
	
	public List<Modulo> listPermissao(Usuario usuario, int qtdeReg);

	public List<Permissao> listPermissaoUsuario(Usuario usuario, String tarefa, int qtdeReg);

	public boolean verifAcesso(Usuario usuario, String tarefa, int acao, int qtdeReg);

}
