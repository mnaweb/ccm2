package br.com.ccm.service;

import java.util.List;

import javax.ejb.Remote;

import br.com.ccm.model.Modulo;
import br.com.ccm.model.Usuario;

@Remote
public interface IModuloBean{

	public static int TIPO_TAREFA_MENU = 0;
	public static int TIPO_TAREFA_CRUD = 1;
	public static int TIPO_TAREFA_RELATORIO = 2;
	public static int TIPO_TAREFA_GENERICA = 3;
	
	public List<Modulo> listModulo(int qtdereg);
	
	public Modulo salvar(Modulo modulo, Usuario usuarioLogado);
	
	public void deletar(Modulo modulo);

}
