package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.ITipoVeiculoBean;
import br.com.ccm.model.TipoVeiculo;

@Named(value="mbTipoVeiculo")
@ConversationScoped
public class TipoVeiculoControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5690636436369060328L;

	public TipoVeiculoControl(){
	}

	@EJB
	private ITipoVeiculoBean tipoVeiculoBean;

    private List<TipoVeiculo> dataModel;
    private TipoVeiculo selection = new TipoVeiculo();
    private TipoVeiculo tipoVeiculo = new TipoVeiculo();

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public ITipoVeiculoBean getTipoVeiculoBean() {
		return tipoVeiculoBean;
	}

	public void setTipoVeiculoBean(ITipoVeiculoBean tipoVeiculoBean) {
		this.tipoVeiculoBean = tipoVeiculoBean;
	}

	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculo TipoVeiculo) {
		this.tipoVeiculo = TipoVeiculo;
	}

	public List<TipoVeiculo> getDataModel(){
    	
		List<TipoVeiculo> lista = tipoVeiculoBean.listaAll();
    	
		dataModel = lista;
        return dataModel;
    }

    public TipoVeiculo getSelection() {
        return selection;
    }

    public void setSelection(TipoVeiculo selection) {
        this.selection = selection;
    }
    
	public String salvar(){
		try {
			tipoVeiculoBean.salvar(tipoVeiculo, Funcoes.getUsuarioLogado());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}

		Funcoes.setarAcesso("tipoVeiculo");	
    	Funcoes.alteraPag1("simulador/tipoVeiculoConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoVeiculoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.setarAcesso("tipoVeiculo");	
    	Funcoes.alteraPag1("simulador/tipoVeiculoConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoVeiculoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public void deletar(ActionEvent actionEvent){
		tipoVeiculo = getSelection();
		tipoVeiculoBean.deletar(tipoVeiculo);
		
		conversation.end();
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.tipoVeiculo = new TipoVeiculo();
		
    	Funcoes.setarAcesso("tipoVeiculo");	
    	Funcoes.alteraPag1("simulador/tipoVeiculoConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoVeiculoDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar() {
		this.tipoVeiculo = getSelection();
		
		Funcoes.setarAcesso("tipoVeiculo");	
    	Funcoes.alteraPag1("simulador/tipoVeiculoConsultar.xhtml");
    	Funcoes.alteraPag2("simulador/tipoVeiculoDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

}
