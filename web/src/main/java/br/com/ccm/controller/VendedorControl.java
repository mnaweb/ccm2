package br.com.ccm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;

import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.service.IVendedorBean;
import br.com.ccm.model.Vendedor;

@Named(value="mbVendedor")
@ConversationScoped
public class VendedorControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8922324998303265670L;

	public VendedorControl(){
	}

	@EJB
	private IVendedorBean vendedorBean;
	
    private List<Vendedor> dataModel;
    private Vendedor selection = new Vendedor();
    private Vendedor vendedor = new Vendedor();

	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}

	public IVendedorBean getVendedorBean() {
		return vendedorBean;
	}

	public void setVendedorBean(IVendedorBean vendedorBean) {
		this.vendedorBean = vendedorBean;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor Vendedor) {
		this.vendedor = Vendedor;
	}

	public List<Vendedor> getDataModel(){
    	
		List<Vendedor> lista = vendedorBean.listaAll();
    	
		dataModel = lista;
        return dataModel;
    }

    public Vendedor getSelection() {
        return selection;
    }

    public void setSelection(Vendedor selection) {
        this.selection = selection;
    }
    
	public String salvar(){
		vendedorBean.salvar(vendedor,Funcoes.getUsuarioLogado(), Funcoes.getEmpresa());
		
		Funcoes.setarAcesso("vendedor");
    	Funcoes.alteraPag1("Cadastro/vendedorConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/vendedorDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		conversation.end();
		
		return "crud";
	}

	public String cancelar(){
		Funcoes.setarAcesso("vendedor");
    	Funcoes.alteraPag1("Cadastro/vendedorConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/vendedorDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_CONSULTAR);
		
		return "crud";
	}
	
	public String deletar(ActionEvent actionEvent){
		
		vendedor = getSelection();
		
		try{
			vendedorBean.deletar(vendedor, Funcoes.getQtdeRegMax());
		} catch (AplicacaoException e) {
			Funcoes.createMessageJSFErro(e.getMessage(), null);
			return null;
		}
		
		conversation.end();
		
		return "vendedorConsultar";
	}
	
	public void criar(ActionEvent actionEvent) {
    	this.vendedor = new Vendedor();
    	
		Funcoes.setarAcesso("vendedor");
    	Funcoes.alteraPag1("Cadastro/vendedorConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/vendedorDigitar.xhtml");
    	Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editar() {
		this.vendedor = getSelection();
		
		Funcoes.setarAcesso("vendedor");
    	Funcoes.alteraPag1("Cadastro/vendedorConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/vendedorDigitar.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

    public void editarLogo() {
		this.vendedor = getSelection();
		
		Funcoes.setarAcesso("vendedor");
    	Funcoes.alteraPag1("Cadastro/vendedorConsultar.xhtml");
    	Funcoes.alteraPag2("Cadastro/vendedorLogo.xhtml");
		Funcoes.abaAtiva(PrincipalControl.ABA_DIGITAR);
    }

	public void handleFileUpload(FileUploadEvent event) {
		try {
    		HttpServletRequest request = (HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		
			String imagemURL = "/sistema/img_usuario/" + vendedor.getId() + "_" + event.getFile().getFileName();
            String realPath = request.getSession().getServletContext().getRealPath(""); 
            
            // cria diretorio
			File tmpDir = new File(realPath + "/sistema/img_usuario");
            if (!tmpDir.exists())
                tmpDir.mkdirs();
            
            File arquivoDest = new File(realPath+imagemURL);
            FileOutputStream fos = new FileOutputStream(arquivoDest);  

            fos.write(event.getFile().getContents());  
            fos.flush();  
            fos.close();
            
            // cria diretorio
			File tmpDir2 = new File(Funcoes.getMessageFromResourceBundle("caminho.logo")+"/sistema/img_usuario");
            if (!tmpDir2.exists())
                tmpDir2.mkdirs();

            File arquivoDest2 = new File(Funcoes.getMessageFromResourceBundle("caminho.logo")+"/"+imagemURL);
            FileOutputStream fos2 = new FileOutputStream(arquivoDest2);  

            fos2.write(event.getFile().getContents());  
            fos2.flush();  
            fos2.close();

            vendedor.setArquivo(imagemURL);
            
            FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");  
            FacesContext.getCurrentInstance().addMessage("arq", msg);
        } catch (Exception ex) {  
        	ex.printStackTrace();
        }
	}

}
