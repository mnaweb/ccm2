package br.com.ccm.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import br.com.ccm.service.IClienteBean;
import br.com.ccm.service.IObsBean;
import br.com.ccm.service.IPrincipalBean;
import br.com.ccm.jsf.Funcoes;
import br.com.ccm.model.Obs;
import br.com.ccm.model.Usuario;

@Named(value="mbAvisoVen")
@ConversationScoped
public class AvisoVendedorControl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8216369151334853999L;

	public AvisoVendedorControl(){
	}

	@EJB
	private IClienteBean clienteBean;
	
	@EJB
	private IPrincipalBean principalBean;
	
	@EJB
	private IObsBean obsBean;
	
	private List<Obs> listObs;
	private List<Obs> listObsCcm;
	private List<Obs> listObsLocatario;
	
	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void initBean() {
		if (conversation.isTransient()) {
			conversation.begin();
		}
	}
	
	public IClienteBean getClienteBean() {
		return clienteBean;
	}

	public void setClienteBean(IClienteBean clienteBean) {
		this.clienteBean = clienteBean;
	}

	public IObsBean getObsBean() {
		return obsBean;
	}

	public void setObsBean(IObsBean obsBean) {
		this.obsBean = obsBean;
	}

	public List<Obs> getListObs() {
		Usuario usu = Funcoes.getUsuarioLogado();
		
		listObs = obsBean.listObsVendedor(usu.getVendedor(), 0, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		
		return listObs;
	}

	public void setListObs(List<Obs> listObs) {
		this.listObs = listObs;
	}

	public void lido(ActionEvent actionEvent){
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getAttribute("his")!=null){
			Long id = ((Obs)req.getAttribute("his")).getId();
			Obs obs = obsBean.getById(id);
			obs.setLido(!obs.isLido());
			obsBean.salvarObs(obs, Funcoes.getUsuarioLogado());
		}
		conversation.end();
	}
	
	public String avisoVendedor() {
		return "avisoVendedor";
    }

	public List<Obs> getListObsCcm() {
		listObsCcm = obsBean.listObsCcm(0, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		
		return listObsCcm;
	}

	public void setListObsCcm(List<Obs> listObsCcm) {
		this.listObsCcm = listObsCcm;
	}

	public List<Obs> getListObsLocatario() {
		listObsLocatario = obsBean.listObsLocatario(0, Funcoes.getUsuarioLogado(), Funcoes.getEmpresa(), Funcoes.getQtdeRegMax());
		
		return listObsLocatario;
	}

	public void setListObsLocatario(List<Obs> listObsLocatario) {
		this.listObsLocatario = listObsLocatario;
	}

	public void lidoLoc(ActionEvent actionEvent){
		FacesContext context = FacesContext.getCurrentInstance();   
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();   
		if(req.getAttribute("his")!=null){
			Long id = ((Obs)req.getAttribute("his")).getId();
			Obs obs = obsBean.getById(id);
			obs.setLidoLocatario(!obs.isLidoLocatario());
			obsBean.salvarObs(obs, Funcoes.getUsuarioLogado());
		}
		conversation.end();
	}

}
