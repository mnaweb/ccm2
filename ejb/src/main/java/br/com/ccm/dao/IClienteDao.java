package br.com.ccm.dao;

import java.util.Date;
import java.util.List;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;
import br.com.ccm.model.Vendedor;

public interface IClienteDao extends IGenericDAO<Cliente>{

	public List<Cliente> listFichas(Vendedor vendedor, String cpfProc, Date dtFin, Date dtIni, String statusProc, boolean cancelado, String cnpjProc, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	public long getQtdeFichasStatus(String status, Usuario usuarioLogado, Empresa empresa, int qtdeReg);
	
	public Long verificarDuplicidadeFicha(Vendedor vendedor, String cpf, Date data, boolean considerarExcluidas, Usuario usuarioLogado, Empresa empresa, int qtdeReg);

	public List<Cliente> listFichasByVendedor(Vendedor vendedor, int qtdeReg);
	
	public List<Cliente> listFichasByUsuario(Usuario usuario, int qtdeReg);
	
	public List<Cliente> listFichasByBanco(Banco banco, int qtdeReg);
	
}