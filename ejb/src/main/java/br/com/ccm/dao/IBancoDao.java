package br.com.ccm.dao;

import java.util.List;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

public interface IBancoDao extends IGenericDAO<Banco>{
	
	public List<Banco> listAll(Usuario usuarioLogado, Empresa empresa, int qtdeReg);
	
	public List<Banco> listAllByEmpresa(Empresa empresa, int qtdeReg);
	
	public List<Banco> listBancosByUsuario(Usuario usuario, int qtdeReg);
	
}
