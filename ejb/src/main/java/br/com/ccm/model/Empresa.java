package br.com.ccm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ts_empresa")
public class Empresa implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1311201599836799764L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_id")
	private Long id;
	@Column(name = "ds_nome")
	private String nome;
	@Column(name = "ds_url")
	private String url;
	@Column(name = "ds_indexMsgTxt1")
	private String indexMsgTxt1;
	@Column(name = "ds_indexMsgTxt2")
	private String indexMsgTxt2;
	@Column(name = "ds_indexMsgTxt3")
	private String indexMsgTxt3;
	@Column(name = "ds_indexMsgTxt4")
	private String indexMsgTxt4;
	@Column(name = "ds_indexMsgTxt5")
	private String indexMsgTxt5;
	@Column(name = "ds_indexMsgTxt6")
	private String indexMsgTxt6;
	@Column(name = "ds_menuDadosTxt1")
	private String menuDadosTxt1;
	@Column(name = "ds_menuDadosTxt2")
	private String menuDadosTxt2;
	@Column(name = "ds_menuDadosTxt3")
	private String menuDadosTxt3;
	@Column(name = "ds_menuDadosTxt4")
	private String menuDadosTxt4;
	@Column(name = "ds_menuDadosTxt5")
	private String menuDadosTxt5;
	@Column(name = "ds_menuDadosTxt6")
	private String menuDadosTxt6;
	@Column(name = "ds_relatorioUrl")
	private String relatorioUrl;
	@Column(name = "ds_relatorioEmail")
	private String relatorioEmail;
	@Column(name = "ds_relatorioFone1")
	private String relatorioFone1;
	@Column(name = "ds_relatorioFone2")
	private String relatorioFone2;
	@Column(name = "ds_tema")
	private String tema;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@Column(name = "vl_impostos")
	private BigDecimal impostos;
	@Column(name = "vl_custoFicha")
	private BigDecimal custoFicha;
	@Column(name = "vl_custoFichaPerc")
	private BigDecimal custoFichaPerc;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@Column(name = "fl_ativo")
	private boolean ativo;
	@Lob
    @Column(name="img_logo", nullable=false, columnDefinition="mediumblob")
    private byte[] image;
	
	public Empresa() {
		id = new Long(0);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIndexMsgTxt1() {
		return indexMsgTxt1;
	}
	public void setIndexMsgTxt1(String indexMsgTxt1) {
		this.indexMsgTxt1 = indexMsgTxt1;
	}
	public String getIndexMsgTxt2() {
		return indexMsgTxt2;
	}
	public void setIndexMsgTxt2(String indexMsgTxt2) {
		this.indexMsgTxt2 = indexMsgTxt2;
	}
	public String getIndexMsgTxt3() {
		return indexMsgTxt3;
	}
	public void setIndexMsgTxt3(String indexMsgTxt3) {
		this.indexMsgTxt3 = indexMsgTxt3;
	}
	public String getIndexMsgTxt4() {
		return indexMsgTxt4;
	}
	public void setIndexMsgTxt4(String indexMsgTxt4) {
		this.indexMsgTxt4 = indexMsgTxt4;
	}
	public String getIndexMsgTxt5() {
		return indexMsgTxt5;
	}
	public void setIndexMsgTxt5(String indexMsgTxt5) {
		this.indexMsgTxt5 = indexMsgTxt5;
	}
	public String getIndexMsgTxt6() {
		return indexMsgTxt6;
	}
	public void setIndexMsgTxt6(String indexMsgTxt6) {
		this.indexMsgTxt6 = indexMsgTxt6;
	}
	public String getMenuDadosTxt1() {
		return menuDadosTxt1;
	}
	public void setMenuDadosTxt1(String menuDadosTxt1) {
		this.menuDadosTxt1 = menuDadosTxt1;
	}
	public String getMenuDadosTxt2() {
		return menuDadosTxt2;
	}
	public void setMenuDadosTxt2(String menuDadosTxt2) {
		this.menuDadosTxt2 = menuDadosTxt2;
	}
	public String getMenuDadosTxt3() {
		return menuDadosTxt3;
	}
	public void setMenuDadosTxt3(String menuDadosTxt3) {
		this.menuDadosTxt3 = menuDadosTxt3;
	}
	public String getMenuDadosTxt4() {
		return menuDadosTxt4;
	}
	public void setMenuDadosTxt4(String menuDadosTxt4) {
		this.menuDadosTxt4 = menuDadosTxt4;
	}
	public String getMenuDadosTxt5() {
		return menuDadosTxt5;
	}
	public void setMenuDadosTxt5(String menuDadosTxt5) {
		this.menuDadosTxt5 = menuDadosTxt5;
	}
	public String getMenuDadosTxt6() {
		return menuDadosTxt6;
	}
	public void setMenuDadosTxt6(String menuDadosTxt6) {
		this.menuDadosTxt6 = menuDadosTxt6;
	}
	public String getRelatorioUrl() {
		return relatorioUrl;
	}
	public void setRelatorioUrl(String relatorioUrl) {
		this.relatorioUrl = relatorioUrl;
	}
	public String getRelatorioEmail() {
		return relatorioEmail;
	}
	public void setRelatorioEmail(String relatorioEmail) {
		this.relatorioEmail = relatorioEmail;
	}
	public String getRelatorioFone1() {
		return relatorioFone1;
	}
	public void setRelatorioFone1(String relatorioFone1) {
		this.relatorioFone1 = relatorioFone1;
	}
	public String getRelatorioFone2() {
		return relatorioFone2;
	}
	public void setRelatorioFone2(String relatorioFone2) {
		this.relatorioFone2 = relatorioFone2;
	}
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	
	public BigDecimal getImpostos() {
		return impostos;
	}
	public void setImpostos(BigDecimal impostos) {
		this.impostos = impostos;
	}
	public BigDecimal getCustoFicha() {
		return custoFicha;
	}
	public void setCustoFicha(BigDecimal custoFicha) {
		this.custoFicha = custoFicha;
	}
	public BigDecimal getCustoFichaPerc() {
		return custoFichaPerc;
	}
	public void setCustoFichaPerc(BigDecimal custoFichaPerc) {
		this.custoFichaPerc = custoFichaPerc;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}