package br.com.ccm.service;

import javax.ejb.Remote;

@Remote
public interface IBeanDeLista{
	
	public Object getObjetoLista(Class clazz, long id);
	
}
