package br.com.ccm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.ccm.dao.IBancoDao;
import br.com.ccm.exception.AplicacaoException;
import br.com.ccm.service.IBancoBean;
import br.com.ccm.service.IClienteBean;
import br.com.ccm.model.Banco;
import br.com.ccm.model.Cliente;
import br.com.ccm.model.Empresa;
import br.com.ccm.model.Usuario;

@Stateless
public class BancoBean implements IBancoBean,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4622368647086630941L;

	@EJB
	private IBancoDao dao;
	
	@EJB
	private IClienteBean clienteBean;
	
	public ArrayList<Banco> listaAll(Usuario usuarioLogado, Empresa empresa, int qtdereg){
		return (ArrayList<Banco>) dao.listAll(usuarioLogado, empresa, qtdereg);
	}
	
	public ArrayList<Banco> listAllByEmpresa(Empresa empresa, int qtdereg){
		return (ArrayList<Banco>) dao.listAllByEmpresa(empresa, qtdereg);
	}
	
	public Banco salvar(Banco banco,Usuario usuarioLogado, Empresa empresa){
		if(banco.getId()==0){
			if(banco.getEmpresaPortal()==null){
				banco.setEmpresaPortal( empresa);
			} 
			banco.setInclusao(new Date());
			banco.setCriador( usuarioLogado);
		}else{
			banco.setAlteracao(new Date());
		}
		
		dao.save(banco);
		
		return banco;
	}
	
	public void deletar(Banco banco, int qtdereg) throws AplicacaoException{
		List<Cliente> list = clienteBean.listFichasByBanco(banco, qtdereg);
		if(list.size()>0){
			String msg = "N??o foi poss??vel excluir.\nOs seguintes itens est??o vinculados:\n";
			msg = msg +"\nFichas:\n";
			int mensagem = 0;
			for (Cliente cliente : list) {				
				msg=msg+"- "+cliente.getId()+"\n";
				mensagem++;
				if(mensagem==3) break;
			}
			throw new AplicacaoException(msg);
		}
			
		dao.delete(banco);
	}

	public List<Banco> listBancosByUsuario(Usuario usuario, int qtdereg){
		return dao.listBancosByUsuario(usuario, qtdereg);
	}

}
