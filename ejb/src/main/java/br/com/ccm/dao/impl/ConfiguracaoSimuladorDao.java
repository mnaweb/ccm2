package br.com.ccm.dao.impl;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.ccm.dao.IConfiguracaoSimuladorDao;
import br.com.ccm.model.ConfiguracaoSimulador;

@Stateless
public class ConfiguracaoSimuladorDao extends GenericDAO<ConfiguracaoSimulador> implements IConfiguracaoSimuladorDao,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460340813643414204L;

	public ConfiguracaoSimuladorDao() {
		super(ConfiguracaoSimulador.class);
	}

}
