package br.com.ccm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="cd_endereco")
public class Endereco implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3824220917498401404L;
	
	public static final int TIPO_ENDERECO_RESIDENCIAL=1;
	public static final int TIPO_ENDERECO_COMERCIAL=2;
	
	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name="fk_cliente")
	private Cliente cliente;
	@Column(name = "nr_tpend")
	private int tipoEndereco;
	@Column(name = "ds_cep")
	private String cep;
	@Column(name = "ds_logradouro")
	private String logradouro;
	@Column(name = "nr_end")
	private int numEnd;
	@Column(name = "ds_complemento")
	private String complemento;
	@Column(name = "ds_bairro")
	private String bairro;
	@Column(name = "ds_cidade")
	private String cidade;
	@Column(name = "ds_uf")
	private String uf;
	@Column(name = "nr_temporesanos")
	private int tempoResAnos;
	@Column(name = "nr_temporesmeses")
	private int tempoResMeses;
	@Column(name = "ds_tpresidencia")
	private String tpResidencia;
	@Column(name = "dh_inclusao")
	private Date inclusao;
	@Column(name = "dh_alteracao")
	private Date alteracao;
	@ManyToOne
	@JoinColumn(name="fk_criador")
	private Usuario criador;
	@Column(name = "tg_inativo")
	private Long inativo;
	
	public Endereco() {
		if(this.id==null)
			this.id=new Long(0);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(int tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public int getNumEnd() {
		return numEnd;
	}

	public void setNumEnd(int numEnd) {
		this.numEnd = numEnd;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public int getTempoResAnos() {
		return tempoResAnos;
	}

	public void setTempoResAnos(int tempoResAnos) {
		this.tempoResAnos = tempoResAnos;
	}

	public int getTempoResMeses() {
		return tempoResMeses;
	}

	public void setTempoResMeses(int tempoResMeses) {
		this.tempoResMeses = tempoResMeses;
	}

	public String getTpResidencia() {
		return tpResidencia;
	}

	public void setTpResidencia(String tpResidencia) {
		this.tpResidencia = tpResidencia;
	}

	public Date getInclusao() {
		return inclusao;
	}
	public void setInclusao(Date inclusao) {
		this.inclusao = inclusao;
	}
	public Date getAlteracao() {
		return alteracao;
	}
	public void setAlteracao(Date alteracao) {
		this.alteracao = alteracao;
	}
	public Long getInativo() {
		return inativo;
	}
	public void setInativo(Long inativo) {
		this.inativo = inativo;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
